/**
 *	Classname: AcPreguntaSegmentoImpl
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.model.PreguntaSegmento;
import co.com.coomeva.argumento.comercial.repository.PreguntaSegmentoRepository;
import co.com.coomeva.argumento.comercial.service.PreguntaSegmentoService;

@Transactional
@Service
public class PreguntaSegmentoServiceImpl implements PreguntaSegmentoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreguntaSegmentoServiceImpl.class);

	@Autowired
	private PreguntaSegmentoRepository preguntaSegmentoRepository;

	/**
	 * Metodo que retorna las preguntas de acuerdo a el codigo de segmento que
	 * recibe por para metro
	 * 
	 * @param codigoSegmento
	 * @return lstPreguntaSegmento
	 */
	@Override
	public List<PreguntaSegmento> findQuestionBySegment(Long codigoSegmento) {
		List<PreguntaSegmento> lstPreguntaSegmento = new ArrayList<>();
		try {
			lstPreguntaSegmento = this.preguntaSegmentoRepository.findQuestionBySegment(codigoSegmento);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstPreguntaSegmento;
	}
}
