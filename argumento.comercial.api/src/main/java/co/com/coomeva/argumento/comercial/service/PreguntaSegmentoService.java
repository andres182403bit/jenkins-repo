/**
 *	Interfasename: IAcPreguntaSegmento
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.model.PreguntaSegmento;

public interface PreguntaSegmentoService {
	public List<PreguntaSegmento> findQuestionBySegment(Long codigoSegmento);

}