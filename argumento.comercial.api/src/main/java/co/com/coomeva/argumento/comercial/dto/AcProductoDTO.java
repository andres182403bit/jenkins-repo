/**
 *	Classname: AcProductoDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

import java.math.BigDecimal;

public class AcProductoDTO {
	private Long codigo;
	private String descripcion;
	private String caracteristica;
	private String ventaja;
	private String requisitoDocumental;
	private String simulador;
	private String nota;
	private BigDecimal valor;
	private AcObjetivoDTO objetivoDto;

	public AcObjetivoDTO getObjetivoDto() {
		return objetivoDto;
	}

	public void setObjetivoDto(AcObjetivoDTO objetivoDto) {
		this.objetivoDto = objetivoDto;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCaracteristica() {
		return caracteristica;
	}

	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}

	public String getVentaja() {
		return ventaja;
	}

	public void setVentaja(String ventaja) {
		this.ventaja = ventaja;
	}

	public String getRequisitoDocumental() {
		return requisitoDocumental;
	}

	public void setRequisitoDocumental(String requisitoDocumental) {
		this.requisitoDocumental = requisitoDocumental;
	}

	public String getSimulador() {
		return simulador;
	}

	public void setSimulador(String simulador) {
		this.simulador = simulador;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "AcProductoDTO [codigo=" + codigo + ", descripcion=" + descripcion + ", caracteristica=" + caracteristica
				+ ", ventaja=" + ventaja + ", requisitoDocumental=" + requisitoDocumental + ", simulador=" + simulador
				+ ", nota=" + nota + ", valor=" + valor + ", objetivoDto=" + objetivoDto + "]";
	}
}
