package co.com.coomeva.argumento.comercial.util;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.BodyInserters.FormInserter;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import co.com.coomeva.argumento.comercial.dto.AcProductoProspectoDTO;
import co.com.coomeva.argumento.comercial.dto.GoDoWorksResponse;

public class DistributorWSRest {

	private static final Logger LOGGER = LoggerFactory.getLogger(DistributorWSRest.class);

	public static GoDoWorksResponse restGoDoWorks(String cuenta, String proyecto, String baseUrl, String uri,
			AcProductoProspectoDTO productoProspectoDTO) {
		GoDoWorksResponse responseGoDoWork = new GoDoWorksResponse();
		responseGoDoWork.setError(true);
		ClientResponse clientResponse = null;
		try {
			FormInserter<String> formInserter = BodyInserters.fromFormData("cuenta", cuenta);
			formInserter.with("proyecto", proyecto);

			formInserter.with("cedula", productoProspectoDTO.getCodigoProspecto().getIdentificacion());
			formInserter.with("nombres", productoProspectoDTO.getCodigoProspecto().getNombre()
					+ productoProspectoDTO.getCodigoProspecto().getApellido());
			formInserter.with("direccion", productoProspectoDTO.getCodigoProspecto().getDireccion());
			formInserter.with("fechas", productoProspectoDTO.getFechaAOfrecer());
			formInserter.with("producto", productoProspectoDTO.getCodigoProducto().getDescripcion());
			formInserter.with("zona", productoProspectoDTO.getCodigoProspecto().getZona());
			formInserter.with("ejecutivo",
					productoProspectoDTO.getCodigoProspecto().getEjecutivoIntegral().getIdentificacion());
			formInserter.with("correo", productoProspectoDTO.getCodigoProspecto().getEmail());
			formInserter.with("estrato", productoProspectoDTO.getCodigoProspecto().getEstrato());
			formInserter.with("nivelAcademico", productoProspectoDTO.getCodigoProspecto().getNivelAcademico());
			formInserter.with("profesion", productoProspectoDTO.getCodigoProspecto().getProfesion());
			formInserter.with("celular", productoProspectoDTO.getCodigoProspecto().getCelular());
			formInserter.with("departamento",
					productoProspectoDTO.getCodigoProspecto().getCiudadDto().getNombreDepartamento());
			formInserter.with("localidad", productoProspectoDTO.getCodigoProspecto().getCiudadDto().getNombre());

			WebClient webRestClient = WebClient.builder().baseUrl(baseUrl)
					.defaultHeader(HttpHeaders.CONTENT_TYPE, "application/www-form-urlencoded")
					.defaultUriVariables(Collections.singletonMap("url", baseUrl)).build();

			clientResponse = webRestClient.post()
					// Se setea la URL de consulta empleado
					.uri(uri)
					// Se setea el request al cliente
					// .body(Mono.just(empleadoSFRequest), GoDoWorksRequest.class)
					.body(formInserter)
					// Se ejecuta el cliente
					.exchange().block();

			// Se obtiene el response del cliente
			responseGoDoWork = clientResponse.bodyToMono(GoDoWorksResponse.class).block();

			LOGGER.info("GoDoWorks Request: {}", productoProspectoDTO.toString());
			LOGGER.info("GoDoWorks Error: {}", responseGoDoWork.isError());
			LOGGER.info("GoDoWorks Mensaje: {}", responseGoDoWork.getMensaje());
			Thread.sleep(1000);
		} catch (Exception e) {
			responseGoDoWork.setMensaje("Error enviando GoDoWorks");
			LOGGER.error("Error llamando el servicio REST RESTGODOWORKS.", e);
		}

		return responseGoDoWork;
	}
}