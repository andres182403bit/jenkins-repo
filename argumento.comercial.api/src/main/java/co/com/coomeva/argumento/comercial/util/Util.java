/**
* Classname: Util
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Util {
	private static final Logger log = LoggerFactory.getLogger(Util.class);
	public static final String INFO = "info";
	public static final String WARM = "warm";
	public static final String ERROR = "error";

	/**
	 * Permite obtener la fecha recibida, en el formato recibido
	 * 
	 * @param fechaActual
	 * @return dtmFecha
	 */
	public static Date formatoFecha(String strFecha, String formato) {
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formato);
		Date dtmFecha = new Date();
		try {
			dtmFecha = formatoDelTexto.parse(strFecha);
		} catch (Exception e) {
			log.error("Error: {}", e.getMessage(), e);
		}
		return dtmFecha;
	}

	/**
	 * Permite obtener la fecha actual con el separador que se le indique AAAA-MM-DD
	 * 
	 * @param separador
	 * @param Date
	 * @return sbDate
	 */
	public static String formatoFecha(Date fecha, String separador) {
		String strFecha = "";
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(fecha);

			StringBuilder sbDate = new StringBuilder();

			sbDate.append(cal.get(Calendar.YEAR));
			sbDate.append(separador);

			int month = cal.get(Calendar.MONTH) + 1;
			String strMonth = null;
			if (month <= 9)
				strMonth = "0" + month;
			else
				strMonth = "" + month;
			sbDate.append(strMonth);
			sbDate.append(separador);

			int day = cal.get(Calendar.DAY_OF_MONTH);
			String strDay = null;

			if (day <= 9) {
				strDay = "0" + day;
			} else {
				strDay = "" + day;
			}
			
			sbDate.append(strDay);

			strFecha = sbDate.toString();
		} catch (Exception e) {
			log.error("Error: {}", e.getMessage(), e);
		}
		
		return strFecha;
	}
}
