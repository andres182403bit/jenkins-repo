/**
* InterfaseName: IAcObjetivoProspectoRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.coomeva.argumento.comercial.model.ObjetivosProspecto;

public interface ObjetivoProspectoRepository extends JpaRepository<ObjetivosProspecto, Long> {

	@Query(value = "SELECT a FROM ObjetivosProspecto a WHERE a.codigoObjetivo.codigo = :codObjetivo AND a.codigoProspecto.codigo = :codProspecto")
	public ObjetivosProspecto findObjetivoProspecto(@Param("codObjetivo") Long codObjetivo,
			@Param("codProspecto") Long codProspecto);

	@Modifying
	@Query(value = "DELETE FROM ObjetivosProspecto a WHERE a.codigoProspecto.codigo = :codProspecto")
	public void deleteObjetivoProspecto(@Param("codProspecto") Long codProspecto);
}
