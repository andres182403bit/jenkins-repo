/**
* Classname: ClientWS
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import co.com.coomeva.argumento.comercial.dto.SectionDTO;

public class ClientWS {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientWS.class);

	public static final int VALIDA_APP_PROFILE = 1;

	/**
	 * 
	 * @param xmldata
	 * @param uri
	 * @return
	 */
	public static NodeList sendXmlCustomNode(String xmldata, String uri) {
		try {
			// Create socket
			MessageFactory msgFactory = MessageFactory.newInstance();
			SOAPConnection con = SOAPConnectionFactory.newInstance().createConnection();
			SOAPMessage reqMsg = msgFactory.createMessage(null, new ByteArrayInputStream(xmldata.getBytes()));

			URL epURL = new URL(uri);
			SOAPMessage response = con.call(reqMsg, epURL);
			NodeList children = response.getSOAPBody().getChildNodes();
			return (children);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
		}
		return null;
	}

	static HashMap<String, String> hash = new HashMap<String, String>();

	/**
	 * 
	 * @param currentNode
	 */
	public static void getChild(Node currentNode) {
		NodeList children = currentNode.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node current = children.item(i);
			String name = current.getLocalName();
			String value = current.getTextContent();

			if (current.getChildNodes() != null && current.getChildNodes().getLength() > 1) {
				getChild(current);
			} else {
				hash.put(name, value);
			}
		}
	}

	/**
	 * 
	 * @param children
	 * @param nameSearch
	 * @return
	 */
	public static NodeList getChildByName(NodeList children, String nameSearch) {
		NodeList response = null;
		for (int i = 0; i < children.getLength(); i++) {
			Node current = children.item(i);
			String name = current.getLocalName();

			if (name != null && name.equals(nameSearch)) {
				return current.getChildNodes();
			}

			if (current.getChildNodes() != null && current.getChildNodes().getLength() > 0) {
				if (current.getChildNodes().item(0).getLocalName() != null
						&& !current.getChildNodes().item(0).getLocalName().isEmpty()) {
					return getChildByName(current.getChildNodes(), nameSearch);
				}
			}
		}
		return response;
	}

	/**
	 * 
	 * @param children
	 * @param nameSearch
	 * @param response
	 */
	public static void getChildValueByName(NodeList children, String nameSearch, List<String> response) {
		int lon = children.getLength();
		for (int i = 0; i < lon; i++) {
			Node current = children.item(i);
			String name = current.getLocalName();
			String value = current.getTextContent();

			if (name != null && name.equals(nameSearch)) {
				response.add(value);
			}

			int numNodes = current.getChildNodes().getLength();
			if (current.getChildNodes() != null && numNodes > 0) {

				if (current.getChildNodes().item(0).getLocalName() != null
						&& !current.getChildNodes().item(0).getLocalName().isEmpty()) {
					getChildValueByName(current.getChildNodes(), nameSearch, response);
				}
			}
		}
	}

	/**
	 * 
	 * @param children
	 * @param nameSearch
	 * @param response
	 */
	public static void getChildValueByName(NodeList children, String nameSearch, StringBuilder response) {
		int lon = children.getLength();
		for (int i = 0; i < lon; i++) {
			Node current = children.item(i);
			String name = current.getLocalName();
			String value = current.getTextContent();

			if (name != null && name.equals(nameSearch)) {
				response.append(value);
			}

			int numNodes = current.getChildNodes().getLength();
			if (current.getChildNodes() != null && numNodes > 0) {

				if (current.getChildNodes().item(0).getLocalName() != null
						&& !current.getChildNodes().item(0).getLocalName().isEmpty()) {
					getChildValueByName(current.getChildNodes(), nameSearch, response);
				}
			}
		}
	}

	/**
	 * 
	 * @param children
	 * @param nameSearch
	 * @param parentName
	 * @param response
	 */
	public static void getChildValueByName(NodeList children, String nameSearch, String parentName,
			StringBuilder response) {
		int lon = children.getLength();
		for (int i = 0; i < lon; i++) {
			Node current = children.item(i);
			String parent = current.getParentNode().getLocalName();
			String name = current.getLocalName();
			String value = current.getTextContent();

			if (parent.equals(parentName) && name != null && name.equals(nameSearch)) {
				response.append(value);
			}

			int numNodes = current.getChildNodes().getLength();
			if (current.getChildNodes() != null && numNodes > 0) {

				if (current.getChildNodes().item(0).getLocalName() != null
						&& !current.getChildNodes().item(0).getLocalName().isEmpty()) {
					getChildValueByName(current.getChildNodes(), nameSearch, parentName, response);
				}
			}
		}
	}

	/**
	 * 
	 * @param children
	 * @param nameSearch
	 * @param parentName
	 * @param response
	 */
	public static void getChildValueByName(NodeList children, String nameSearch, String parentName,
			List<String> response) {
		int lon = children.getLength();
		for (int i = 0; i < lon; i++) {
			Node current = children.item(i);
			String parent = current.getParentNode().getLocalName();
			String name = current.getLocalName();
			String value = current.getTextContent();

			if (parent.equals(parentName) && name != null && name.equals(nameSearch)) {
				if (!response.contains(value)) {
					response.add(value);
				}
			}

			int numNodes = current.getChildNodes().getLength();
			if (current.getChildNodes() != null && numNodes > 0) {

				if (current.getChildNodes().item(0).getLocalName() != null
						&& !current.getChildNodes().item(0).getLocalName().isEmpty()) {
					getChildValueByName(current.getChildNodes(), nameSearch, parentName, response);
				}
			}
		}
	}

	/**
	 * 
	 * @param children
	 * @param nameSearch
	 * @param parentName
	 * @param response
	 */
	public static void getChildSectionsValueByName(NodeList children, String parentName, List<SectionDTO> response) {
		int lon = children.getLength();
		List<SectionDTO> action = new ArrayList<>();
		SectionDTO section = new SectionDTO();
		SectionDTO actionSect = new SectionDTO();
		for (int i = 0; i < lon; i++) {
			Node current = children.item(i);
			String parent = current.getParentNode().getLocalName();
			String name = current.getLocalName();
			String value = current.getTextContent();

			if (parent.equals(parentName) && name != null) {
				if (name.equals("actions")) {
					if (value != null && !value.isEmpty() && !value.equals("null")) {
						actionSect.setName(value);
						action.add(actionSect);
						actionSect = new SectionDTO();
					}
				}
				if (name.equals("name")) {
					if (value != null && !value.isEmpty() && !value.equals("null")) {
						section.setModule(value);
						section.setActions(action);
						action = new ArrayList<>();
					}
				}
				if (!existsSection(section, response)) {
					if (section != null && section.getModule() != null) {
						response.add(section);
					}

				}
			}

			int numNodes = current.getChildNodes().getLength();
			if (current.getChildNodes() != null && numNodes > 0) {

				if (current.getChildNodes().item(0).getLocalName() != null
						&& !current.getChildNodes().item(0).getLocalName().isEmpty()) {
					getChildSectionsValueByName(current.getChildNodes(), parentName, response);
				}
			}
		}
	}

	/**
	 * 
	 * @param section
	 * @param response
	 * @return
	 */
	public static boolean existsSection(SectionDTO section, List<SectionDTO> response) {
		for (SectionDTO sectionDTO : response) {

			if (section.getModule() != null && sectionDTO.getModule() != null) {
				if (section.getModule().equals(sectionDTO.getModule())) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * 
	 * @param currentNode
	 * @param exclude
	 */
	public static void getChild(Node currentNode, String[] exclude) {
		NodeList children = currentNode.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node current = children.item(i);
			String name = current.getLocalName();
			String value = current.getTextContent();

			if (current.getChildNodes() != null && current.getChildNodes().getLength() > 1) {
				getChild(current, exclude);
			} else {
				for (int j = 0; j < exclude.length; j++) {
					if (exclude[j].equals(name)) {
						getChild(current, exclude);
						return;
					}
				}
				hash.put(name, value);
			}
		}
	}
}
