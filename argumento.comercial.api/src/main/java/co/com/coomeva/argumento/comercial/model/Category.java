/**
* Classname: Category
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "GLD_CATEGORIA", schema = DatabaseUtil.SCHEMA)
public class Category {

	@Id
	@Column(name = "ID", length = 11)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NOMBRE", nullable = false, length = 60)
	private String name;

	@Column(name = "ESTADO", nullable = false, length = 4)
	private String state;

	public Category(Long id, String name, String state) {
		this.id = id;
		this.name = name;
		this.state = state;
	}

	public Category() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + ", state=" + state + "]";
	}

}