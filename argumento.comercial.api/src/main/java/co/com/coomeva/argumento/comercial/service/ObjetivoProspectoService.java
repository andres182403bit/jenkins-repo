/**
* InterfaseName: IAcObjetivoProspecto
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcObjetivosProspectoDTO;
import co.com.coomeva.argumento.comercial.model.ObjetivosProspecto;

public interface ObjetivoProspectoService {
	public List<ObjetivosProspecto> findAll();

	public AcObjetivosProspectoDTO save(AcObjetivosProspectoDTO acObjetivosProspectoDto);

	public ObjetivosProspecto findObjetivoProspecto(Long codObjetivo, Long codProspecto);

	public void deleteObjetivoProspecto(Long codProspecto);
}