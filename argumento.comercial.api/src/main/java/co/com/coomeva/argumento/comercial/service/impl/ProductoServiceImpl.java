/**
* Classname: AcProductoImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.AcProductoDTO;
import co.com.coomeva.argumento.comercial.dto.AcProductoObjetivoDTO;
import co.com.coomeva.argumento.comercial.model.Objetivo;
import co.com.coomeva.argumento.comercial.model.Producto;
import co.com.coomeva.argumento.comercial.model.ProductoObjetivo;
import co.com.coomeva.argumento.comercial.repository.ProductoRepository;
import co.com.coomeva.argumento.comercial.service.ObjetivoService;
import co.com.coomeva.argumento.comercial.service.ProductoService;
import co.com.coomeva.argumento.comercial.service.ProductoObjetivoService;

@Transactional
@Service
public class ProductoServiceImpl implements ProductoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductoServiceImpl.class);

	@Autowired
	private ProductoRepository productoRepository;
	@Autowired
	private ObjetivoService objetivoService;
	@Autowired
	private ProductoObjetivoService productoObjetivoService;

	/**
	 * Metodo que retorna todo los objetos AcProducto que existen
	 * 
	 * @return List<AcProducto>
	 */
	@Override
	public List<Producto> findAll() {
		return productoRepository.findAll();
	}

	/**
	 * Metodo que almacena un producto en la base de datos
	 * 
	 * @return AcProducto
	 */
	@Override
	public Producto save(Producto acProducto) {
		return productoRepository.save(acProducto);
	}

	/**
	 * Metodo para obtener los productos asociados a un objetivo
	 * 
	 * @param lstObjetivo
	 * @return lstObjetivosDto
	 */
	public List<AcProductoObjetivoDTO> getObjetivosProductos(List<AcObjetivoDTO> lstObjetivo) {
		List<AcObjetivoDTO> lstObjetivosDto = new ArrayList<>();
		List<AcProductoObjetivoDTO> lstProcuctoObjetivoDto = new ArrayList<>();
		try {
			lstObjetivosDto = lstObjetivo;
			for (AcObjetivoDTO acObjetivoDto : lstObjetivosDto) {
				Objetivo acObjetivo = this.objetivoService.findByCodigo(acObjetivoDto.getCodigo());
				List<ProductoObjetivo> lstProcuctoObjetivo = acObjetivo.getAcProductoObjetivoList();
				if (!lstProcuctoObjetivo.isEmpty()) {
					for (ProductoObjetivo acProductoObjetivo : lstProcuctoObjetivo) {
						lstProcuctoObjetivoDto.add(this.productoObjetivoService.transformToDTO(acProductoObjetivo));
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstProcuctoObjetivoDto;
	}

	/**
	 * Metodo para transformar una entidad en un objeto DTO
	 * 
	 * @param acProductoEt
	 * @return acProductoDTO
	 */
	public AcProductoDTO transformToDTO(Producto acProductoEt) {
		Producto acProducto = new Producto();
		AcProductoDTO acProductoDTO = new AcProductoDTO();
		try {
			acProducto = acProductoEt;
			acProductoDTO.setCaracteristica(acProducto.getCaracteristica());
			acProductoDTO.setCodigo(acProducto.getCodigo());
			acProductoDTO.setDescripcion(acProducto.getDescripcion());
			acProductoDTO.setNota(acProducto.getNota());
			acProductoDTO.setRequisitoDocumental(acProducto.getRequisitoDocumental());
			acProductoDTO.setSimulador(acProducto.getSimulador());
			acProductoDTO.setValor(acProducto.getValor());
			acProductoDTO.setVentaja(acProducto.getVentaja());

			return acProductoDTO;
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Metodo para tranformar un objeto DTO en una Entidad
	 * 
	 * @param acProducto
	 * @return acProductoEt
	 */
	public Producto transformToET(AcProductoDTO acProducto) {
		Producto acProductoEt = new Producto();
		AcProductoDTO acProductoDTO = new AcProductoDTO();
		try {
			acProductoDTO = acProducto;
			acProductoEt.setCaracteristica(acProductoDTO.getCaracteristica());
			acProductoEt.setCodigo(acProductoDTO.getCodigo());
			acProductoEt.setDescripcion(acProductoDTO.getDescripcion());
			acProductoEt.setNota(acProductoDTO.getNota());
			acProductoEt.setRequisitoDocumental(acProductoDTO.getRequisitoDocumental());
			acProductoEt.setSimulador(acProductoDTO.getSimulador());
			acProductoEt.setValor(acProductoDTO.getValor());
			acProductoEt.setVentaja(acProductoDTO.getVentaja());

			return acProductoEt;
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

}