/**
* Classname: ArgumentoComercialAppDbConfig
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.config.db;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "co.com.coomeva.argumento.comercial.repository")
public class ArgumentoComercialAppDbConfig {
	
	@Value("${argumento.comercial.datasource.jndi-name}")
	private String argumentoComercialJndiName;
    
    /**
	 * Metodo en donde se configura el DataSource
	 * de la aplicacion Argumento Comercial
	 * @return DataSource
	 */
	@Primary
	@Bean(name = "dataSource")
	public DataSource dataSource() {
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		return dataSourceLookup.getDataSource(argumentoComercialJndiName);
	}

	/**
	 * Metodo en el que se definen y configura
	 * las clases del modelo y unidad de persistencia
	 * @return factoryBean
	 */
	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setDataSource(dataSource());
		factoryBean.setPackagesToScan("co.com.coomeva.argumento.comercial.model");
		factoryBean.setJpaVendorAdapter(jpaVendorAdapter());
		factoryBean.setJpaProperties(jpaProperties());
		factoryBean.setPersistenceUnitName("argumentoComercialUnit");
		return factoryBean;
	}

	/**
	 * Metodo que retorna el HibernateJpaVendorAdapter
	 * @return HibernateJpaVendorAdapter
	 */
	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		return new HibernateJpaVendorAdapter();
	}

	/**
	 * Metodo para configurar el manejador de 
	 * transacciones
	 * @param entityManagerFactory
	 * @return PlatformTransactionManager
	 */
	@Primary
	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

	/**
	 * Metodo para definir los archivos de 
	 * propiedades
	 * 
	 * @return Properties
	 */
	private Properties jpaProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.id.new_generator_mappings", "false");
		properties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		properties.put("hibernate.show_sql", "true");
		return properties;
	}
}
