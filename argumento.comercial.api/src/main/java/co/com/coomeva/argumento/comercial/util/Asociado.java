/**
* Classname: Asociado
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Asociado", propOrder = { "barComercial", "barFamiliar", "barResidencia", "celular", "ciuComercial",
		"ciuFamiliar", "ciuResidencia", "codCiuComercial", "codCiuFamiliar", "codCiuResidencia", "codDepExpDocumento",
		"codEstadoCivil", "codLugExpDocumento", "codRetornoWS", "codSexo", "codTipDocumento", "codTipoCasa",
		"depExpDocumento", "descRetornoWS", "dirComercial", "dirCorrespondencia", "dirFamiliar", "dirResidencia",
		"email", "estadoCivil", "estrato", "extComercial", "faxComercial", "fecExpDocumento", "lugExpDocumento",
		"nombre1", "nombre2", "numDocumento", "numInt", "perMayCargo", "perMenCargo", "primerApellido",
		"segundoApellido", "sexo", "sistema", "telComercial", "telFamiliar", "telResidencia", "tipDocumento",
		"tipUbiDirCor", "tipoCasa", "usuario" })
@XmlSeeAlso({ AsociadoVida.class })
public class Asociado {

	@XmlElement(required = true, nillable = true)
	protected String barComercial;
	@XmlElement(required = true, nillable = true)
	protected String barFamiliar;
	@XmlElement(required = true, nillable = true)
	protected String barResidencia;
	@XmlElement(required = true, nillable = true)
	protected String celular;
	@XmlElement(required = true, nillable = true)
	protected String ciuComercial;
	@XmlElement(required = true, nillable = true)
	protected String ciuFamiliar;
	@XmlElement(required = true, nillable = true)
	protected String ciuResidencia;
	@XmlElement(required = true, nillable = true)
	protected String codCiuComercial;
	@XmlElement(required = true, nillable = true)
	protected String codCiuFamiliar;
	@XmlElement(required = true, nillable = true)
	protected String codCiuResidencia;
	protected long codDepExpDocumento;
	protected long codEstadoCivil;
	protected long codLugExpDocumento;
	protected long codRetornoWS;
	protected long codSexo;
	protected long codTipDocumento;
	protected long codTipoCasa;
	@XmlElement(required = true, nillable = true)
	protected String depExpDocumento;
	@XmlElement(required = true, nillable = true)
	protected String descRetornoWS;
	@XmlElement(required = true, nillable = true)
	protected String dirComercial;
	@XmlElement(required = true, nillable = true)
	protected String dirCorrespondencia;
	@XmlElement(required = true, nillable = true)
	protected String dirFamiliar;
	@XmlElement(required = true, nillable = true)
	protected String dirResidencia;
	@XmlElement(required = true, nillable = true)
	protected String email;
	@XmlElement(required = true, nillable = true)
	protected String estadoCivil;
	protected long estrato;
	@XmlElement(required = true, nillable = true)
	protected String extComercial;
	@XmlElement(required = true, nillable = true)
	protected String faxComercial;
	@XmlElement(required = true, nillable = true)
	protected String fecExpDocumento;
	@XmlElement(required = true, nillable = true)
	protected String lugExpDocumento;
	@XmlElement(required = true, nillable = true)
	protected String nombre1;
	@XmlElement(required = true, nillable = true)
	protected String nombre2;
	@XmlElement(required = true, nillable = true)
	protected String numDocumento;
	protected long numInt;
	protected long perMayCargo;
	protected long perMenCargo;
	@XmlElement(required = true, nillable = true)
	protected String primerApellido;
	@XmlElement(required = true, nillable = true)
	protected String segundoApellido;
	@XmlElement(required = true, nillable = true)
	protected String sexo;
	@XmlElement(required = true, nillable = true)
	protected String sistema;
	@XmlElement(required = true, nillable = true)
	protected String telComercial;
	@XmlElement(required = true, nillable = true)
	protected String telFamiliar;
	@XmlElement(required = true, nillable = true)
	protected String telResidencia;
	@XmlElement(required = true, nillable = true)
	protected String tipDocumento;
	@XmlElement(required = true, nillable = true)
	protected String tipUbiDirCor;
	@XmlElement(required = true, nillable = true)
	protected String tipoCasa;
	@XmlElement(required = true, nillable = true)
	protected String usuario;

	// Propiedades agregadas
	protected String razonSocial;
	protected String nombreCompleto;
	protected String descEstado;
	protected long age;
	protected boolean isValidateUpdateState;
	protected String kindOfPerson;
	protected boolean assignOrCreateAssoiate;
	protected boolean isShowOrHiddeCategory;
	protected String fecIngresoDate;
	protected String fecnacimientoDate;
	protected Long codeCategory;

	public Asociado()
	{
		this.barComercial= "";
		this.barFamiliar= "";
		this.barResidencia= "";
		this.celular= "";
		this.ciuComercial= "";
		this.ciuFamiliar= "";
		this.ciuResidencia= "";
		this.codCiuComercial= "";
		this.codCiuFamiliar= "";
		this.codCiuResidencia= "";
		this.depExpDocumento= "";
		this.descRetornoWS= "";
		this.dirComercial= "";
		this.dirCorrespondencia= "";
		this.dirFamiliar= "";
		this.dirResidencia= "";
		this.email= "";
		this.estadoCivil= "";
		this.extComercial= "";
		this.faxComercial= "";
		this.fecExpDocumento= "";
		this.lugExpDocumento= "";
		this.nombre1= "";
		this.nombre2= "";
		this.numDocumento= "";
		this.primerApellido= "";
		this.segundoApellido= "";
		this.sexo= "";
		this.sistema= "";
		this.telComercial= "";
		this.telFamiliar= "";
		this.telResidencia= "";
		this.tipDocumento= "";
		this.tipUbiDirCor= "";
		this.tipoCasa= "";
		this.usuario= "";
		this.razonSocial= "";
		this.nombreCompleto= "";
		this.descEstado= "";
		this.kindOfPerson= "";
		this.fecIngresoDate= "";
		this.fecnacimientoDate= "";
		this.estrato= new Long(0);
		this.codDepExpDocumento= new Long(0);
		this.codEstadoCivil= new Long(0);
		this.codLugExpDocumento= new Long(0);
		this.codRetornoWS= new Long(0);
		this.codSexo= new Long(0);
		this.codTipDocumento= new Long(0);
		this.codTipoCasa= new Long(0);
		this.numInt= new Long(0);
		this.perMayCargo= new Long(0);
		this.perMenCargo= new Long(0);
		this.age= new Long(0);
		this.codeCategory= new Long(0);
	}
	
	
	/**
	 * Obtiene el valor de la propiedad barComercial.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBarComercial() {
		return barComercial;
	}

	/**
	 * Define el valor de la propiedad barComercial.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBarComercial(String value) {
		this.barComercial = value;
	}

	/**
	 * Obtiene el valor de la propiedad barFamiliar.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBarFamiliar() {
		return barFamiliar;
	}

	/**
	 * Define el valor de la propiedad barFamiliar.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBarFamiliar(String value) {
		this.barFamiliar = value;
	}

	/**
	 * Obtiene el valor de la propiedad barResidencia.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBarResidencia() {
		return barResidencia;
	}

	/**
	 * Define el valor de la propiedad barResidencia.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBarResidencia(String value) {
		this.barResidencia = value;
	}

	/**
	 * Obtiene el valor de la propiedad celular.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCelular() {
		return celular;
	}

	/**
	 * Define el valor de la propiedad celular.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCelular(String value) {
		this.celular = value;
	}

	/**
	 * Obtiene el valor de la propiedad ciuComercial.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCiuComercial() {
		return ciuComercial;
	}

	/**
	 * Define el valor de la propiedad ciuComercial.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCiuComercial(String value) {
		this.ciuComercial = value;
	}

	/**
	 * Obtiene el valor de la propiedad ciuFamiliar.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCiuFamiliar() {
		return ciuFamiliar;
	}

	/**
	 * Define el valor de la propiedad ciuFamiliar.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCiuFamiliar(String value) {
		this.ciuFamiliar = value;
	}

	/**
	 * Obtiene el valor de la propiedad ciuResidencia.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCiuResidencia() {
		return ciuResidencia;
	}

	/**
	 * Define el valor de la propiedad ciuResidencia.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCiuResidencia(String value) {
		this.ciuResidencia = value;
	}

	/**
	 * Obtiene el valor de la propiedad codCiuComercial.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCodCiuComercial() {
		return codCiuComercial;
	}

	/**
	 * Define el valor de la propiedad codCiuComercial.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCodCiuComercial(String value) {
		this.codCiuComercial = value;
	}

	/**
	 * Obtiene el valor de la propiedad codCiuFamiliar.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCodCiuFamiliar() {
		return codCiuFamiliar;
	}

	/**
	 * Define el valor de la propiedad codCiuFamiliar.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCodCiuFamiliar(String value) {
		this.codCiuFamiliar = value;
	}

	/**
	 * Obtiene el valor de la propiedad codCiuResidencia.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCodCiuResidencia() {
		return codCiuResidencia;
	}

	/**
	 * Define el valor de la propiedad codCiuResidencia.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCodCiuResidencia(String value) {
		this.codCiuResidencia = value;
	}

	public long getCodDepExpDocumento() {
		return codDepExpDocumento;
	}
	
	public void setCodDepExpDocumento(long value) {
		this.codDepExpDocumento = value;
	}

	public long getCodEstadoCivil() {
		return codEstadoCivil;
	}

	public void setCodEstadoCivil(long value) {
		this.codEstadoCivil = value;
	}

	public long getCodLugExpDocumento() {
		return codLugExpDocumento;
	}

	public void setCodLugExpDocumento(long value) {
		this.codLugExpDocumento = value;
	}

	public long getCodRetornoWS() {
		return codRetornoWS;
	}

	public void setCodRetornoWS(long value) {
		this.codRetornoWS = value;
	}

	public long getCodSexo() {
		return codSexo;
	}

	public void setCodSexo(long value) {
		this.codSexo = value;
	}

	public long getCodTipDocumento() {
		return codTipDocumento;
	}

	public void setCodTipDocumento(long value) {
		this.codTipDocumento = value;
	}

	public long getCodTipoCasa() {
		return codTipoCasa;
	}

	public void setCodTipoCasa(long value) {
		this.codTipoCasa = value;
	}

	public String getDepExpDocumento() {
		return depExpDocumento;
	}

	public void setDepExpDocumento(String value) {
		this.depExpDocumento = value;
	}

	public String getDescRetornoWS() {
		return descRetornoWS;
	}

	
	public void setDescRetornoWS(String value) {
		this.descRetornoWS = value;
	}

	public String getDirComercial() {
		return dirComercial;
	}

	public void setDirComercial(String value) {
		this.dirComercial = value;
	}

	public String getDirCorrespondencia() {
		return dirCorrespondencia;
	}

	public void setDirCorrespondencia(String value) {
		this.dirCorrespondencia = value;
	}

	public String getDirFamiliar() {
		return dirFamiliar;
	}

	public void setDirFamiliar(String value) {
		this.dirFamiliar = value;
	}

	public String getDirResidencia() {
		return dirResidencia;
	}

	public void setDirResidencia(String value) {
		this.dirResidencia = value;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String value) {
		this.email = value;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String value) {
		this.estadoCivil = value;
	}

	public long getEstrato() {
		return estrato;
	}

	public void setEstrato(long value) {
		this.estrato = value;
	}

	public String getExtComercial() {
		return extComercial;
	}

	public void setExtComercial(String value) {
		this.extComercial = value;
	}

	public String getFaxComercial() {
		return faxComercial;
	}

	public void setFaxComercial(String value) {
		this.faxComercial = value;
	}

	public String getFecExpDocumento() {
		return fecExpDocumento;
	}

	public void setFecExpDocumento(String value) {
		this.fecExpDocumento = value;
	}

	public String getLugExpDocumento() {
		return lugExpDocumento;
	}

	public void setLugExpDocumento(String value) {
		this.lugExpDocumento = value;
	}

	public String getNombre1() {
		return nombre1;
	}

	public void setNombre1(String value) {
		this.nombre1 = value;
	}

	public String getNombre2() {
		return nombre2;
	}

	public void setNombre2(String value) {
		this.nombre2 = value;
	}

	public String getNumDocumento() {
		return numDocumento;
	}

	public void setNumDocumento(String value) {
		this.numDocumento = value;
	}

	public long getNumInt() {
		return numInt;
	}

	public void setNumInt(long value) {
		this.numInt = value;
	}

	public long getPerMayCargo() {
		return perMayCargo;
	}

	public void setPerMayCargo(long value) {
		this.perMayCargo = value;
	}

	public long getPerMenCargo() {
		return perMenCargo;
	}

	public void setPerMenCargo(long value) {
		this.perMenCargo = value;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String value) {
		this.primerApellido = value;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String value) {
		this.segundoApellido = value;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String value) {
		this.sexo = value;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String value) {
		this.sistema = value;
	}

	public String getTelComercial() {
		return telComercial;
	}

	public void setTelComercial(String value) {
		this.telComercial = value;
	}

	public String getTelFamiliar() {
		return telFamiliar;
	}

	public void setTelFamiliar(String value) {
		this.telFamiliar = value;
	}

	public String getTelResidencia() {
		return telResidencia;
	}

	public void setTelResidencia(String value) {
		this.telResidencia = value;
	}

	public String getTipDocumento() {
		return tipDocumento;
	}

	public void setTipDocumento(String value) {
		this.tipDocumento = value;
	}

	public String getTipUbiDirCor() {
		return tipUbiDirCor;
	}

	public void setTipUbiDirCor(String value) {
		this.tipUbiDirCor = value;
	}

	public String getTipoCasa() {
		return tipoCasa;
	}

	public void setTipoCasa(String value) {
		this.tipoCasa = value;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String value) {
		this.usuario = value;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getDescEstado() {
		return descEstado;
	}

	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	public long getAge() {
		return age;
	}

	public void setAge(long age) {
		this.age = age;
	}

	public boolean isValidateUpdateState() {
		return isValidateUpdateState;
	}

	public void setValidateUpdateState(boolean isValidateUpdateState) {
		this.isValidateUpdateState = isValidateUpdateState;
	}

	public String getKindOfPerson() {
		return kindOfPerson;
	}

	public void setKindOfPerson(String kindOfPerson) {
		this.kindOfPerson = kindOfPerson;
	}

	public boolean isAssignOrCreateAssoiate() {
		return assignOrCreateAssoiate;
	}

	public void setAssignOrCreateAssoiate(boolean assignOrCreateAssoiate) {
		this.assignOrCreateAssoiate = assignOrCreateAssoiate;
	}

	public boolean isShowOrHiddeCategory() {
		return isShowOrHiddeCategory;
	}

	public void setShowOrHiddeCategory(boolean isShowOrHiddeCategory) {
		this.isShowOrHiddeCategory = isShowOrHiddeCategory;
	}

	public String getFecIngresoDate() {
		return fecIngresoDate;
	}

	public void setFecIngresoDate(String fecIngresoDate) {
		this.fecIngresoDate = fecIngresoDate;
	}

	public String getFecnacimientoDate() {
		return fecnacimientoDate;
	}

	public void setFecnacimientoDate(String fecnacimientoDate) {
		this.fecnacimientoDate = fecnacimientoDate;
	}

	public Long getCodeCategory() {
		return codeCategory;
	}

	public void setCodeCategory(Long codeCategory) {
		this.codeCategory = codeCategory;
	}

}
