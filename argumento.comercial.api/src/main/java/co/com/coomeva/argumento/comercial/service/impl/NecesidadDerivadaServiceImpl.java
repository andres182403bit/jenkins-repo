/**
* Classname: AcNecesidadDerivadaImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcNecesidadDerivadaDTO;
import co.com.coomeva.argumento.comercial.model.NecesidadDerivada;
import co.com.coomeva.argumento.comercial.service.NecesidadDerivadaService;

@Transactional
@Service
public class NecesidadDerivadaServiceImpl implements NecesidadDerivadaService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NecesidadDerivadaServiceImpl.class);

	/**
	 * transforma una entidad en un objeto DTO
	 * 
	 * @param acNecesidadDerivada
	 * @return acNecesidadDerivadaDTO
	 */
	public AcNecesidadDerivadaDTO transformToDTO(NecesidadDerivada acNecesidadDerivada) {
		AcNecesidadDerivadaDTO acNecesidadDerivadaDTO = new AcNecesidadDerivadaDTO();
		NecesidadDerivada acNecesidadDerivadaET = new NecesidadDerivada();
		try {
			acNecesidadDerivadaET = acNecesidadDerivada;
			acNecesidadDerivadaDTO.setCodigo(acNecesidadDerivadaET.getCodigo());
			acNecesidadDerivadaDTO.setDescripcion(acNecesidadDerivadaET.getDescripcion());
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}

		return acNecesidadDerivadaDTO;
	}

	/**
	 * metodo que transforma un DTO en una entidad
	 * 
	 * @param acNecesidadDerivada
	 * @return acNecesidadDerivadaET
	 */
	public NecesidadDerivada transformToET(AcNecesidadDerivadaDTO acNecesidadDerivada) {
		AcNecesidadDerivadaDTO acNecesidadDerivadaDTO = new AcNecesidadDerivadaDTO();
		NecesidadDerivada acNecesidadDerivadaET = new NecesidadDerivada();
		try {
			acNecesidadDerivadaDTO = acNecesidadDerivada;
			acNecesidadDerivadaET.setCodigo(acNecesidadDerivadaDTO.getCodigo());
			acNecesidadDerivadaET.setDescripcion(acNecesidadDerivadaDTO.getDescripcion());
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acNecesidadDerivadaET;
	}

}
