/**
* Classname: SOAPClientSAAJ
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SOAPClientSAAJ {

	private static final Logger LOGGER = LoggerFactory.getLogger(SOAPClientSAAJ.class);

	private SOAPClientSAAJ() {

	}

	/**
	 * Método que consume el servicio SOAP y su acción
	 * 
	 * @param envelope    -- Esquema del request en xml
	 * @param urlEndPoint -- Url del EndPoint
	 * @return response
	 */
	public static SOAPMessage consumeSOAP(String envelope, String urlEndPoint) {
		SOAPMessage response = null;
		try {
			SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
			SOAPConnection connection = sfc.createConnection();
			InputStream is = new ByteArrayInputStream(envelope.getBytes());
			SOAPMessage request = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL)
					.createMessage(new MimeHeaders(), is);
			request.removeAllAttachments();
			MimeHeaders headers = request.getMimeHeaders();
			headers.addHeader("SOAPAction", "http://schemas.xmlsoap.org/soap/http");
			URL endpoint = new URL(urlEndPoint);
			response = connection.call(request, endpoint);
			response.getSOAPBody();
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
		}
		return response;
	}

}