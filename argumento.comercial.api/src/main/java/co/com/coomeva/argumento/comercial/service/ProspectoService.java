/**
 *	Interfasename: IAcProspecto
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcProspectoDTO;
import co.com.coomeva.argumento.comercial.model.Prospecto;

public interface ProspectoService {
	public Prospecto findByCodigo(Long codigo);

	public Prospecto findByIdentificacion(String identificacion);

	public List<Prospecto> findAll();

	public AcProspectoDTO save(AcProspectoDTO prospecto);

	public AcProspectoDTO findByTipoIdentificacionIdentificacion(Short tipoIdentificacion, String identificacion);

	public AcProspectoDTO transformToDTO(Prospecto prospecto);

	public Prospecto transformToET(AcProspectoDTO prospecto);
}