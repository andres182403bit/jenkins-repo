/**
 *	Classname: AcObjetivoDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

import java.util.List;

public class AcObjetivoDTO {
	private Long codigo;
	private String descripcion;
	private List<AcNecesidadDerivadaDTO> lstNecesodadDerivadaDto;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<AcNecesidadDerivadaDTO> getLstNecesodadDerivadaDto() {
		return lstNecesodadDerivadaDto;
	}

	public void setLstNecesodadDerivadaDto(List<AcNecesidadDerivadaDTO> lstNecesodadDerivadaDto) {
		this.lstNecesodadDerivadaDto = lstNecesodadDerivadaDto;
	}
}
