/**
* InterfaseName: IAcParametro
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service;

import co.com.coomeva.argumento.comercial.model.Parametro;

public interface ParametroService {
	public Parametro findByName(String nombre);
}
