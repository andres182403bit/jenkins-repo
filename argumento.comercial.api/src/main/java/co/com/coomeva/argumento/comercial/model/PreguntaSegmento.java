/**
* Classname: AcPreguntaSegmento
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_PREGUNTA_SEGMENTO", schema = DatabaseUtil.SCHEMA)
public class PreguntaSegmento implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_PREGUNTA_SEGMENTO_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_PREGUNTA_SEGMENTO_SEQ", sequenceName = "AC_PREGUNTA_SEGMENTO_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@JoinColumn(name = "CODIGO_PREGUNTA", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	private Pregunta codigoPregunta;
	@JoinColumn(name = "CODIGO_SEGMENTO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	private Segmento codigoSegmento;

	public PreguntaSegmento() {
	}

	public PreguntaSegmento(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Pregunta getCodigoPregunta() {
		return codigoPregunta;
	}

	public void setCodigoPregunta(Pregunta codigoPregunta) {
		this.codigoPregunta = codigoPregunta;
	}

	public Segmento getCodigoSegmento() {
		return codigoSegmento;
	}

	public void setCodigoSegmento(Segmento codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof PreguntaSegmento)) {
			return false;
		}
		PreguntaSegmento other = (PreguntaSegmento) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcPreguntaSegmento[ codigo=" + codigo + " ]";
	}

}
