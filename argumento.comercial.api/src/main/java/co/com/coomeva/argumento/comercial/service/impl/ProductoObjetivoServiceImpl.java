/**
* Classname: AcProductoObjetivoImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcProductoObjetivoDTO;
import co.com.coomeva.argumento.comercial.model.ProductoObjetivo;
import co.com.coomeva.argumento.comercial.repository.ProductoObjetivoRepository;
import co.com.coomeva.argumento.comercial.service.ObjetivoService;
import co.com.coomeva.argumento.comercial.service.ProductoService;
import co.com.coomeva.argumento.comercial.service.ProductoObjetivoService;

@Transactional
@Service
public class ProductoObjetivoServiceImpl implements ProductoObjetivoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductoObjetivoServiceImpl.class);

	@Autowired
	private ProductoObjetivoRepository productoObjetivoRepository;
	@Autowired
	private ObjetivoService objetivoService;
	@Autowired
	private ProductoService productoService;

	/**
	 * Metodo que retorna todos los objetos AcProductoObjetivo que existan en la
	 * base de datos
	 * 
	 * @return List<AcProductoObjetivo>
	 */
	@Override
	public List<ProductoObjetivo> findAll() {
		return productoObjetivoRepository.findAll();
	}

	/**
	 * Metodo que almacena un AcProductoObjetivo
	 * 
	 * @param acProductoObjetivo
	 * @return acProductoObjetivoDto
	 */
	@Override
	public AcProductoObjetivoDTO save(AcProductoObjetivoDTO acProductoObjetivoDto) {
		ProductoObjetivo acProductoObjetivoET = new ProductoObjetivo();
		try {
			acProductoObjetivoET = this.productoObjetivoRepository.save(this.transformToET(acProductoObjetivoDto));
			acProductoObjetivoDto = this.transformToDTO(acProductoObjetivoET);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acProductoObjetivoDto;
	}

	/**
	 * Metodo que tranforma un Objeto DTO en una entidad
	 * 
	 * @param acProductoObjetivo
	 * @return acProductoObjetivoET
	 */
	public ProductoObjetivo transformToET(AcProductoObjetivoDTO acProductoObjetivo) {
		AcProductoObjetivoDTO acProductoObjetivoDto = new AcProductoObjetivoDTO();
		ProductoObjetivo acProductoObjetivoET = new ProductoObjetivo();
		try {
			acProductoObjetivoDto = acProductoObjetivo;
			acProductoObjetivoET.setCodigo(acProductoObjetivoDto.getCodigo());
			acProductoObjetivoET.setBeneficio(acProductoObjetivoDto.getBeneficio());
			acProductoObjetivoET
					.setCodigoObjetivo(this.objetivoService.transformToET(acProductoObjetivoDto.getAcObjetivoDto()));
			acProductoObjetivoET
					.setCodigoProducto(this.productoService.transformToET(acProductoObjetivoDto.getAcProductoDto()));
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acProductoObjetivoET;
	}

	/**
	 * Metodo que transforma un objeto entidad en un DTO
	 * 
	 * @param acProductoObjetivo
	 * @return acProductoObjetivoDto
	 */
	public AcProductoObjetivoDTO transformToDTO(ProductoObjetivo acProductoObjetivo) {
		AcProductoObjetivoDTO acProductoObjetivoDto = new AcProductoObjetivoDTO();
		ProductoObjetivo acProductoObjetivoET = new ProductoObjetivo();
		try {
			acProductoObjetivoET = acProductoObjetivo;
			acProductoObjetivoDto.setCodigo(acProductoObjetivoET.getCodigo());
			acProductoObjetivoDto.setBeneficio(acProductoObjetivoET.getBeneficio());
			acProductoObjetivoDto
					.setAcObjetivoDto(this.objetivoService.transformToDTO(acProductoObjetivoET.getCodigoObjetivo()));
			acProductoObjetivoDto
					.setAcProductoDto(this.productoService.transformToDTO(acProductoObjetivoET.getCodigoProducto()));
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acProductoObjetivoDto;
	}
}