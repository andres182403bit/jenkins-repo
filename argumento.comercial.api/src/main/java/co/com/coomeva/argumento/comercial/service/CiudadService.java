/**
* InterfaseName: IAcCiudad
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcCiudadDTO;
import co.com.coomeva.argumento.comercial.model.Ciudad;

public interface CiudadService {
	public List<Ciudad> consultarCiudades();

	public List<AcCiudadDTO> findAllDto();

	public List<Ciudad> findByDepartamento(Long codigoDepartamento);

	public AcCiudadDTO transformToDto(Ciudad acCiudad);

	public Ciudad transformToET(AcCiudadDTO acCiudadDto);
}
