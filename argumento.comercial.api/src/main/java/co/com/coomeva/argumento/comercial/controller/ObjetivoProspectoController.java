package co.com.coomeva.argumento.comercial.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcObjetivosProspectoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.ObjetivoProspectoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo de los objetivos del prospecto en la
 * encuesta.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/objetivoprospecto" })
public class ObjetivoProspectoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ObjetivoProspectoController.class);

	@Autowired
	private ObjetivoProspectoService objetivoProspectoService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que se encarga de almacenar los objetivos del prospecto en la
	 * encuesta.
	 * 
	 * @param objetivoProspectoDTO Objetivo seleccionado
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/save")
	public ResponseService save(@RequestBody AcObjetivosProspectoDTO objetivoProspectoDTO) {
		ResponseService responseService = new ResponseService();
		try {
			AcObjetivosProspectoDTO objetivoProspectoResponseDTO = this.objetivoProspectoService
					.save(objetivoProspectoDTO);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"),
					objetivoProspectoResponseDTO);
		} catch (Exception e) {
			LOGGER.error("Error ObjetivoProspectoController::save: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}
}