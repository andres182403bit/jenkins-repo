/**
 *	Classname: LoginDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

public class LoginDTO {
	private String login;
	private String password;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}