/**
* InterfaseName: IAcRespuestaRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.coomeva.argumento.comercial.model.Prospecto;
import co.com.coomeva.argumento.comercial.model.Respuesta;

public interface RespuestaRepository extends JpaRepository<Respuesta, Integer> {

	public Respuesta findByCodigo(@Param("codigo") Long codigo);

	@Query("SELECT a FROM Respuesta a WHERE a.codigoPregunta =:codigoPregunta and a.codigoProspecto=:codigoProspecto")
	public Respuesta findByCodigoPreguntaAndCodigoProspecto(@Param("codigoPregunta") String codigoPregunta,
			@Param("codigoProspecto") Prospecto codigoProspecto);
}
