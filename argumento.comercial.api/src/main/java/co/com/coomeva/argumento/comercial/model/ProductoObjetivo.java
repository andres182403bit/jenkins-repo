/**
* Classname: AcProducto
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_PRODUCTO_OBJETIVO", schema = DatabaseUtil.SCHEMA)
public class ProductoObjetivo implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_PRODUCTO_OBJETIVO_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_PRODUCTO_OBJETIVO_SEQ", sequenceName = "AC_PRODUCTO_OBJETIVO_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Size(max = 1000)
	@Column(name = "BENEFICIO")
	private String beneficio;
	@JoinColumn(name = "CODIGO_OBJETIVO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	@JsonBackReference
	private Objetivo codigoObjetivo;
	@JoinColumn(name = "CODIGO_PRODUCTO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	private Producto codigoProducto;

	public ProductoObjetivo() {
	}

	public ProductoObjetivo(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getBeneficio() {
		return beneficio;
	}

	public void setBeneficio(String beneficio) {
		this.beneficio = beneficio;
	}

	public Objetivo getCodigoObjetivo() {
		return codigoObjetivo;
	}

	public void setCodigoObjetivo(Objetivo codigoObjetivo) {
		this.codigoObjetivo = codigoObjetivo;
	}

	public Producto getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(Producto codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ProductoObjetivo)) {
			return false;
		}
		ProductoObjetivo other = (ProductoObjetivo) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcProductoObjetivo[ codigo=" + codigo + " ]";
	}

}
