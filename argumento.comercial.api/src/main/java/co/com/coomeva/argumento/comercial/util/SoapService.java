/**
* Classname: SoapService
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import org.w3c.dom.NodeList;
 
public class SoapService {

	/**
	 * 
	 * @param login
	 * @param password
	 * @param app
	 * @param url
	 * @return
	 */
	public NodeList validarProfile(String login, String password, String app, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ges=\"http://geside.coomeva.com.co\">\n" +
        		"   <soapenv:Header/>\n" +
        		"   <soapenv:Body>\n" +
        		"      <ges:validateUserApp>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:directory>2</ges:directory>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:userName>"+login+"</ges:userName>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:password>"+password+"</ges:password>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:validaAplicacion>1</ges:validaAplicacion>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:app>"+app+"</ges:app>\n" +
        		"      </ges:validateUserApp>\n" +
        		"   </soapenv:Body>\n" +
        		"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
		
	/**
	 * 
	 * @param login
	 * @param password
	 * @param recipient
	 * @param message
	 * @param url
	 * @return
	 */
	public NodeList sendSim(String login, String password, String recipient, String message, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://services.sms.service.coomeva.com.co/\">\n" +
        		"   <soapenv:Header/>\n" +
        		"   <soapenv:Body>\n" +
        		"      <ser:sendMessage>\n" +
        		"         <smsInfo>\n" +
        		"            <username>"+ login +"</username>\n" +
        		"            <password>"+ password +"</password>\n" +
        		"            <recipient>"+ recipient +"</recipient>\n" +
        		"            <messagedata>"+ message +"</messagedata>\n" +
        		"            <longMessage>true</longMessage>\n" +
        		"         </smsInfo>\n" +
        		"      </ser:sendMessage>\n" +
        		"   </soapenv:Body>\n" +
        		"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
	
	/**
	 * 
	 * @param tipoDocumento
	 * @param documento
	 * @param url
	 * @return
	 */
	public NodeList generateToken(String tipoDocumento, String documento, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tok=\"http://token.coomeva.com.co/\">\n" +
        		"   <soapenv:Header/>\n" +
        		"   <soapenv:Body>\n" +
        		"      <tok:generateToken>\n" +
        		"         <!--Optional:-->\n" +
        		"         <arg0>" + tipoDocumento + "</arg0>\n" +
        		"         <!--Optional:-->\n" +
        		"         <arg1>" + documento + "</arg1>\n" +
        		"      </tok:generateToken>\n" +
        		"   </soapenv:Body>\n" +
        		"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
	
	/**
	 * 
	 * @param tipoDocumento
	 * @param documento
	 * @param token
	 * @param url
	 * @return
	 */
	public NodeList validateToken(String tipoDocumento, String documento, String token, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tok=\"http://token.coomeva.com.co/\">\n" +
						"   <soapenv:Header/>\n" +
						"   <soapenv:Body>\n" +
						"      <tok:validateToken>\n" +
						"         <!--Optional:-->\n" +
						"         <arg0>" + tipoDocumento + "</arg0>\n" +
						"         <!--Optional:-->\n" +
						"         <arg1>" + documento + "</arg1>\n" +
						"         <!--Optional:-->\n" +
						"         <arg2>" + token + "</arg2>\n" +
						"      </tok:validateToken>\n" +
						"   </soapenv:Body>\n" +
						"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
	
	/**
	 * 
	 * @param direccion
	 * @param url
	 * @return
	 */
	public NodeList validationServiceDireccion(String direccion, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://www.coomeva.com.co/GoldenService/Validation\">\n" +
        		"   <soapenv:Header/>\n" +
        		"   <soapenv:Body>\n" +
        		"      <val:validarDireccion>\n" +
        		"         <!--Optional:-->\n" +
        		"         <direccion>"+direccion+"</direccion>\n" +
        		"      </val:validarDireccion>\n" +
        		"   </soapenv:Body>\n" +
        		"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
	
	/**
	 * 
	 * @param correo
	 * @param url
	 * @return
	 */
	public NodeList validationServiceCorreo(String correo, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://www.coomeva.com.co/GoldenService/Validation\">\n" +
		"   <soapenv:Header/>\n" +
		"   <soapenv:Body>\n" +
		"      <val:validarCorreo>\n" +
		"         <!--Optional:-->\n" +
		"         <correoElectronico>"+correo+"</correoElectronico>\n" +
		"      </val:validarCorreo>\n" +
		"   </soapenv:Body>\n" +
		"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
	
	/**
	 * 
	 * @param correo
	 * @param url
	 * @return
	 */
	public NodeList validationServiceTelefono(String telefono, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://www.coomeva.com.co/GoldenService/Validation\">\n" +
		"   <soapenv:Header/>\n" +
		"   <soapenv:Body>\n" +
		"      <val:validarNumeroTelefono>\n" +
		"         <!--Optional:-->\n" +
		"         <numeroTelefono>"+telefono+"</numeroTelefono>\n" +
		"      </val:validarNumeroTelefono>\n" +
		"   </soapenv:Body>\n" +
		"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
	
	/**
	 * 
	 * @param correo
	 * @param url
	 * @return
	 */
	public NodeList validationServiceCel(String cel, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://www.coomeva.com.co/GoldenService/Validation\">\n" +
		"   <soapenv:Header/>\n" +
		"   <soapenv:Body>\n" +
		"      <val:validarNumeroCelular>\n" +
		"         <!--Optional:-->\n" +
		"         <numeroCelular>"+cel+"</numeroCelular>\n" +
		"      </val:validarNumeroCelular>\n" +
		"   </soapenv:Body>\n" +
		"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
	
	/**
	 * 
	 * @param direccion
	 * @param url
	 * @return
	 */
	public NodeList transformationDireccionService(String direccion, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tran=\"http://www.coomeva.com.co/GoldenService/Transformation\">\n" +
					"   <soapenv:Header/>\n" +
					"   <soapenv:Body>\n" +
					"      <tran:transformarDireccion>\n" +
					"         <!--Optional:-->\n" +
					"         <direccion>"+direccion+"</direccion>\n" +
					"      </tran:transformarDireccion>\n" +
					"   </soapenv:Body>\n" +
					"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
}
