/**
* Classname: AcSegmentoImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcSegmentoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.model.Pregunta;
import co.com.coomeva.argumento.comercial.model.PreguntaSegmento;
import co.com.coomeva.argumento.comercial.model.Prospecto;
import co.com.coomeva.argumento.comercial.model.Segmento;
import co.com.coomeva.argumento.comercial.repository.PreguntaRepository;
import co.com.coomeva.argumento.comercial.repository.PreguntaSegmentoRepository;
import co.com.coomeva.argumento.comercial.repository.SegmentoRepository;
import co.com.coomeva.argumento.comercial.service.SegmentoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

@Transactional
@Service
public class SegmentoServiceImpl implements SegmentoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SegmentoServiceImpl.class);

	@Autowired
	private SegmentoRepository segmentoRepository;

	@Autowired
	private PreguntaRepository preguntaRepository;

	@Autowired
	private PreguntaSegmentoRepository preguntaSegmentoRepository;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Metodo que retorna todo los objetos AcSegmento que existen en la base de
	 * datos
	 * 
	 * @return List<AcSegmento>
	 */
	@Override
	public List<Segmento> findAll() {
		return segmentoRepository.findAll();
	}

	/**
	 * Metodo que busca un segmento por nombre
	 * 
	 * @param nombre
	 * @return AcSegmento
	 */
	@Override
	public Segmento findByNombre(String nombre) {
		return segmentoRepository.findByNombre(nombre);
	}

	/**
	 * metodo para asignar segmento a un prospecto
	 * 
	 * @param prospect
	 * @return AcSegmento
	 */
	public Segmento defineSegment(Prospecto prospectSegmented) {
		Segmento segmentAsigned = new Segmento();
		try {
			Short menoresACargo = 1;
			if (prospectSegmented.getPersonaACargo().equals("N")) {
				menoresACargo = 2;
			}

			segmentAsigned = this.findSegment(Short.parseShort(prospectSegmented.getEdad()),
					prospectSegmented.getGenero(), menoresACargo);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return segmentAsigned;
	}

	/**
	 * Metodo que determina el segmento al que pertenece un prospecto
	 * 
	 * @param prospect
	 * @return segment
	 */
	@Override
	public Segmento getSegmento(Prospecto prospect) {
		Segmento segment = new Segmento();
		try {
			segment = this.defineSegment(prospect);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
		}
		return segment;
	}

	/**
	 * Metodo que transforma una entidad en un objeto DTO
	 * 
	 * @param acSegmentoTransform
	 * @return acSegmentoDTO
	 */
	public AcSegmentoDTO transformToDTO(Segmento acSegmentoTransform) {
		AcSegmentoDTO acSegmentoDTO = new AcSegmentoDTO();
		try {
			acSegmentoDTO.setCodigo(acSegmentoTransform.getCodigo());
			acSegmentoDTO.setDescripcion(acSegmentoTransform.getDescripcion());
			acSegmentoDTO.setEdadInicio(acSegmentoTransform.getEdadInicio());
			acSegmentoDTO.setEdadFin(acSegmentoTransform.getEdadFin());
			acSegmentoDTO.setGenero(acSegmentoTransform.getGenero());
			acSegmentoDTO.setMenoresCargo(acSegmentoTransform.getMenoresCargo());
			acSegmentoDTO.setNombre(acSegmentoTransform.getNombre());

			String genero = "";
			if (acSegmentoDTO.getGenero() == 1) {
				genero = "Masculino";
			}

			if (acSegmentoDTO.getGenero() == 2) {
				genero = "Femenino";
			}

			if (acSegmentoDTO.getGenero() == 3) {
				genero = "Indiferente";
			}

			acSegmentoDTO.setStrGenero(genero);

			String menoresCargo = "";
			if (acSegmentoDTO.getMenoresCargo() == 1) {
				menoresCargo = "Si";
			}

			if (acSegmentoDTO.getMenoresCargo() == 2) {
				menoresCargo = "No";
			}

			if (acSegmentoDTO.getMenoresCargo() == 3) {
				menoresCargo = "Indiferente";
			}

			acSegmentoDTO.setStrMenorCargo(menoresCargo);

		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acSegmentoDTO;
	}

	/**
	 * Metodo que tranforma un DTO en una Entidad
	 * 
	 * @param acSegmentoDTO
	 * @return acSegmentoTransform
	 */
	public Segmento transformToET(AcSegmentoDTO acSegmentoDTO) {
		Segmento acSegmentoTransform = new Segmento();
		try {
			acSegmentoTransform.setCodigo(acSegmentoDTO.getCodigo());
			acSegmentoTransform.setDescripcion(acSegmentoDTO.getDescripcion());
			acSegmentoTransform.setEdadInicio(acSegmentoDTO.getEdadInicio());
			acSegmentoTransform.setEdadFin(acSegmentoDTO.getEdadFin());
			acSegmentoTransform.setGenero(acSegmentoDTO.getGenero());
			acSegmentoTransform.setMenoresCargo(acSegmentoDTO.getMenoresCargo());
			acSegmentoTransform.setNombre(acSegmentoDTO.getNombre());
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acSegmentoTransform;
	}

	/**
	 * Metodo que busca un segmento dependiendo de los parametros recibidos
	 * 
	 * @param edad
	 * @param genero
	 * @param menoresCargo
	 */
	@Override
	public Segmento findSegment(Short edad, Short genero, Short menoresCargo) {
		Segmento segment = new Segmento();
		try {
			segment = this.segmentoRepository.findSegment(edad, Short.parseShort("3"), Short.parseShort("3"));

			if (segment == null) {
				segment = this.segmentoRepository.findSegment(edad, Short.parseShort("3"), menoresCargo);
			}

			if (segment == null) {
				segment = this.segmentoRepository.findSegment(edad, genero, menoresCargo);
			}

		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return segment;
	}

	/**
	 * Metodo que busca un AcSegmento por codigo
	 * 
	 * @param codigo
	 * @return segment
	 */
	@Override
	public Segmento findByCodigo(Long codigo) {
		Segmento segment = new Segmento();
		try {
			segment = this.segmentoRepository.findByCodigo(codigo);
			return segment;
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Metodo que almacena un segmento en base de datos
	 * 
	 * @param acSegmantoDto
	 * @return acSegmantoDto
	 */
	@Override
	public AcSegmentoDTO save(AcSegmentoDTO acSegmantoDto) {
		Segmento acSegmentoEt;
		try {
			acSegmentoEt = segmentoRepository.validateSegment(acSegmantoDto.getEdadInicio(), acSegmantoDto.getEdadFin(),
					acSegmantoDto.getGenero(), acSegmantoDto.getMenoresCargo());

			// Se valida que no exista un segmento con las mismas condiciones
			if (acSegmentoEt != null && !acSegmentoEt.getCodigo().equals(acSegmantoDto.getCodigo())) {
				return null;
			}

			if (acSegmantoDto.getCodigo() != null) {
				acSegmentoEt = this.findByCodigo(acSegmantoDto.getCodigo());
				acSegmentoEt.setDescripcion(acSegmantoDto.getDescripcion());
				acSegmentoEt.setNombre(acSegmantoDto.getNombre());
				acSegmentoEt.setEdadInicio(acSegmantoDto.getEdadInicio());
				acSegmentoEt.setEdadFin(acSegmantoDto.getEdadFin());
				acSegmentoEt.setGenero(acSegmantoDto.getGenero());
				acSegmentoEt.setMenoresCargo(acSegmantoDto.getMenoresCargo());
			} else {
				acSegmentoEt = this.transformToET(acSegmantoDto);
			}

			acSegmentoEt = this.segmentoRepository.save(acSegmentoEt);
			acSegmantoDto = this.transformToDTO(acSegmentoEt);

			PreguntaSegmento acPreguntaSegmento;
			for (Iterator<Pregunta> iterator = preguntaRepository.findAll().iterator(); iterator.hasNext();) {
				acPreguntaSegmento = new PreguntaSegmento();
				Pregunta acPregunta = (Pregunta) iterator.next();

				acPreguntaSegmento.setCodigoPregunta(acPregunta);
				acPreguntaSegmento.setCodigoSegmento(acSegmentoEt);

				preguntaSegmentoRepository.save(acPreguntaSegmento);
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acSegmantoDto;
	}

	/**
	 * Metodo que elimina un AcSegmento de la base de datos
	 * 
	 * @param acSegmentoDto
	 * @return responseService
	 */
	@Override
	public ResponseService delete(AcSegmentoDTO acSegmentoDto) {
		ResponseService responseService = new ResponseService();
		Segmento acSegmentoET = new Segmento();
		try {
			if (acSegmentoDto.getCodigo() != null) {
				acSegmentoET = this.findByCodigo(acSegmentoDto.getCodigo());
				if (acSegmentoET.getAcObjetivoSegmentoList().isEmpty() && acSegmentoET.getAcProspectoList().isEmpty()) {
					this.segmentoRepository.delete(acSegmentoET);
					ResponseServiceUtil.buildSuccessResponse(responseService,
							managePropertiesLogic.getProperties("respuesta.properties", "DeleteDatos"), acSegmentoDto);
				} else {
					ResponseServiceUtil.buildFailResponse(responseService,
							managePropertiesLogic.getProperties("respuesta.properties", "DeleteDatosAsociados"));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		return responseService;
	}

	/**
	 * Retorna todos los objetos de una base de datos transformados en objetos DTO
	 * 
	 * @return lstAcSegmentoDto
	 */
	@Override
	public List<AcSegmentoDTO> findAllDto() {
		List<AcSegmentoDTO> lstAcSegmentoDto = new ArrayList<>();
		List<Segmento> lstAcSegmento = new ArrayList<>();
		try {
			lstAcSegmento = segmentoRepository.findAll();
			for (Segmento acSegmento : lstAcSegmento) {
				lstAcSegmentoDto.add(this.transformToDTO(acSegmento));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstAcSegmentoDto;
	}
}