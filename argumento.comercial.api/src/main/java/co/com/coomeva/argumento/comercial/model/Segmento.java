/**
* Classname: AcSegmento
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_SEGMENTO", schema = DatabaseUtil.SCHEMA)
public class Segmento implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_SEGMENTO_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_SEGMENTO_SEQ", sequenceName = "AC_SEGMENTO_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Size(max = 100)
	@Column(name = "NOMBRE")
	private String nombre;
	@Size(max = 500)
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@Column(name = "EDAD_INICIO")
	private Short edadInicio;
	@Column(name = "EDAD_FIN")
	private Short edadFin;
	@Column(name = "GENERO")
	private Short genero;
	@Column(name = "MENORES_CARGO")
	private Short menoresCargo;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoSegmento")
	private List<PreguntaSegmento> acPreguntaSegmentoList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoSegmento")
	private List<ObjetivoSegmento> acObjetivoSegmentoList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoSegmento")
	private List<Prospecto> acProspectoList;

	public Segmento() {
	}

	public Segmento(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Short getEdadInicio() {
		return edadInicio;
	}

	public void setEdadInicio(Short edadInicio) {
		this.edadInicio = edadInicio;
	}

	public Short getEdadFin() {
		return edadFin;
	}

	public void setEdadFin(Short edadFin) {
		this.edadFin = edadFin;
	}

	public Short getGenero() {
		return genero;
	}

	public void setGenero(Short genero) {
		this.genero = genero;
	}

	public Short getMenoresCargo() {
		return menoresCargo;
	}

	public void setMenoresCargo(Short menoresCargo) {
		this.menoresCargo = menoresCargo;
	}

	@XmlTransient
	@JsonIgnore
	public List<PreguntaSegmento> getAcPreguntaSegmentoList() {
		return acPreguntaSegmentoList;
	}

	public void setAcPreguntaSegmentoList(List<PreguntaSegmento> acPreguntaSegmentoList) {
		this.acPreguntaSegmentoList = acPreguntaSegmentoList;
	}

	@XmlTransient
	@JsonIgnore
	public List<ObjetivoSegmento> getAcObjetivoSegmentoList() {
		return acObjetivoSegmentoList;
	}

	public void setAcObjetivoSegmentoList(List<ObjetivoSegmento> acObjetivoSegmentoList) {
		this.acObjetivoSegmentoList = acObjetivoSegmentoList;
	}

	@XmlTransient
	@JsonIgnore
	public List<Prospecto> getAcProspectoList() {
		return acProspectoList;
	}

	public void setAcProspectoList(List<Prospecto> acProspectoList) {
		this.acProspectoList = acProspectoList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Segmento)) {
			return false;
		}
		Segmento other = (Segmento) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcSegmento[ codigo=" + codigo + " ]";
	}

}
