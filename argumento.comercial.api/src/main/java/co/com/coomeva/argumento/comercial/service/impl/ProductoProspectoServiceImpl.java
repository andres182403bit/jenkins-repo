/**
* Classname: AcProductoProspectoImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcProductoProspectoDTO;
import co.com.coomeva.argumento.comercial.dto.GoDoWorksResponse;
import co.com.coomeva.argumento.comercial.model.ProductoProspecto;
import co.com.coomeva.argumento.comercial.repository.ParametroRepository;
import co.com.coomeva.argumento.comercial.repository.ProductoProspectoRepository;
import co.com.coomeva.argumento.comercial.service.ProductoService;
import co.com.coomeva.argumento.comercial.service.ProductoProspectoService;
import co.com.coomeva.argumento.comercial.service.ProspectoService;
import co.com.coomeva.argumento.comercial.util.DistributorWSRest;
import co.com.coomeva.argumento.comercial.util.Util;

@Transactional
@Service
public class ProductoProspectoServiceImpl implements ProductoProspectoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductoProspectoServiceImpl.class);

	@Autowired
	private ProductoProspectoRepository productoProspectoRepository;

	@Autowired
	private ParametroRepository parametroRepository;

	@Autowired
	private ProductoService productoService;

	@Autowired
	private ProspectoService prospectoService;

	/**
	 * Metodo que retorna una lista de objetos AcProductoProspecto con los objetos
	 * que existen en base de datos
	 * 
	 * @return List<AcProductoProspecto>
	 */
	@Override
	public List<ProductoProspecto> findAll() {
		return productoProspectoRepository.findAll();
	}

	/**
	 * Metodo que almacena un objeto AcProductoProspencto en la base de datos
	 * 
	 * @param productoProspectoDTO
	 * @return productoProspectoDTO
	 */
	@Override
	public ProductoProspecto save(AcProductoProspectoDTO productoProspectoDTO) {
		ProductoProspecto productoProspectoSave = new ProductoProspecto();
		try {
			productoProspectoSave = this.transformToET(productoProspectoDTO);
			productoProspectoSave = this.productoProspectoRepository.save(productoProspectoSave);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return productoProspectoSave;
	}

	/**
	 * Metodo que tranforma un Objeto DTO en una entidad
	 * 
	 * @param productoProspecto
	 * @return productoProspectoET
	 */
	@Override
	public ProductoProspecto transformToET(AcProductoProspectoDTO productoProspecto) {
		try {
			AcProductoProspectoDTO productoProspectoDTO = productoProspecto;
			ProductoProspecto productoProspectoET = new ProductoProspecto();

			productoProspectoET.setCodigo(productoProspecto.getCodigo());
			productoProspectoET
					.setFechaAOfrecer(Util.formatoFecha(productoProspectoDTO.getFechaAOfrecer(), "yyyy-MM-dd"));
			productoProspectoET
					.setCodigoProducto(this.productoService.transformToET(productoProspectoDTO.getCodigoProducto()));
			productoProspectoET
					.setCodigoProspecto(this.prospectoService.transformToET(productoProspectoDTO.getCodigoProspecto()));
			productoProspectoET.setAgendado(productoProspectoDTO.getAgendado());
			return productoProspectoET;

		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Metodo que elimina un AcProductoProspecto de la base de datos
	 * 
	 * @param codProspecto
	 */
	@Override
	public void deleteProductoProspecto(Long codProspecto) {
		try {
			this.productoProspectoRepository.deleteProductoProspecto(codProspecto);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Almacena una lista de objetos DTO que recibe por parametro
	 * 
	 * @param lstAcProductoProspectoDTO
	 * @return lstRetorno
	 */
	public List<AcProductoProspectoDTO> saveList(List<AcProductoProspectoDTO> lstAcProductoProspectoDTO) {
		List<AcProductoProspectoDTO> lstRetorno = new ArrayList<>();
		try {
			String cuentaGoDoWork = this.parametroRepository.findByName("CUENTA_GO_DOWORKS").getValor();
			String proyectoGoDoWork = this.parametroRepository.findByName("PROYECTO_GO_DOWORKS").getValor();
			String urlGoDoWork = this.parametroRepository.findByName("URL_GO_DOWORKS").getValor();
			String uriGoDoWork = this.parametroRepository.findByName("URI_GO_DOWORKS").getValor();
			if (!lstAcProductoProspectoDTO.isEmpty()) {
				this.deleteProductoProspecto(lstAcProductoProspectoDTO.get(0).getCodigoProspecto().getCodigo());
			}
			AcProductoProspectoDTO acProductoProspectoResponse;
			for (AcProductoProspectoDTO acProductoProspectoDTO : lstAcProductoProspectoDTO) {
				acProductoProspectoDTO.setAgendado("N");
				acProductoProspectoResponse = this.transformToDto(this.save(acProductoProspectoDTO));
				GoDoWorksResponse responseGoDoWork = DistributorWSRest.restGoDoWorks(cuentaGoDoWork, proyectoGoDoWork,
						urlGoDoWork, uriGoDoWork, acProductoProspectoResponse);

				if (!responseGoDoWork.isError()) {
					acProductoProspectoResponse.setAgendado("S");
					acProductoProspectoResponse = this.transformToDto(this.save(acProductoProspectoResponse));
				}

				lstRetorno.add(acProductoProspectoResponse);
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstRetorno;
	}

	/**
	 * Actualiza una lista de objetos DTO de acuerdo a lo que se recibe por
	 * parametros
	 * 
	 * @param lstAcProductoProspectoDTO
	 * @return lstRetorno
	 */
	public List<AcProductoProspectoDTO> updateList(List<AcProductoProspectoDTO> lstAcProductoProspectoDTO) {
		List<AcProductoProspectoDTO> lstRetorno = new ArrayList<>();
		try {
			for (AcProductoProspectoDTO acProductoProspectoDTO : lstAcProductoProspectoDTO) {
				ProductoProspecto acProductoProspecto = this.findByCodigo(acProductoProspectoDTO.getCodigo());
				acProductoProspecto.setAgendado(acProductoProspectoDTO.getAgendado());
				lstRetorno.add(this.transformToDto(this.productoProspectoRepository.save(acProductoProspecto)));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstRetorno;
	}

	/**
	 * Busca objetos AcProductoProspecto por codigo
	 * 
	 * @param codigo
	 * @return acProductoProspecto
	 */
	@Override
	public ProductoProspecto findByCodigo(Long codigo) {
		ProductoProspecto acProductoProspecto = new ProductoProspecto();
		try {
			acProductoProspecto = this.productoProspectoRepository.findByCodigo(codigo);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acProductoProspecto;
	}

	/**
	 * Metodo que transforma un objeto entidad en un DTO
	 * 
	 * @param acProductoProspecto
	 * @return acProductoProspecto
	 */
	public AcProductoProspectoDTO transformToDto(ProductoProspecto acProductoProspecto) {
		AcProductoProspectoDTO acProductoProspectoDTO = new AcProductoProspectoDTO();
		try {
			acProductoProspectoDTO.setCodigo(acProductoProspecto.getCodigo());
			acProductoProspectoDTO
					.setCodigoProducto(this.productoService.transformToDTO(acProductoProspecto.getCodigoProducto()));
			acProductoProspectoDTO
					.setCodigoProspecto(this.prospectoService.transformToDTO(acProductoProspecto.getCodigoProspecto()));
			acProductoProspectoDTO.setFechaAOfrecer(Util.formatoFecha(acProductoProspecto.getFechaAOfrecer(), "-"));
			acProductoProspectoDTO.setAgendado(acProductoProspecto.getAgendado());
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acProductoProspectoDTO;
	}
}