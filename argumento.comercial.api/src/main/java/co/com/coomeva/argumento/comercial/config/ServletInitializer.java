package co.com.coomeva.argumento.comercial.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;

/**
 * Servlet que se encarga de inicializar los componentes Spring-Boot
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@SpringBootApplication(scanBasePackages = { "co.com.coomeva.argumento.comercial" })
@PropertySource("classpath:log4j.properties")
@PropertySource("classpath:parametros.properties")
public class ServletInitializer extends SpringBootServletInitializer {

	/**
	 * Metodo para configurar la aplicacion Spring-Boot
	 * 
	 * @author Diego Bocanegra
	 * @param application
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ServletInitializer.class);
	}

}
