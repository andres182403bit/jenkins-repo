package co.com.coomeva.argumento.comercial.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcRespuestaDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.RespuestaService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo de las respuestas de la encuesta del
 * prospecto.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/respuesta" })
public class RespuestaController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RespuestaController.class);

	@Autowired
	private RespuestaService respuestaService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que se encarga de almacenar las respuestas de la encuesta del
	 * prospecto.
	 * 
	 * @param respuestaDTO Objeto con la respuesta de las preguntas de la encuesta.
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/save")
	public ResponseService save(@RequestBody AcRespuestaDTO respuestaDTO) {
		ResponseService responseService = new ResponseService();
		try {
			AcRespuestaDTO respuestaResponseDTO = this.respuestaService.save(respuestaDTO);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"), respuestaResponseDTO);
		} catch (Exception e) {
			LOGGER.error("Error RespuestaController::save: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}
}