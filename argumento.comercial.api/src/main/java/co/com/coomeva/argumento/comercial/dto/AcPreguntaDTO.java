/**
 *	Classname: AcPreguntaDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

public class AcPreguntaDTO {
	private Long codigo;
    private String numero;
    private String pregunta;
    private String ayuda;
    
    public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public String getAyuda() {
		return ayuda;
	}
	public void setAyuda(String ayuda) {
		this.ayuda = ayuda;
	}
}
