/**
* Classname: AcObjetivo
* @version 1.0 15/03/2020
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_OBJETIVO", schema = DatabaseUtil.SCHEMA)
public class Objetivo implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_OBJETIVO_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_OBJETIVO_SEQ", sequenceName = "AC_OBJETIVO_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Size(max = 300)
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoObjetivo")
	@JsonIgnore
	private List<ObjetivosProspecto> acObjetivosProspectoList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoObjetivo")
	@JsonIgnore
	private List<ProductoObjetivo> acProductoObjetivoList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoObjetivo")
	@JsonIgnore
	private List<NecesidadObjetivo> acNecesidadObjetivoList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoObjetivo")
	@JsonIgnore
	private List<ObjetivoSegmento> acObjetivoSegmentoList;

	public Objetivo() {
	}

	public Objetivo(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@XmlTransient
	public List<ObjetivosProspecto> getAcObjetivosProspectoList() {
		return acObjetivosProspectoList;
	}

	public void setAcObjetivosProspectoList(List<ObjetivosProspecto> acObjetivosProspectoList) {
		this.acObjetivosProspectoList = acObjetivosProspectoList;
	}

	@XmlTransient
	public List<ProductoObjetivo> getAcProductoObjetivoList() {
		return acProductoObjetivoList;
	}

	public void setAcProductoObjetivoList(List<ProductoObjetivo> acProductoObjetivoList) {
		this.acProductoObjetivoList = acProductoObjetivoList;
	}

	@XmlTransient
	public List<NecesidadObjetivo> getAcNecesidadObjetivoList() {
		return acNecesidadObjetivoList;
	}

	public void setAcNecesidadObjetivoList(List<NecesidadObjetivo> acNecesidadObjetivoList) {
		this.acNecesidadObjetivoList = acNecesidadObjetivoList;
	}

	@XmlTransient
	public List<ObjetivoSegmento> getAcObjetivoSegmentoList() {
		return acObjetivoSegmentoList;
	}

	public void setAcObjetivoSegmentoList(List<ObjetivoSegmento> acObjetivoSegmentoList) {
		this.acObjetivoSegmentoList = acObjetivoSegmentoList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Objetivo)) {
			return false;
		}
		Objetivo other = (Objetivo) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcObjetivo[ codigo=" + codigo + " ]";
	}

}