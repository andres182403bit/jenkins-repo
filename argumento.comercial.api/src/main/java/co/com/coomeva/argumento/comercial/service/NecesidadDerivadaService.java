/**
* InterfaseName: IAcNecesidadDerivada
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service;

import co.com.coomeva.argumento.comercial.dto.AcNecesidadDerivadaDTO;
import co.com.coomeva.argumento.comercial.model.NecesidadDerivada;

public interface NecesidadDerivadaService {
	
	public AcNecesidadDerivadaDTO transformToDTO(NecesidadDerivada acNecesidadDerivada);
	public NecesidadDerivada transformToET(AcNecesidadDerivadaDTO acNecesidadDerivada);
}