package co.com.coomeva.argumento.comercial.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcProspectoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.ProspectoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo de los prospectos de la encuesta.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/prospecto" })
public class ProspectoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProspectoController.class);

	@Autowired
	private ProspectoService prospectoService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que se encarga de almacenar el prospecto de una encuesta.
	 * 
	 * @param prospectoDTO Objeto con la información del prospecto.
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/save")
	public ResponseService save(@RequestBody AcProspectoDTO prospectoDTO) {
		ResponseService responseService = new ResponseService();
		try {
			prospectoDTO = this.prospectoService.save(prospectoDTO);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"), prospectoDTO);
			return responseService;
		} catch (Exception e) {
			LOGGER.error("Error ProspectoController::save: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		return responseService;
	}

	/**
	 * Servicio que se encarga de consultar un prospecto en la aplicación o el
	 * servicio web de asociado vida.
	 * 
	 * @param tipoIdentificacion Contiene el tipo de identificacion del prospecto
	 * @param identificacion     Contiene el numero de indentificacion del prospecto
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@GetMapping("/findByTipoIdId")
	public ResponseService findProspectoByTipoAndId(@RequestParam("tipoIdentificacion") String tipoIdentificacion,
			@RequestParam("identificacion") String identificacion) {
		ResponseService responseService = new ResponseService();
		try {
			AcProspectoDTO prospectoResponseDTO = this.prospectoService
					.findByTipoIdentificacionIdentificacion(Short.parseShort(tipoIdentificacion), identificacion);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"), prospectoResponseDTO);
		} catch (Exception e) {
			LOGGER.error("Error ProspectoController::findProspectoByTipoAndId: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		
		return responseService;
	}
}