/**
* Classname: Status
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GLD_ESTADO_ASOCIADO")
public class Status {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", length = 11)
	private Long id;

	@Column(name = "DESCRIPCION", nullable = false, length = 100)
	private String description;

	@Column(name = "ESTADO", nullable = false, length = 100)
	private Long statusAso;

	@Column(name = "COD_ESTADO_ASO", nullable = false, length = 100)
	private String codEstadoAso;

	public Status() {
		// Construct
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCodEstadoAso() {
		return codEstadoAso;
	}

	public void setCodEstadoAso(String codEstadoAso) {
		this.codEstadoAso = codEstadoAso;
	}

	public Long getStatusAso() {
		return statusAso;
	}

	public void setStatusAso(Long statusAso) {
		this.statusAso = statusAso;
	}
	
	

}
