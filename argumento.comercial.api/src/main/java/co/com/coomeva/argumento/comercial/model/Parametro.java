/**
* Classname: AcParametro
*
* @version 1.0 04/05/2020
* Clase Para la entidad Ciudad
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_PARAMETRO", schema = DatabaseUtil.SCHEMA)
public class Parametro implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_PARAMETRO_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_PARAMETRO_SEQ", sequenceName = "AC_PARAMETRO_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "NOMBRE")
	private String nombre;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 500)
	@Column(name = "VALOR")
	private String valor;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 500)
	@Column(name = "DESCRIPCION")
	private String descripcion;

	public Parametro() {
	}

	public Parametro(Long codigo) {
		this.codigo = codigo;
	}

	public Parametro(Long codigo, String nombre, String valor, String descripcion) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.valor = valor;
		this.descripcion = descripcion;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Parametro)) {
			return false;
		}
		Parametro other = (Parametro) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcParametro[ codigo=" + codigo + " ]";
	}

}
