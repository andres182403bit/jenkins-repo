/**
* Classname: AcObjetivoSegmento
* @version 1.0 15/03/2020
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_OBJETIVO_SEGMENTO", schema = DatabaseUtil.SCHEMA)
public class ObjetivoSegmento implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@JoinColumn(name = "CODIGO_OBJETIVO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	@JsonManagedReference
	private Objetivo codigoObjetivo;
	@JoinColumn(name = "CODIGO_SEGMENTO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	@JsonBackReference
	private Segmento codigoSegmento;

	public ObjetivoSegmento() {
	}

	public ObjetivoSegmento(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Objetivo getCodigoObjetivo() {
		return codigoObjetivo;
	}

	public void setCodigoObjetivo(Objetivo codigoObjetivo) {
		this.codigoObjetivo = codigoObjetivo;
	}

	public Segmento getCodigoSegmento() {
		return codigoSegmento;
	}

	public void setCodigoSegmento(Segmento codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ObjetivoSegmento)) {
			return false;
		}
		ObjetivoSegmento other = (ObjetivoSegmento) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcObjetivoSegmento[ codigo=" + codigo + " ]";
	}

}
