/**
* Classname: AcNecesidadDerivada
* @version 1.0 15/03/2020
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_NECESIDAD_DERIVADA", schema = DatabaseUtil.SCHEMA)
public class NecesidadDerivada implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Size(max = 300)
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoNecesidad")
	private List<NecesidadObjetivo> acNecesidadObjetivoList;

	public NecesidadDerivada() {
	}

	public NecesidadDerivada(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@XmlTransient
	public List<NecesidadObjetivo> getAcNecesidadObjetivoList() {
		return acNecesidadObjetivoList;
	}

	public void setAcNecesidadObjetivoList(List<NecesidadObjetivo> acNecesidadObjetivoList) {
		this.acNecesidadObjetivoList = acNecesidadObjetivoList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof NecesidadDerivada)) {
			return false;
		}
		NecesidadDerivada other = (NecesidadDerivada) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcNecesidadDerivada[ codigo=" + codigo + " ]";
	}

}
