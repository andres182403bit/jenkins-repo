/**
* InterfaseName: IAcObjetivo
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.model.Objetivo;

public interface ObjetivoService {
	public List<Objetivo> findAll();

	public AcObjetivoDTO save(AcObjetivoDTO prospecto);

	public AcObjetivoDTO transformToDTO(Objetivo objetivo);

	public Objetivo findByCodigo(Long codigo);

	public List<AcObjetivoDTO> findAllDto();

	public ResponseService delete(AcObjetivoDTO acObjetivo);

	public Objetivo transformToET(AcObjetivoDTO acObjetivo);
}