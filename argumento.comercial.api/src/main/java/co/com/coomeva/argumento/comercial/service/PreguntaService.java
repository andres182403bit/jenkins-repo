/**
 *	Interfasename: IAcPregunta
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcPreguntaDTO;
import co.com.coomeva.argumento.comercial.model.Pregunta;

public interface PreguntaService {
	public AcPreguntaDTO transformToDTO(Pregunta acPregunta);

	public List<AcPreguntaDTO> getQuestions(Long codigoSegmento);
}
