/**
* Classname: AsociadoVidaLogic
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import co.com.coomeva.argumento.comercial.model.Category;
import co.com.coomeva.argumento.comercial.model.Status;

public class AsociadoVidaLogic {

	private static final Logger LOGGER = LoggerFactory.getLogger(AsociadoVidaLogic.class);

	private static String operationConsultarAsociadoVidaAgenVin = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.wsasociado.coomeva.com.co\">\r\n"
			+ "   <soapenv:Header/>\r\n" + "   <soapenv:Body>\r\n"
			+ "      <ws:consultarAsociadoVidaAgenVin soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\r\n"
			+ "         <identificacion xsi:type=\"xsd:long\">AddNumDocDev</identificacion>\r\n"
			+ "         <tipDocumento xsi:type=\"xsd:long\">AddTipDocDev</tipDocumento>\r\n"
			+ "      </ws:consultarAsociadoVidaAgenVin>\r\n" + "   </soapenv:Body>\r\n" + "</soapenv:Envelope>";

	private static String operationConsultarAsociadoVida = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.wsasociado.coomeva.com.co\">\r\n"
			+ "   <soapenv:Header/>\r\n" + "   <soapenv:Body>\r\n"
			+ "      <ws:consultarAsociadoVida soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\r\n"
			+ "         <identificacion xsi:type=\"xsd:long\">AddNumDocDev</identificacion>\r\n"
			+ "         <tipDocumento xsi:type=\"xsd:long\">AddTipDocDev</tipDocumento>\r\n"
			+ "      </ws:consultarAsociadoVida>\r\n" + "   </soapenv:Body>\r\n" + "</soapenv:Envelope>";

	private AsociadoVidaLogic() {
	}

	/**
	 * Método que valida que no se permita actualizar un estado en específico
	 * 
	 * @param associatedStatus codigo del estado
	 * @return validated
	 */
	public static boolean validateStateForUpdate(Status status) {
		boolean validated = false;
		if (status.getStatusAso() != null && String.valueOf(status.getStatusAso()).equals("1")) {
			validated = true;
		}
		return validated;
	}

	/**
	 * Método que válida si la descripción se debe mostrar o no
	 * 
	 * @param descriptionCategory descripción de la categoría a la que pertenece el
	 *                            asociado
	 * @return validateCategory
	 */
	public static boolean validateCategoryShow(String descriptionCategory, Category category) {
		boolean validateCategory = false;
		if (category != null && category.getState() != null && !category.getState().equals("")
				&& category.getState().equals("S")) {
			validateCategory = true;
		}
		return validateCategory;
	}

	/**
	 * Consumir en el servicio la acción de consultar el asociado vida
	 * 
	 * @param numId             numberDocument
	 * @param tipDoc            typeDocument
	 * @param asociadoVidaParam set SOAP values to this asociadoVida
	 * @return asociadoVida associated obtained from SOAP
	 */
	public static AsociadoVida actionConsultarAsociadoVida(String numId, long tipDoc, AsociadoVida asociadoVidaParam,
			List<Status> listStatus) {
		AsociadoVida asociadoVida = null;
		if (asociadoVidaParam == null) {
			asociadoVida = new AsociadoVida();
		} else {
			asociadoVida = asociadoVidaParam;
		}
		try {
			String send = "";
			send = operationConsultarAsociadoVida;
			send = send.replace("AddNumDocDev", (numId));
			send = send.replace("AddTipDocDev", Long.toString(tipDoc));
			String endpoint = LoadProperties.getInstance().getProperty("asociado.properties", "endpointAsociadoVida");
			SOAPMessage response = SOAPClientSAAJ.consumeSOAP(send, endpoint);
			SOAPBody body = response.getSOAPBody();

			String codRetornoWS = getPropertyResponseString(body, "codRetornoWS");
			if (codRetornoWS != null && !codRetornoWS.isEmpty()) {
				asociadoVida.setCodRetornoWS(Long.parseLong(codRetornoWS));
			} else {
				asociadoVida.setCodRetornoWS(-1L);
			}

			if (asociadoVida.getCodRetornoWS() == 1) {
				asociadoVida.setBarComercial(getPropertyResponseString(body, "barComercial"));
				asociadoVida.setBarFamiliar(getPropertyResponseString(body, "barFamiliar"));
				asociadoVida.setBarResidencia(getPropertyResponseString(body, "barResidencia"));
				asociadoVida.setCelular(getPropertyResponseString(body, "celular"));
				asociadoVida.setCiuComercial(getPropertyResponseString(body, "ciuComercial"));
				asociadoVida.setCiuFamiliar(getPropertyResponseString(body, "ciuFamiliar"));
				asociadoVida.setCiuResidencia(getPropertyResponseString(body, "ciuResidencia"));
				asociadoVida.setCodAsociado(Long.parseLong(getPropertyResponseString(body, "codAsociado")));
				asociadoVida.setCodCiuComercial(getPropertyResponseString(body, "codCiuComercial"));
				asociadoVida.setCodCiuFamiliar(getPropertyResponseString(body, "codCiuFamiliar"));
				asociadoVida.setCodCiuResidencia(getPropertyResponseString(body, "codCiuResidencia"));
				asociadoVida
						.setCodDepExpDocumento(Long.parseLong(getPropertyResponseString(body, "codDepExpDocumento")));
				asociadoVida.setCodEstadoCivil(Long.parseLong(getPropertyResponseString(body, "codEstadoCivil")));
				asociadoVida
						.setCodLugExpDocumento(Long.parseLong(getPropertyResponseString(body, "codLugExpDocumento")));
				asociadoVida.setCodSexo(Long.parseLong(getPropertyResponseString(body, "codSexo")));
				asociadoVida.setCodTipDocumento(Long.parseLong(getPropertyResponseString(body, "codTipDocumento")));
				asociadoVida.setCodTipoCasa(Long.parseLong(getPropertyResponseString(body, "codTipoCasa")));
				asociadoVida.setDepExpDocumento(getPropertyResponseString(body, "depExpDocumento"));
				asociadoVida.setDescRetornoWS(getPropertyResponseString(body, "descRetornoWS"));
				asociadoVida.setDirComercial(getPropertyResponseString(body, "dirComercial"));
				asociadoVida.setDirCorrespondencia(getPropertyResponseString(body, "dirCorrespondencia"));
				asociadoVida.setDirFamiliar(getPropertyResponseString(body, "dirFamiliar"));
				asociadoVida.setDirResidencia(getPropertyResponseString(body, "dirResidencia"));
				asociadoVida.setEmail(getPropertyResponseString(body, "email"));
				asociadoVida.setEstrato(Long.parseLong(getPropertyResponseString(body, "estrato")));
				asociadoVida.setEstado(Long.parseLong(getPropertyResponseString(body, "estado")));
				asociadoVida.setExtComercial(getPropertyResponseString(body, "extComercial"));
				asociadoVida.setFaxComercial(getPropertyResponseString(body, "faxComercial"));
				asociadoVida.setFecExpDocumento(getPropertyResponseString(body, "fecExpDocumento"));
				asociadoVida.setLugExpDocumento(getPropertyResponseString(body, "lugExpDocumento"));
				asociadoVida.setNombre1(getPropertyResponseString(body, "nombre1"));
				asociadoVida.setNombre2(getPropertyResponseString(body, "nombre2"));
				asociadoVida.setNumDocumento(getPropertyResponseString(body, "numDocumento"));
				asociadoVida.setNumInt(Long.parseLong(getPropertyResponseString(body, "numInt")));
				asociadoVida.setPerMayCargo(Long.parseLong(getPropertyResponseString(body, "perMayCargo")));
				asociadoVida.setPerMenCargo(Long.parseLong(getPropertyResponseString(body, "perMenCargo")));
				asociadoVida.setPrimerApellido(getPropertyResponseString(body, "primerApellido"));
				asociadoVida.setSegundoApellido(getPropertyResponseString(body, "segundoApellido"));
				asociadoVida.setSexo(getPropertyResponseString(body, "sexo"));
				asociadoVida.setSistema(getPropertyResponseString(body, "sistema"));
				asociadoVida.setTelComercial(getPropertyResponseString(body, "telComercial"));
				asociadoVida.setTelFamiliar(getPropertyResponseString(body, "telFamiliar"));
				asociadoVida.setTelResidencia(getPropertyResponseString(body, "telResidencia"));
				asociadoVida.setTipDocumento(getPropertyResponseString(body, "tipDocumento"));
				asociadoVida.setTipUbiDirCor(getPropertyResponseString(body, "tipUbiDirCor"));
				asociadoVida.setTipoCasa(getPropertyResponseString(body, "tipoCasa"));
				asociadoVida.setEstadoCivil(getPropertyResponseString(body, "estadoCivil"));
				asociadoVida.setUsuario(getPropertyResponseString(body, "usuario"));
				asociadoVida.setAssignOrCreateAssoiate(true);
				asociadoVida.setValidateUpdateState(false);
				asociadoVida.setDescEstado(assignStatusMessage(asociadoVida.getEstado(), listStatus));

				String codOficinaVin = LoadProperties.getInstance().getProperty("asociado.properties", "codOficinaVin");
				String codTipoVinculacion = LoadProperties.getInstance().getProperty("asociado.properties",
						"codTipoVinculacion");
				String desOficinaVin = LoadProperties.getInstance().getProperty("asociado.properties", "desOficinaVin");
				String desTipovinculacion = LoadProperties.getInstance().getProperty("asociado.properties",
						"desTipovinculacion");

				String codOfi = getPropertyResponseString(body, codOficinaVin);
				if (codOfi != null && !codOfi.isEmpty()) {
					asociadoVida.setCodOficinaVin(Long.parseLong(codOfi));
				}

				String codTipoVin = getPropertyResponseString(body, codTipoVinculacion);
				if (codTipoVin != null && !codTipoVin.isEmpty()) {
					asociadoVida.setCodTipoVinculacion(Long.parseLong(codTipoVin));
				}

				String cuotaVen = getPropertyResponseString(body, "cuotaVencida");
				if (cuotaVen != null && !cuotaVen.isEmpty()) {
					asociadoVida.setCuotaVencida(Long.parseLong(cuotaVen));
				}

				asociadoVida.setDesOficinaVin(getPropertyResponseString(body, desOficinaVin));
				asociadoVida.setDesTipovinculacion(getPropertyResponseString(body, desTipovinculacion));
				asociadoVida.setCorte(getPropertyResponseString(body, "corte"));
				asociadoVida.setOcupacion(getPropertyResponseString(body, "ocupacion"));
				asociadoVida.setProfesion(getPropertyResponseString(body, "profesion"));

				String fecEstado = getPropertyResponseString(body, "fecEstado");
				if (fecEstado != null && !fecEstado.isEmpty()) {
					asociadoVida.setFecEstado(Long.parseLong(fecEstado));
				}

				String fecIngreso = getPropertyResponseString(body, "fecIngreso");
				if (fecIngreso != null && !fecIngreso.isEmpty()) {
					asociadoVida.setFecIngreso(Long.parseLong(fecIngreso));
				}

				String fecNaci = getPropertyResponseString(body, "fecNacimiento");
				if (fecNaci != null && !fecNaci.isEmpty()) {
					asociadoVida.setFecNacimiento(Long.parseLong(fecNaci));
				}
				
				asociadoVida.setAge(generateAgeWithCurrentDateAndDateBirth(asociadoVida.getFecNacimiento()));

			} else {
				asociadoVida.setDescRetornoWS(getPropertyResponseString(body, "descRetornoWS"));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
		}
		return asociadoVida;
	}

	/**
	 * Asignar descripcion del estado según el código
	 * 
	 * @param associateStateCodeInt codigo de estado del asociado
	 * @return
	 */
	private static String assignStatusMessage(long associateStateCodeInt, List<Status> listStatus) {
		String descriptionStatus = "";
		if (listStatus != null) {
			for (Status status : listStatus) {
				if (status.getCodEstadoAso().equalsIgnoreCase(Long.toString(associateStateCodeInt))) {
					descriptionStatus = status.getDescription();
				}
			}
		}

		return descriptionStatus;
	}

	/**
	 * Consumir en el servicio la acción de consultar el asociado vida con
	 * propiedades de agen vin
	 * 
	 * @param numId             numero de documento
	 * @param tipDoc            tipo de documento
	 * @param asociadoVidaParam asociado vida que se le setean los nuevos valores
	 * @return asociadoVida
	 */
	public static AsociadoVida consultarAsociadoVidaAgenVin(String numId, long tipDoc, AsociadoVida asociadoVidaParam) {
		AsociadoVida asociadoVida = null;
		if (asociadoVidaParam == null) {
			asociadoVida = new AsociadoVida();
		} else {
			asociadoVida = asociadoVidaParam;
		}
		String send = "";
		send = operationConsultarAsociadoVidaAgenVin;
		send = send.replace("AddNumDocDev", (numId));
		send = send.replace("AddTipDocDev", Long.toString(tipDoc));
		String endpoint = LoadProperties.getInstance().getProperty("asociado.properties", "endpointAsociadoVida");
		try {
			SOAPMessage response = SOAPClientSAAJ.consumeSOAP(send, endpoint);
			SOAPBody body = response.getSOAPBody();

			String codRetornoWS = getPropertyResponseString(body, "codRetornoWS");

			if (codRetornoWS != null && !codRetornoWS.isEmpty()) {
				asociadoVida.setCodRetornoWS(Long.parseLong(codRetornoWS));
			} else {
				asociadoVida.setCodRetornoWS(-1L);
			}

			if (asociadoVida.getCodRetornoWS() == 1) {
				asociadoVida.setCodAsociado(Long.parseLong(getPropertyResponseString(body, "codAsociado")));
				asociadoVida.setCodOficinaVin(Long.parseLong(getPropertyResponseString(body, "codOficinaVin")));
				asociadoVida
						.setCodTipoVinculacion(Long.parseLong(getPropertyResponseString(body, "codTipoVinculacion")));
				asociadoVida.setCorte(getPropertyResponseString(body, "corte"));
				asociadoVida.setCuotaVencida(Long.parseLong(getPropertyResponseString(body, "cuotaVencida")));
				asociadoVida.setFecEstado(Long.parseLong(getPropertyResponseString(body, "fecEstado")));
				asociadoVida.setFecIngreso(Long.parseLong(getPropertyResponseString(body, "fecIngreso")));
				asociadoVida.setFecNacimiento(Long.parseLong(getPropertyResponseString(body, "fecNacimiento")));
				asociadoVida.setOcupacion(getPropertyResponseString(body, "ocupacion"));
				asociadoVida.setProfesion(getPropertyResponseString(body, "profesion"));
				asociadoVida.setDesOficinaVin(getPropertyResponseString(body, "desOficinaVin"));
				asociadoVida.setDesTipovinculacion(getPropertyResponseString(body, "desTipovinculacion"));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
		}
		return asociadoVida;
	}

	/**
	 * Método que obtiene la edad de la fecha de nacimiento y la fecha actual
	 * 
	 * @param numberDateBirthDay day of the birthday date
	 * @return (long) ageAso age
	 * @throws ParseException Excepción para parsear el formato fecha
	 */
	public static long generateAgeWithCurrentDateAndDateBirth(long numberDateBirthDay) throws ParseException {

		if (numberDateBirthDay <= 0) {
			return numberDateBirthDay;
		}

		String birthFullAsoStr = transformFechaInLong(numberDateBirthDay, "-", 1);

		// Formatear a la cadena a Date
		Date birthDate = formatterToDate("yyyy-MM-dd", birthFullAsoStr);

		// Armar la fecha para restar entre ellas
		Date currentDate = new Date();
		double currentDateLong = transformDate(currentDate);
		double birthDateLong = transformDate(birthDate);

		// Calcular edad
		double ageAso = currentDateLong - birthDateLong;
		ageAso = ageAso / 10000;
		ageAso = Math.floor(ageAso);
		return (long) ageAso;
	}

	/**
	 * Transforma la fecha
	 * 
	 * @param date fecha
	 * @return birthFullAsoStr
	 */
	public static double transformDate(Date date) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		String year = Integer.toString(calendar.get(Calendar.YEAR));
		String month = Integer.toString(calendar.get(Calendar.MONTH) + 1);
		String day = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
		if (month.length() == 1) {
			month = 0 + month;
		}
		if (day.length() == 1) {
			day = 0 + day;
		}
		String dateStr = year + month + day;
		return Double.parseDouble(dateStr);
	}

	/**
	 * Transforma la fecha en Long
	 * 
	 * @param numberDateBirthDay numero de cumpleaños
	 * @param separador          separador de fecha
	 * @param orden              orden de la fecha
	 * @return birthFullAsoStr
	 */
	public static String transformFechaInLong(long numberDateBirthDay, String separador, int orden) {
		// Extraer el año, mes y día de la fecha de nacimiento
		String birthFullAsoStr = Long.toString(numberDateBirthDay);
		if (birthFullAsoStr.length() > 7) {
			String birthAsoYearStr = birthFullAsoStr.substring(0, 4);
			String birthAsoMonthStr = birthFullAsoStr.substring(4, 6);
			String birthAsoDayStr = birthFullAsoStr.substring(6, 8);
			if (birthAsoMonthStr.substring(0, 1).equals("0")) {
				birthAsoMonthStr = birthAsoMonthStr.substring(1);
			}
			if (orden == 1) {
				birthFullAsoStr = birthAsoYearStr + separador + birthAsoMonthStr + separador + birthAsoDayStr;
			} else if (orden == 2) {
				if (birthAsoMonthStr.length() == 1) {
					birthFullAsoStr = birthAsoDayStr + separador + 0 + birthAsoMonthStr + separador + birthAsoYearStr;
				} else {
					birthFullAsoStr = birthAsoDayStr + separador + birthAsoMonthStr + separador + birthAsoYearStr;
				}

			}
		}
		return birthFullAsoStr;
	}

	/**
	 * Formatear a Fecha
	 * 
	 * @param formato    formato de la fecha
	 * @param dateString fecha en String
	 * @return fecha formateada
	 * @throws ParseException Excepción que controla el Parseo
	 */
	public static Date formatterToDate(String formato, String dateString) throws ParseException {
		DateFormat formatter = new SimpleDateFormat(formato);
		return formatter.parse(dateString);
	}

	/**
	 * Obtener las propiedades de las respuestas en String
	 * 
	 * @param body   body of SOAP
	 * @param tagMsg message or response
	 * @return someMsgContent
	 */
	public static String getPropertyResponseString(SOAPBody body, String tagMsg) {
		String someMsgContent = null;
		try {
			NodeList nodes = body.getElementsByTagName(tagMsg);
			Node node = nodes.item(0);
			someMsgContent = node != null ? node.getTextContent() : "";
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
		}
		return someMsgContent;
	}

	/**
	 * Organiza el nombre completo del asociado
	 * 
	 * @param firstName     primer nombre
	 * @param secondName    segundo nombre
	 * @param surname       primer apellido
	 * @param secondSurname segundo apellido
	 * @return fullname
	 */
	public static String organizateFullName(String firstName, String secondName, String surname, String secondSurname) {
		String fullname = "";
		firstName = changeNullToEmpty(firstName);
		secondName = changeNullToEmpty(secondName);
		surname = changeNullToEmpty(surname);
		secondSurname = changeNullToEmpty(secondSurname);

		if (!firstName.equals("") && !firstName.isEmpty()) {
			fullname = firstName;
		}
		if (!secondName.equals("") && !secondName.isEmpty()) {
			fullname += " " + secondName;
		}
		if (!surname.equals("") && !surname.isEmpty()) {
			fullname += " " + surname;
		}
		if (!secondSurname.equals("") && !secondSurname.isEmpty()) {
			fullname += " " + secondSurname;
		}
		fullname = fullname.replace("null", "").trim();
		return fullname;
	}

	/**
	 * Cambia la cadena nulo a empty
	 * 
	 * @param param parametro
	 * @return param
	 */
	public static String changeNullToEmpty(String param) {
		if (param == null) {
			param = "";
		}
		return param;
	}
}
