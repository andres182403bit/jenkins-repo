package co.com.coomeva.argumento.comercial.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcProductoObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.ProductoObjetivoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo de los objetivos del prospecto en una
 * encuesta.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/productoobjetivo" })
public class ProductoObjetivoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductoObjetivoController.class);

	@Autowired
	private ProductoObjetivoService productoObjetivoService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que se encarga de almacenar los productos por objetivo del
	 * prospecto.
	 * 
	 * @param productoObjetivoDTO Producto Objetivo seleccionado
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/save")
	public ResponseService save(@RequestBody AcProductoObjetivoDTO productoObjetivoDTO) {
		ResponseService responseService = new ResponseService();
		try {
			AcProductoObjetivoDTO productoObjetivoResponseDTO = this.productoObjetivoService.save(productoObjetivoDTO);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"),
					productoObjetivoResponseDTO);
		} catch (Exception e) {
			LOGGER.error("Error ProductoObjetivoController::save: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		
		return responseService;
	}
}