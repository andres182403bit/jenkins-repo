/**
* Classname: AcRespuestaImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcRespuestaDTO;
import co.com.coomeva.argumento.comercial.model.Respuesta;
import co.com.coomeva.argumento.comercial.repository.RespuestaRepository;
import co.com.coomeva.argumento.comercial.service.ProspectoService;
import co.com.coomeva.argumento.comercial.service.RespuestaService;

@Transactional
@Service
public class RespuestaServiceImpl implements RespuestaService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RespuestaServiceImpl.class);
	
	@Autowired
	private RespuestaRepository respuestaRepository;
	@Autowired
	private ProspectoService prospectoService;

	/**
	 * Metodo que retorna todos los objetos AcRespuesta que existen en la base de
	 * datos
	 * 
	 * @return List<AcRespuesta>
	 */
	@Override
	public List<Respuesta> findAll() {
		return respuestaRepository.findAll();
	}

	/**
	 * Metodo que almacena en la base de datos un objeto AcRespuesta
	 * 
	 * @param respuesta
	 * @return acRespuestaDto
	 */
	@Override
	public AcRespuestaDTO save(AcRespuestaDTO acRespuestaDto) {
		Respuesta acRespuestaEt;
		try {
			if (acRespuestaDto.getCodigo() != null) {
				acRespuestaEt = this.findByCodigo(acRespuestaDto.getCodigo());
			} else {
				acRespuestaEt = this.buscarCodigoPreguntaProspecto(this.transformToET(acRespuestaDto));
			}

			if (acRespuestaEt != null) {
				acRespuestaDto.setCodigo(acRespuestaEt.getCodigo());
			} else {
				acRespuestaEt = new Respuesta();
			}

			acRespuestaEt.setRespuesta(acRespuestaDto.getRespuesta());
			acRespuestaEt.setCodigoPregunta(acRespuestaDto.getCodigoPregunta());
			acRespuestaEt.setCodigoProspecto(this.prospectoService.transformToET(acRespuestaDto.getAcProspectoDto()));
			acRespuestaEt = this.respuestaRepository.save(acRespuestaEt);
			acRespuestaDto = this.transformToDTO(acRespuestaEt);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acRespuestaDto;
	}

	/**
	 * Metodo que busca una respuesta por codigo
	 * 
	 * @param respuesta
	 * @return acRespuestaEt
	 */
	private Respuesta buscarCodigoPreguntaProspecto(Respuesta acRespuestaEt) {
		try {
			acRespuestaEt = this.respuestaRepository.findByCodigoPreguntaAndCodigoProspecto(
					acRespuestaEt.getCodigoPregunta(), acRespuestaEt.getCodigoProspecto());
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acRespuestaEt;
	}

	/**
	 * Metodo para convertir un DTO en una Entidad
	 * 
	 * @param acRespuestaDto
	 * @return acRespuestaEt
	 */
	public Respuesta transformToET(AcRespuestaDTO acRespuestaDto) {
		Respuesta acRespuestaEt = new Respuesta();
		AcRespuestaDTO acRespuestaTranform = new AcRespuestaDTO();
		try {
			acRespuestaTranform = acRespuestaDto;
			acRespuestaEt.setRespuesta(acRespuestaTranform.getRespuesta());
			acRespuestaEt.setCodigo(acRespuestaTranform.getCodigo());
			acRespuestaEt.setCodigoPregunta(acRespuestaTranform.getCodigoPregunta());
			if (acRespuestaTranform.getAcProspectoDto().getCodigo() != null) {
				acRespuestaEt
						.setCodigoProspecto(this.prospectoService.transformToET(acRespuestaTranform.getAcProspectoDto()));
			} else if (acRespuestaTranform.getCodigoProspecto() != null) {
				acRespuestaEt
						.setCodigoProspecto(this.prospectoService.findByCodigo(acRespuestaTranform.getCodigoProspecto()));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acRespuestaEt;
	}

	/**
	 * Metodo que recibe una entidad y retorna un DTO
	 * 
	 * @param acRespuestaEt
	 * @return acRespuestaDto
	 */
	public AcRespuestaDTO transformToDTO(Respuesta acRespuestaEt) {
		Respuesta acRespuestaEtTranform = new Respuesta();
		AcRespuestaDTO acRespuestaDto = new AcRespuestaDTO();
		try {
			acRespuestaEtTranform = acRespuestaEt;
			acRespuestaDto.setRespuesta(acRespuestaEtTranform.getRespuesta());
			acRespuestaDto.setCodigo(acRespuestaEtTranform.getCodigo());
			acRespuestaDto.setCodigoPregunta(acRespuestaEtTranform.getCodigoPregunta());
			acRespuestaDto
					.setAcProspectoDto(this.prospectoService.transformToDTO(acRespuestaEtTranform.getCodigoProspecto()));
			acRespuestaDto.setCodigoProspecto(acRespuestaDto.getAcProspectoDto().getCodigo());
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acRespuestaDto;
	}

	/**
	 * Metodo que retorna un objeto AcRespuesta por codigo
	 * 
	 * @param codigo
	 * @return AcRespuesta
	 */
	@Override
	public Respuesta findByCodigo(Long codigo) {
		return this.respuestaRepository.findByCodigo(codigo);
	}

}