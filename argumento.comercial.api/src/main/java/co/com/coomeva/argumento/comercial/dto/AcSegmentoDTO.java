/**
 *	Classname: AcSegmentoDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

public class AcSegmentoDTO {
	private Long codigo;
	private String nombre;
	private String descripcion;
	private Short edadInicio;
	private Short edadFin;
	private Short genero;
	private Short menoresCargo;
	private String strGenero;
	private String strMenorCargo;
	
	public String getStrGenero() {
		return strGenero;
	}
	public void setStrGenero(String strGenero) {
		this.strGenero = strGenero;
	}
	public String getStrMenorCargo() {
		return strMenorCargo;
	}
	public void setStrMenorCargo(String strMenorCargo) {
		this.strMenorCargo = strMenorCargo;
	}	
	
	 public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Short getEdadInicio() {
		return edadInicio;
	}
	public void setEdadInicio(Short edadInicio) {
		this.edadInicio = edadInicio;
	}
	public Short getEdadFin() {
		return edadFin;
	}
	public void setEdadFin(Short edadFin) {
		this.edadFin = edadFin;
	}
	public Short getGenero() {
		return genero;
	}
	public void setGenero(Short genero) {
		this.genero = genero;
	}
	public Short getMenoresCargo() {
		return menoresCargo;
	}
	public void setMenoresCargo(Short menoresCargo) {
		this.menoresCargo = menoresCargo;
	}
}
