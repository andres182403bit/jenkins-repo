/**
* Classname: AcProspectoImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcEjecutivoIntegralDTO;
import co.com.coomeva.argumento.comercial.dto.AcObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.AcProspectoDTO;
import co.com.coomeva.argumento.comercial.dto.AcSegmentoDTO;
import co.com.coomeva.argumento.comercial.model.ObjetivoSegmento;
import co.com.coomeva.argumento.comercial.model.Prospecto;
import co.com.coomeva.argumento.comercial.repository.ProspectoRepository;
import co.com.coomeva.argumento.comercial.service.CiudadService;
import co.com.coomeva.argumento.comercial.service.EjecutivoIntegralService;
import co.com.coomeva.argumento.comercial.service.ObjetivoService;
import co.com.coomeva.argumento.comercial.service.ProspectoService;
import co.com.coomeva.argumento.comercial.service.SegmentoService;
import co.com.coomeva.argumento.comercial.util.AsociadoVida;
import co.com.coomeva.argumento.comercial.util.AsociadoVidaLogic;

@Transactional
@Service
public class ProspectoServiceImpl implements ProspectoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProspectoServiceImpl.class);

	@Autowired
	private ProspectoRepository prospectoRepository;
	@Autowired
	private SegmentoService segmentoService;
	@Autowired
	private EjecutivoIntegralService ejecutivoIntegralService;
	@Autowired
	private ObjetivoService objetivoService;
	@Autowired
	private CiudadService ciudadService;

	/**
	 * Metodo que busca un prospecto por identificacion
	 * 
	 * @param identificacion
	 * @return AcProspecto
	 */
	@Override
	public Prospecto findByIdentificacion(String identificacion) {
		return this.prospectoRepository.findByIdentificacion(identificacion).get(0);

	}

	/**
	 * Metodo que retorna todos los objetos AcProspecto que existen en la base de
	 * datos
	 * 
	 * @return List<AcProspecto>
	 */
	@Override
	public List<Prospecto> findAll() {
		return this.prospectoRepository.findAll();
	}

	/**
	 * Metodo que guarda un objeto AcProspecto en la base de datos
	 * 
	 * @param prospecto
	 * @return prospectoDtoSave
	 */
	@Override
	public AcProspectoDTO save(AcProspectoDTO prospectoDtoSave) {
		try {
			Prospecto prospectoSave;

			AcProspectoDTO prospectoExistente = this.findByTipoIdentificacionIdentificacion(
					prospectoDtoSave.getTipoIdentificacion(), prospectoDtoSave.getIdentificacion());

			prospectoDtoSave.setCodigo(prospectoExistente.getCodigo());
			prospectoDtoSave.setEsAsociado(prospectoExistente.getEsAsociado());

			prospectoSave = this.transformToET(prospectoDtoSave);

			return this.transformToDTO(this.prospectoRepository.save(prospectoSave));
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Metodo que busca por tipo de identificacion y identificacion un AcProspecto
	 * 
	 * @param tipoIdentificacion
	 * @param identificacion
	 * @return prospecto
	 */
	@Override
	public AcProspectoDTO findByTipoIdentificacionIdentificacion(Short tipoIdentificacion, String identificacion) {
		try {
			Prospecto prospecto = this.prospectoRepository.findByTipoIdentificacionIdentificacion(tipoIdentificacion,
					identificacion);

			if (prospecto == null) {
				prospecto = this.consultaAsociadoVida(tipoIdentificacion.toString(), identificacion, new Prospecto());
			}

			return transformToDTO(prospecto);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	public Prospecto consultaAsociadoVida(String tipoIdentificacion, String identificacion, Prospecto prospecto) {
		try {
			AsociadoVida asociadoVida = this.consultarAsociado(tipoIdentificacion, identificacion);
			if (asociadoVida.getCodRetornoWS() == 0) {
				return prospecto;
			} else {
				if ((asociadoVida.getEstado() >= 10 && asociadoVida.getEstado() <= 14)
						|| (asociadoVida.getEstado() >= 17 && asociadoVida.getEstado() <= 18)) {
					prospecto.setTipoIdentificacion((short) asociadoVida.getCodTipDocumento());
					prospecto.setIdentificacion(asociadoVida.getNumDocumento());
					prospecto.setNombre(asociadoVida.getNombre1().trim() + asociadoVida.getNombre2().trim());
					prospecto.setApellido(
							asociadoVida.getPrimerApellido().trim() + " " + asociadoVida.getSegundoApellido().trim());
					prospecto.setCelular(asociadoVida.getCelular().trim());
					prospecto.setDireccion(asociadoVida.getDirResidencia().trim());
					prospecto.setEmail(asociadoVida.getEmail().trim());

					prospecto.setEdad(asociadoVida.getAge() + "");

					prospecto.setEsAsociado("S");
					prospecto.setEstrato(asociadoVida.getEstrato() + "");
					prospecto.setGenero((short) asociadoVida.getCodSexo());
					prospecto.setProfesion(asociadoVida.getProfesion().trim());
					prospecto.setPersonaACargo("N");

					if (asociadoVida.getPerMayCargo() > 0 || asociadoVida.getPerMenCargo() > 0) {
						prospecto.setPersonaACargo("S");
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return prospecto;
	}

	/**
	 * Metodo para transformar una entidad en un objeto DTO
	 * 
	 * @param prospecto
	 * @return acProspectoDTO
	 */
	public AcProspectoDTO transformToDTO(Prospecto acProspecto) {
		AcProspectoDTO acProspectoDTO = new AcProspectoDTO();
		try {
			if (acProspecto != null) {
				if (acProspecto.getCodigoSegmento() != null) {
					List<ObjetivoSegmento> lstObjeticoSegmento = acProspecto.getCodigoSegmento()
							.getAcObjetivoSegmentoList();
					List<AcObjetivoDTO> lstObjetivo = new ArrayList<>();

					for (ObjetivoSegmento objetivoSegmento : lstObjeticoSegmento) {
						lstObjetivo.add(this.objetivoService.transformToDTO(objetivoSegmento.getCodigoObjetivo()));
					}

					acProspectoDTO.setLstObjetivo(lstObjetivo);
					acProspectoDTO.setSegmentoDto(this.segmentoService.transformToDTO(acProspecto.getCodigoSegmento()));
				}

				acProspectoDTO.setCodigo(acProspecto.getCodigo());
				acProspectoDTO.setTipoIdentificacion(acProspecto.getTipoIdentificacion());
				acProspectoDTO.setIdentificacion(acProspecto.getIdentificacion());
				acProspectoDTO.setNombre(acProspecto.getNombre());
				acProspectoDTO.setApellido(acProspecto.getApellido());
				acProspectoDTO.setCelular(acProspecto.getCelular());
				if (acProspecto.getCodigoEjecutivo() != null) {
					acProspectoDTO.setEjecutivoIntegral(
							this.ejecutivoIntegralService.transformToDTO(acProspecto.getCodigoEjecutivo()));
				}
				acProspectoDTO.setDireccion(acProspecto.getDireccion());
				acProspectoDTO.setEmail(acProspecto.getEmail());
				acProspectoDTO.setEdad(acProspecto.getEdad());
				acProspectoDTO.setEsAsociado(acProspecto.getEsAsociado());
				acProspectoDTO.setEstrato(acProspecto.getEstrato());
				acProspectoDTO.setGenero(acProspecto.getGenero());
				acProspectoDTO.setNivelAcademico(acProspecto.getNivelAcademico());
				acProspectoDTO.setPersonaACargo(acProspecto.getPersonaACargo());
				acProspectoDTO.setProfesion(acProspecto.getProfesion());
				acProspectoDTO.setZona(acProspecto.getZona());

				if (acProspecto.getCodigoCiudad() != null) {
					acProspectoDTO.setCiudadDto(this.ciudadService.transformToDto(acProspecto.getCodigoCiudad()));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acProspectoDTO;
	}

	/**
	 * Metodo para tranformar un objeto DTO en una Entidad
	 * 
	 * @param prospecto
	 * @return acProspectoEt
	 */
	public Prospecto transformToET(AcProspectoDTO acProspectoDTO) {
		Prospecto acProspectoEt = new Prospecto();
		try {
			if (acProspectoDTO != null) {
				acProspectoEt.setCodigo(acProspectoDTO.getCodigo());
				acProspectoEt.setTipoIdentificacion(acProspectoDTO.getTipoIdentificacion());
				acProspectoEt.setIdentificacion(acProspectoDTO.getIdentificacion());
				acProspectoEt.setNombre(acProspectoDTO.getNombre());
				acProspectoEt.setApellido(acProspectoDTO.getApellido());
				acProspectoEt.setCelular(acProspectoDTO.getCelular());

				AcEjecutivoIntegralDTO acEjecutivoIntegralDTO = acProspectoDTO.getEjecutivoIntegral();
				if (acEjecutivoIntegralDTO != null) {
					acProspectoEt.setCodigoEjecutivo(this.ejecutivoIntegralService
							.findByUsuario(acProspectoDTO.getEjecutivoIntegral().getUsuario()));
				}

				acProspectoEt.setDireccion(acProspectoDTO.getDireccion());
				acProspectoEt.setEmail(acProspectoDTO.getEmail());
				acProspectoEt.setEdad(acProspectoDTO.getEdad());
				acProspectoEt.setEsAsociado(acProspectoDTO.getEsAsociado());
				acProspectoEt.setEstrato(acProspectoDTO.getEstrato());
				acProspectoEt.setGenero(acProspectoDTO.getGenero());
				acProspectoEt.setNivelAcademico(acProspectoDTO.getNivelAcademico());
				acProspectoEt.setPersonaACargo(acProspectoDTO.getPersonaACargo());
				acProspectoEt.setProfesion(acProspectoDTO.getProfesion());
				acProspectoEt.setZona(acProspectoDTO.getZona());

				AcSegmentoDTO acSegmentoDTO = acProspectoDTO.getSegmentoDto();
				if (acSegmentoDTO != null) {
					acProspectoEt.setCodigoSegmento(this.segmentoService.findByCodigo(acSegmentoDTO.getCodigo()));
				} else {
					acProspectoEt.setCodigoSegmento(this.segmentoService.defineSegment(acProspectoEt));
				}
				acProspectoEt.setCodigoCiudad(this.ciudadService.transformToET(acProspectoDTO.getCiudadDto()));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acProspectoEt;
	}

	/**
	 * Metodo que consulta un asociado por tipo identificacion e identificacion en
	 * el servicio de Coomeva
	 * 
	 * @param tipoIdentificacion
	 * @param identificacion
	 * @return asociadoVida
	 */
	public AsociadoVida consultarAsociado(String tipoIdentificacion, String identificacion) {
		AsociadoVida asociadoVida = new AsociadoVida();
		try {
			asociadoVida = AsociadoVidaLogic.actionConsultarAsociadoVida(identificacion,
					Long.parseLong(tipoIdentificacion), null, null);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return asociadoVida;
	}

	/**
	 * Metodo que busca un AcProspecto por codigo
	 * 
	 * @param codigo
	 * @return AcProspecto
	 */
	@Override
	public Prospecto findByCodigo(Long codigo) {
		return this.prospectoRepository.findByCodigo(codigo);
	}

}
