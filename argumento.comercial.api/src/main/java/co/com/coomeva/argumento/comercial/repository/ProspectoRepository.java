/**
* InterfaseName: IAcProspectoRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.coomeva.argumento.comercial.model.Prospecto;

public interface ProspectoRepository extends JpaRepository<Prospecto, Integer> {

	public Prospecto findByCodigo(@Param("codigo") Long codigo);

	public List<Prospecto> findByIdentificacion(@Param("identificacion") String identificacion);

	@Query("SELECT a FROM Prospecto a WHERE a.tipoIdentificacion =:tipoIdentificacion and a.identificacion=:identificacion")
	public Prospecto findByTipoIdentificacionIdentificacion(@Param("tipoIdentificacion") Short tipoIdentificacion,
			@Param("identificacion") String identificacion);

}
