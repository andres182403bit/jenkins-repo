package co.com.coomeva.argumento.comercial.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.AcProductoObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.ProductoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo de los productos por objetivo de la
 * encuesta del prospecto.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/producto" })
public class ProductoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductoController.class);

	@Autowired
	private ProductoService productoService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que retorna un listado de productos de acuerdo a los objetivos
	 * seleccionados en la encuesta del prospecto.
	 * 
	 * @param listObjetivoDTO
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/getProductos")
	public ResponseService getProductos(@RequestBody List<AcObjetivoDTO> listObjetivoDTO) {
		ResponseService responseService = new ResponseService();
		try {
			List<AcProductoObjetivoDTO> listProcuctoObjetivoResponseDTO = this.productoService
					.getObjetivosProductos(listObjetivoDTO);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"),
					listProcuctoObjetivoResponseDTO);
			return responseService;
		} catch (Exception e) {
			LOGGER.error("Error ProductoController::getProductos: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}

}