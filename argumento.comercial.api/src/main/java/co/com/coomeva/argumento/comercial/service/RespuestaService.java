/**
 *	Interfasename: IAcRespuesta
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcRespuestaDTO;
import co.com.coomeva.argumento.comercial.model.Respuesta;

public interface RespuestaService {
	public List<Respuesta> findAll();

	public AcRespuestaDTO save(AcRespuestaDTO acRespuesta);

	public Respuesta transformToET(AcRespuestaDTO acRespuestaDto);

	public Respuesta findByCodigo(Long codigo);
}