/**
* InterfaseName: IAcParametroRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.coomeva.argumento.comercial.model.Parametro;

public interface ParametroRepository extends JpaRepository<Parametro, Integer> {

	@Query("SELECT a FROM Parametro a WHERE a.nombre=:nombre")
	public Parametro findByName(@Param("nombre") String nombre);
}
