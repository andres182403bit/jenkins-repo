package co.com.coomeva.argumento.comercial.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcCiudadDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.CiudadService;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo del CRUD de una ciudad en la
 * aplicación.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/ciudad" })
public class CiudadController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CiudadController.class);

	@Autowired
	private CiudadService ciudadService;

	/**
	 * Servicio que retorna todos las ciudades que se encuentran almacenadas en el
	 * sistema.
	 * 
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@GetMapping("/getAll")
	public ResponseService getAll() {
		ResponseService responseService = new ResponseService();
		try {
			List<AcCiudadDTO> listCiudadDTO = this.ciudadService.findAllDto();
			ResponseServiceUtil.buildSuccessResponse(responseService, listCiudadDTO);
		} catch (Exception e) {
			LOGGER.error("Error DepartamentoController::getAll: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		return responseService;
	}
}
