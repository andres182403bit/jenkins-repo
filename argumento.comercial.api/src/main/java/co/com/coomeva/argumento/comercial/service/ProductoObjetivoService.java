/**
 *	Interfasename: IAcProductoObjetivo
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcProductoObjetivoDTO;
import co.com.coomeva.argumento.comercial.model.ProductoObjetivo;

public interface ProductoObjetivoService {
	public List<ProductoObjetivo> findAll();

	public AcProductoObjetivoDTO save(AcProductoObjetivoDTO acProductoObjetivo);

	public AcProductoObjetivoDTO transformToDTO(ProductoObjetivo acProductoObjetivo);

	public ProductoObjetivo transformToET(AcProductoObjetivoDTO acProductoObjetivo);
}