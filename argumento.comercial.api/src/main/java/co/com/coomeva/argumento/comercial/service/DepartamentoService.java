/**
* InterfaseName: IAcDepartamento
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcDepartamentoDTO;
import co.com.coomeva.argumento.comercial.model.Departamento;

public interface DepartamentoService {
	public List<Departamento> findAll();

	public AcDepartamentoDTO transformToDto(Departamento acDepartamento);

	public Departamento transformToEt(AcDepartamentoDTO acDepartamentoDto);

	public List<AcDepartamentoDTO> findAllDto();

	public Departamento findByCodigo(Long codigo);
}
