/**
 *	Interfasename: IAcProductoProspecto
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcProductoProspectoDTO;
import co.com.coomeva.argumento.comercial.model.ProductoProspecto;

public interface ProductoProspectoService {
	public List<ProductoProspecto> findAll();

	public ProductoProspecto findByCodigo(Long codigo);

	public ProductoProspecto save(AcProductoProspectoDTO productoProspecto);

	public ProductoProspecto transformToET(AcProductoProspectoDTO productoProspecto);

	public AcProductoProspectoDTO transformToDto(ProductoProspecto acProductoProspecto);

	public void deleteProductoProspecto(Long codProspecto);

	public List<AcProductoProspectoDTO> saveList(List<AcProductoProspectoDTO> lstAcProductoProspectoDTO);

	public List<AcProductoProspectoDTO> updateList(List<AcProductoProspectoDTO> lstAcProductoProspectoDTO);
}