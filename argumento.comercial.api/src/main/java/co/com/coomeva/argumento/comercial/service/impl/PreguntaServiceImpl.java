/**
 *	Classname: AcPreguntaImpl
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcPreguntaDTO;
import co.com.coomeva.argumento.comercial.model.Pregunta;
import co.com.coomeva.argumento.comercial.model.PreguntaSegmento;
import co.com.coomeva.argumento.comercial.service.PreguntaService;
import co.com.coomeva.argumento.comercial.service.PreguntaSegmentoService;

@Transactional
@Service
public class PreguntaServiceImpl implements PreguntaService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreguntaServiceImpl.class);

	@Autowired
	private PreguntaSegmentoService preguntaSegmentoService;

	/**
	 * Metodo que transforma una entidad en un objeto DTO
	 * 
	 * @param acPregunta
	 * @return acPreguntaDTO
	 */
	public AcPreguntaDTO transformToDTO(Pregunta acPregunta) {
		AcPreguntaDTO acPreguntaDTO = new AcPreguntaDTO();
		try {
			acPreguntaDTO.setAyuda(acPregunta.getAyuda());
			acPreguntaDTO.setCodigo(acPregunta.getCodigo());
			acPreguntaDTO.setNumero(acPregunta.getNumero());
			acPreguntaDTO.setPregunta(acPregunta.getPregunta());
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acPreguntaDTO;
	}

	/**
	 * Metodo para consultar las preguntas existentes por codigo de segmento
	 * 
	 * @param codigoSegmento
	 * @return lstPregunta
	 */
	public List<AcPreguntaDTO> getQuestions(Long codigoSegmento) {
		List<PreguntaSegmento> lstPreguntaSegmento = new ArrayList<>();
		List<AcPreguntaDTO> lstPregunta = new ArrayList<>();
		try {
			lstPreguntaSegmento = this.preguntaSegmentoService.findQuestionBySegment(codigoSegmento);
			for (PreguntaSegmento acPreguntaSegmento : lstPreguntaSegmento) {
				lstPregunta.add(this.transformToDTO(acPreguntaSegmento.getCodigoPregunta()));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstPregunta;
	}
}
