/**
* Classname: AcCiudadImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcCiudadDTO;
import co.com.coomeva.argumento.comercial.model.Ciudad;
import co.com.coomeva.argumento.comercial.model.Departamento;
import co.com.coomeva.argumento.comercial.repository.DepartamentoRepository;
import co.com.coomeva.argumento.comercial.repository.CiudadRepository;
import co.com.coomeva.argumento.comercial.service.CiudadService;

@Transactional
@Service
public class CiudadServiceImpl implements CiudadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CiudadServiceImpl.class);

	@Autowired
	private CiudadRepository ciudadRepository;
	@Autowired
	private DepartamentoRepository departamentoRepository;

	/**
	 * retorna la lista de ciudades por departamento
	 * 
	 * @Param codigoDepartamento
	 * @return
	 */
	@Override
	public List<Ciudad> findByDepartamento(Long codigoDepartamento) {
		List<Ciudad> lstCiudades = new ArrayList<>();
		try {
			Departamento departamento = this.departamentoRepository.findById(codigoDepartamento).get();
			lstCiudades = departamento.getAcCiudadList();
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstCiudades;
	}

	/**
	 * retorna todas las ciudades existentes
	 * 
	 * @return lstCiudades
	 */
	@Override
	public List<Ciudad> consultarCiudades() {
		List<Ciudad> lstCiudades = new ArrayList<>();
		try {
			Iterable<Ciudad> itCiudad = this.ciudadRepository.findAll();
			itCiudad.forEach(lstCiudades::add);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstCiudades;
	}

	/**
	 * retorna una lista de AcCiudadesDTO existentes en el sistema
	 * 
	 * @return lstCiudadesDto
	 */
	public List<AcCiudadDTO> findAllDto() {
		List<Ciudad> lstCiudades = new ArrayList<>();
		List<AcCiudadDTO> lstCiudadesDto = new ArrayList<>();
		try {
			lstCiudades = this.consultarCiudades();
			for (Ciudad acCiudad : lstCiudades) {
				lstCiudadesDto.add(this.transformToDto(acCiudad));
			}

		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstCiudadesDto;
	}

	/**
	 * transforma una entidad en un objeto DTO
	 * 
	 * @param acCiudad
	 * @return acCiudadDTO
	 */
	public AcCiudadDTO transformToDto(Ciudad acCiudad) {
		AcCiudadDTO acCiudadDTO = new AcCiudadDTO();
		try {
			acCiudadDTO.setCodigo(acCiudad.getCodigo());
			acCiudadDTO.setNombre(acCiudad.getNombre());
			if (acCiudad.getCodigoDepartamento() != null) {
				acCiudadDTO.setCodigoDepartamento(acCiudad.getCodigoDepartamento().getCodigo());
				acCiudadDTO.setNombreDepartamento(acCiudad.getCodigoDepartamento().getNombre());
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acCiudadDTO;
	}

	/**
	 * transforma un objeto DTO en una entidad
	 * 
	 * @param acCiudadDto
	 * @return acCiudad
	 */
	public Ciudad transformToET(AcCiudadDTO acCiudadDto) {
		Ciudad acCiudad = new Ciudad();
		Departamento acDepartamento = new Departamento();
		try {
			acCiudad.setCodigo(acCiudadDto.getCodigo());
			acCiudad.setNombre(acCiudadDto.getNombre());
			if (acCiudadDto.getCodigoDepartamento() != 0) {
				acDepartamento = this.departamentoRepository.findById(acCiudadDto.getCodigoDepartamento()).get();
				acCiudad.setCodigoDepartamento(acDepartamento);
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acCiudad;
	}
}
