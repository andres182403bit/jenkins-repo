/**
* Classname: AcProspecto
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_PROSPECTO", schema = DatabaseUtil.SCHEMA)
public class Prospecto implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_PROSPECTO_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_PROSPECTO_SEQ", sequenceName = "AC_PROSPECTO_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Column(name = "TIPO_IDENTIFICACION")
	private Short tipoIdentificacion;
	@Size(max = 10)
	@Column(name = "IDENTIFICACION")
	private String identificacion;
	@Size(max = 300)
	@Column(name = "NOMBRE")
	private String nombre;
	@Size(max = 200)
	@Column(name = "APELLIDO")
	private String apellido;
	@Column(name = "GENERO")
	private Short genero;
	@Size(max = 20)
	@Column(name = "ZONA")
	private String zona;
	@Size(max = 10)
	@Column(name = "CELULAR")
	private String celular;
	// @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
	// message="Invalid email")//if the field contains email address consider using
	// this annotation to enforce field validation
	@Size(max = 200)
	@Column(name = "EMAIL")
	private String email;
	@Size(max = 300)
	@Column(name = "DIRECCION")
	private String direccion;
	@Size(max = 10)
	@Column(name = "ESTRATO")
	private String estrato;
	@Size(max = 300)
	@Column(name = "NIVEL_ACADEMICO")
	private String nivelAcademico;
	@Size(max = 300)
	@Column(name = "PROFESION")
	private String profesion;
	@Size(max = 1)
	@Column(name = "PERSONA_A_CARGO")
	private String personaACargo;
	@Size(max = 3)
	@Column(name = "EDAD")
	private String edad;
	@Size(max = 1)
	@Column(name = "ES_ASOCIADO")
	private String esAsociado;

	@OneToMany(mappedBy = "codigoProspecto")
	@JsonManagedReference
	private List<ObjetivosProspecto> acObjetivosProspectoList;
	@JoinColumn(name = "CODIGO_EJECUTIVO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	private EjecutivoIntegral codigoEjecutivo;
	@JoinColumn(name = "CODIGO_SEGMENTO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	private Segmento codigoSegmento;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoProspecto")
	@JsonIgnore
	private List<Respuesta> acRespuestaList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoProspecto")
	@JsonIgnore
	private List<ProductoProspecto> acProductoProspectoList;
	@JoinColumn(name = "CODIGO_CIUDAD", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	private Ciudad codigoCiudad;

	public Prospecto() {
	}

	public Prospecto(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Short getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(Short tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Short getGenero() {
		return genero;
	}

	public void setGenero(Short genero) {
		this.genero = genero;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstrato() {
		return estrato;
	}

	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}

	public String getNivelAcademico() {
		return nivelAcademico;
	}

	public void setNivelAcademico(String nivelAcademico) {
		this.nivelAcademico = nivelAcademico;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getPersonaACargo() {
		return personaACargo;
	}

	public void setPersonaACargo(String personaACargo) {
		this.personaACargo = personaACargo;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getEsAsociado() {
		return esAsociado;
	}

	public void setEsAsociado(String esAsociado) {
		this.esAsociado = esAsociado;
	}

	@XmlTransient
	public List<ObjetivosProspecto> getAcObjetivosProspectoList() {
		return acObjetivosProspectoList;
	}

	public void setAcObjetivosProspectoList(List<ObjetivosProspecto> acObjetivosProspectoList) {
		this.acObjetivosProspectoList = acObjetivosProspectoList;
	}

	public EjecutivoIntegral getCodigoEjecutivo() {
		return codigoEjecutivo;
	}

	public void setCodigoEjecutivo(EjecutivoIntegral codigoEjecutivo) {
		this.codigoEjecutivo = codigoEjecutivo;
	}

	public Segmento getCodigoSegmento() {
		return codigoSegmento;
	}

	public void setCodigoSegmento(Segmento codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}

	public Ciudad getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(Ciudad codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	@XmlTransient
	public List<Respuesta> getAcRespuestaList() {
		return acRespuestaList;
	}

	public void setAcRespuestaList(List<Respuesta> acRespuestaList) {
		this.acRespuestaList = acRespuestaList;
	}

	@XmlTransient
	public List<ProductoProspecto> getAcProductoProspectoList() {
		return acProductoProspectoList;
	}

	public void setAcProductoProspectoList(List<ProductoProspecto> acProductoProspectoList) {
		this.acProductoProspectoList = acProductoProspectoList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Prospecto)) {
			return false;
		}
		Prospecto other = (Prospecto) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcProspecto[ codigo=" + codigo + " ]";
	}

}
