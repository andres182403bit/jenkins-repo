/**
 *	Classname: AcProductoProspectoDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

public class AcProductoProspectoDTO {

	private Long codigo;
	private String fechaAOfrecer;
	private AcProductoDTO codigoProducto;
	private AcProspectoDTO codigoProspecto;
	private String agendado;

	public String getAgendado() {
		return agendado;
	}

	public void setAgendado(String agendado) {
		this.agendado = agendado;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getFechaAOfrecer() {
		return fechaAOfrecer;
	}

	public void setFechaAOfrecer(String fechaAOfrecer) {
		this.fechaAOfrecer = fechaAOfrecer;
	}

	public AcProductoDTO getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(AcProductoDTO codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public AcProspectoDTO getCodigoProspecto() {
		return codigoProspecto;
	}

	public void setCodigoProspecto(AcProspectoDTO codigoProspecto) {
		this.codigoProspecto = codigoProspecto;
	}

	@Override
	public String toString() {
		return "AcProductoProspectoDTO [codigo=" + codigo + ", fechaAOfrecer=" + fechaAOfrecer + ", codigoProducto="
				+ codigoProducto.toString() + ", codigoProspecto=" + codigoProspecto.toString() + ", agendado=" + agendado + "]";
	}

}
