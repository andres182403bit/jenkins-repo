/**
* InterfaseName: IAcDepartamentoRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.coomeva.argumento.comercial.model.Departamento;

public interface DepartamentoRepository extends JpaRepository<Departamento, Long> {

}