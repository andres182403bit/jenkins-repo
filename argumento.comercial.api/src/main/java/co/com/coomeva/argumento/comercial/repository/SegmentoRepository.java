/**
* InterfaseName: IAcSegmentoRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.coomeva.argumento.comercial.model.Segmento;

public interface SegmentoRepository extends JpaRepository<Segmento, Integer> {

	public Segmento findByNombre(@Param("nombre") String nombre);

	public Segmento findByCodigo(@Param("codigo") Long codigo);

	@Query(value = "SELECT * FROM AC_SEGMENTO SEG WHERE ?1 > SEG.EDAD_INICIO AND ?1 <= SEG.EDAD_FIN AND SEG.GENERO =?2 AND SEG.MENORES_CARGO = ?3", nativeQuery = true)
	public Segmento findSegment(Short edad, Short genero, Short menoresCargo);
	
	@Query(value = "SELECT * FROM AC_SEGMENTO SEG WHERE SEG.EDAD_INICIO = ?1 AND SEG.EDAD_FIN = ?2 AND SEG.GENERO =?3 AND SEG.MENORES_CARGO = ?4", nativeQuery = true)
	public Segmento validateSegment(Short edadInicio, Short edadFin, Short genero, Short menoresCargo);
}
