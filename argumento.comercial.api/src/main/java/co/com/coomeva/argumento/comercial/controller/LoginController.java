package co.com.coomeva.argumento.comercial.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.NodeList;

import co.com.coomeva.argumento.comercial.dto.AcEjecutivoIntegralDTO;
import co.com.coomeva.argumento.comercial.dto.LoginDTO;
import co.com.coomeva.argumento.comercial.dto.LoginResponseDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.dto.SectionDTO;
import co.com.coomeva.argumento.comercial.service.EjecutivoIntegralService;
import co.com.coomeva.argumento.comercial.util.ClientWS;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;
import co.com.coomeva.argumento.comercial.util.SoapService;

/**
 * Servicio Rest Controller para la validación del usuario de la aplicación por
 * medio del servicio del ProfileManager de Coomeva.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/services" })
public class LoginController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private EjecutivoIntegralService ejecutivoIntegralService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio encargado de validar si un usuario puede acceder a la aplicación por
	 * medio del servicio de profileManager de Coomeva y si se encuentra se crea o
	 * actualiza en el sistema.
	 * 
	 * @param loginDTO Contiene la información del usuario.
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping(value = "/login")
	public ResponseService validarUsuario(@RequestBody LoginDTO loginDTO) {
		ResponseService responseService = new ResponseService();
		AcEjecutivoIntegralDTO userInfo = new AcEjecutivoIntegralDTO();
		List<SectionDTO> roleSectionList = new ArrayList<>();
		LoginResponseDTO loginResponse = new LoginResponseDTO();
		try {
			// Se inicializa el response con un error controlado.
			ResponseServiceUtil.buildFailResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "userInvalid"));

			String app = managePropertiesLogic.getProperties("general.properties", "appProfile");
			String url = managePropertiesLogic.getProperties("general.properties", "urlProfile");
			SoapService service = new SoapService();
			NodeList hash = service.validarProfile(loginDTO.getLogin(), loginDTO.getPassword(), app, url);
			List<String> authorized = new ArrayList<>();
			ClientWS.getChildValueByName(hash, "authorized", authorized);
			NodeList roles = ClientWS.getChildByName(hash, "roles");

			if (roles == null) {
				ResponseServiceUtil.buildFailResponse(responseService,
						managePropertiesLogic.getProperties("respuesta.properties", "userSinPermisos"));
				return responseService;
			}

			List<String> roleList = new ArrayList<>();
			ClientWS.getChildValueByName(hash, "name", "roles", roleList);
			List<SectionDTO> sectionList = new ArrayList<>();
			ClientWS.getChildSectionsValueByName(hash, "sections", sectionList);

			if (!authorized.isEmpty()) {
				String auth = authorized.get(0);

				if (auth.equals("true")) {
					String strReturn = "return";
					StringBuilder userEmail = new StringBuilder();
					ClientWS.getChildValueByName(hash, "mail", userEmail);
					StringBuilder userName = new StringBuilder();
					ClientWS.getChildValueByName(hash, "name", strReturn, userName);
					StringBuilder userId = new StringBuilder();
					ClientWS.getChildValueByName(hash, "userId", strReturn, userId);
					StringBuilder id = new StringBuilder();
					ClientWS.getChildValueByName(hash, "id", strReturn, id);

					userInfo.setUsuario(userId.toString());
					userInfo.setEmail(userEmail.toString());
					userInfo.setName(userName.toString());
					userInfo.setIdentificacion(id.toString());
					userInfo = this.ejecutivoIntegralService.validarUsurio(userInfo);

					for (String role : roleList) {
						roleSectionList.add(new SectionDTO(role));
					}

					loginResponse.setRole(roleSectionList);
					loginResponse.setSections(sectionList);
					loginResponse.setUserInfo(userInfo);
					ResponseServiceUtil.buildSuccessResponse(responseService, loginResponse);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error LoginController::validarUsuario: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}
}