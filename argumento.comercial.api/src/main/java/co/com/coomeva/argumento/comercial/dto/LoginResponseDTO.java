/**
 *	Classname: LoginResponseDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

import java.util.List;

public class LoginResponseDTO {
	private List<SectionDTO> role;
	private List<SectionDTO> sections;
	private String token;
	private AcEjecutivoIntegralDTO userInfo;
	
	public AcEjecutivoIntegralDTO getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(AcEjecutivoIntegralDTO userInfo) {
		this.userInfo = userInfo;
	}
	public List<SectionDTO> getRole() {
		return role;
	}
	public void setRole(List<SectionDTO> role) {
		this.role = role;
	}
	public List<SectionDTO> getSections() {
		return sections;
	}
	public void setSections(List<SectionDTO> sections) {
		this.sections = sections;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	
}
