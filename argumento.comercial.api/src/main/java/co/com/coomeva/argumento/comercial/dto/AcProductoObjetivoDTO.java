/**
 *	Classname: AcProductoObjetivoDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

public class AcProductoObjetivoDTO
{
	private Long codigo;
	private String beneficio;
	private AcObjetivoDTO acObjetivoDto;
	private AcProductoDTO acProductoDto;
	
	public AcObjetivoDTO getAcObjetivoDto() {
		return acObjetivoDto;
	}
	public void setAcObjetivoDto(AcObjetivoDTO acObjetivoDto) {
		this.acObjetivoDto = acObjetivoDto;
	}
	public AcProductoDTO getAcProductoDto() {
		return acProductoDto;
	}
	public void setAcProductoDto(AcProductoDTO acProductoDto) {
		this.acProductoDto = acProductoDto;
	}
	
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getBeneficio() {
		return beneficio;
	}
	public void setBeneficio(String beneficio) {
		this.beneficio = beneficio;
	}
}
