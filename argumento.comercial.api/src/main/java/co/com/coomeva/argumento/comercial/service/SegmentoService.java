/**
 *	Interfasename: IAcSegmento
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcSegmentoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.model.Prospecto;
import co.com.coomeva.argumento.comercial.model.Segmento;

public interface SegmentoService {

	public List<Segmento> findAll();

	public Segmento findByNombre(String nombre);

	public Segmento findByCodigo(Long codigo);

	public Segmento defineSegment(Prospecto prospect);

	public Segmento getSegmento(Prospecto prospect);

	public AcSegmentoDTO transformToDTO(Segmento segmento);

	public Segmento findSegment(Short edad, Short genero, Short menoresCargo);

	public AcSegmentoDTO save(AcSegmentoDTO acSegmentoDto);

	public ResponseService delete(AcSegmentoDTO acSegmentoDTO);

	public List<AcSegmentoDTO> findAllDto();
}