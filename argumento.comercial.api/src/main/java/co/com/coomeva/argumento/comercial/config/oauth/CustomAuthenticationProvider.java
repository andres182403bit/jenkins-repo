/**
* Classname: CustomAuthenticationProvider
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.config.oauth;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
 
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static final String ROLE_READER = "ROLE_READER";

	/**
	 * Metodo que realiza la autenticacion en la aplicacion
	 * Argumento Comercial
	 * 
	 * @param authentication
	 * @return Authentication
	 */
	@Override 
	public Authentication authenticate(Authentication authentication) {

		String name = authentication.getName();
		String password = authentication.getCredentials().toString();
		List<GrantedAuthority> grantedAuths = new ArrayList<>();
		grantedAuths.add(new SimpleGrantedAuthority(ROLE_READER));
		return new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
		
	}

	/**
	 * Metodo que valida si la autenticación es valida
	 * @param authentication
	 * @return boolean
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
