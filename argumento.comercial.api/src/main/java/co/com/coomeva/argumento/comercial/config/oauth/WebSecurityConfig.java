/**
* Classname: WebSecurityConfig
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.config.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableWebSecurity
@Order(-20)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private CustomAuthenticationProvider authenticationProvider;
    
	/**
	 * Manejador de autenticacion
	 * @return authenticationManagerBean
	 */
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    /**
     * Metodo de configuracion de seguridad
     * @param http
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {   
    	
    	String strServices = "/services/*";
    	String strScope = "#oauth2.hasScope('read')";
    	http.csrf().disable()
        .authorizeRequests()
        .antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
		.antMatchers(HttpMethod.POST, strServices).access(strScope)
		.antMatchers(HttpMethod.PUT, strServices).access(strScope)
		.antMatchers(HttpMethod.GET, strServices).access(strScope);
        http.authorizeRequests().anyRequest().authenticated().and()
        .formLogin().loginPage("/login").permitAll()
        .and().httpBasic().and()
        .requestMatchers()
        .antMatchers("/login", "/oauth/authorize", "/oauth/confirm_access")
        .antMatchers("/fonts/**", "/js/**", "/css/**");
    }
    
    /**
     * Metodo codificador de password
     * @return PasswordEncoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
    	return new PasswordEncoder() {
            public String encode (CharSequence charSequence) {
                return charSequence.toString();
            }
            public boolean matches(CharSequence charSequence, String s) {
                return true;
            }
        };
    }
    
    /**
     * Metodo para detallar configuracion 
     * del servicio
     * @return UserDetailsService
     */
    @Bean
    @Override
    public UserDetailsService userDetailsService() {

        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

        final User.UserBuilder userBuilder = User.builder().passwordEncoder(encoder::encode);
        UserDetails user = userBuilder
                .username("user")
                .password("password")
                .roles("USER")
                .build();

        UserDetails admin = userBuilder
                .username("admin")
                .password("password")
                .roles("USER","ADMIN")
                .build();

        return new InMemoryUserDetailsManager(user, admin);
    }
 
    /**
     * Metodo de configuracion de seguridad
     * @param auth
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);       
    }
    
    /**
     * Metodo para configuracion de CORS
     * @return bean
     */
    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

}
