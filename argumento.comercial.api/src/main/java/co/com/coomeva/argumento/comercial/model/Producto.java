/**
* Classname: AcProducto
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_PRODUCTO", schema = DatabaseUtil.SCHEMA)
public class Producto implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Size(max = 500)
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@Size(max = 500)
	@Column(name = "CARACTERISTICA")
	private String caracteristica;
	@Size(max = 500)
	@Column(name = "VENTAJA")
	private String ventaja;
	@Size(max = 500)
	@Column(name = "REQUISITO_DOCUMENTAL")
	private String requisitoDocumental;
	@Size(max = 500)
	@Column(name = "SIMULADOR")
	private String simulador;
	@Size(max = 500)
	@Column(name = "NOTA")
	private String nota;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@Column(name = "VALOR")
	private BigDecimal valor;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoProducto")
	@JsonIgnore
	private List<ProductoObjetivo> acProductoObjetivoList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoProducto")
	@JsonIgnore
	private List<ProductoProspecto> acProductoProspectoList;

	public Producto() {
	}

	public Producto(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCaracteristica() {
		return caracteristica;
	}

	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}

	public String getVentaja() {
		return ventaja;
	}

	public void setVentaja(String ventaja) {
		this.ventaja = ventaja;
	}

	public String getRequisitoDocumental() {
		return requisitoDocumental;
	}

	public void setRequisitoDocumental(String requisitoDocumental) {
		this.requisitoDocumental = requisitoDocumental;
	}

	public String getSimulador() {
		return simulador;
	}

	public void setSimulador(String simulador) {
		this.simulador = simulador;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@XmlTransient
	public List<ProductoObjetivo> getAcProductoObjetivoList() {
		return acProductoObjetivoList;
	}

	public void setAcProductoObjetivoList(List<ProductoObjetivo> acProductoObjetivoList) {
		this.acProductoObjetivoList = acProductoObjetivoList;
	}

	@XmlTransient
	public List<ProductoProspecto> getAcProductoProspectoList() {
		return acProductoProspectoList;
	}

	public void setAcProductoProspectoList(List<ProductoProspecto> acProductoProspectoList) {
		this.acProductoProspectoList = acProductoProspectoList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Producto)) {
			return false;
		}
		Producto other = (Producto) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcProducto[ codigo=" + codigo + " ]";
	}

}
