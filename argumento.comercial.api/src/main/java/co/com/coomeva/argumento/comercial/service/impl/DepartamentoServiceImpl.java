/**
* Classname: AcDepartamentoImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcCiudadDTO;
import co.com.coomeva.argumento.comercial.dto.AcDepartamentoDTO;
import co.com.coomeva.argumento.comercial.model.Ciudad;
import co.com.coomeva.argumento.comercial.model.Departamento;
import co.com.coomeva.argumento.comercial.repository.DepartamentoRepository;
import co.com.coomeva.argumento.comercial.service.CiudadService;
import co.com.coomeva.argumento.comercial.service.DepartamentoService;

@Transactional
@Service
public class DepartamentoServiceImpl implements DepartamentoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartamentoServiceImpl.class);

	@Autowired
	private DepartamentoRepository departamentoRepository;
	@Autowired
	private CiudadService ciudadService;

	/**
	 * Retorna todos los departamentos existentes en la base de datos
	 * 
	 * @return lstDepartamentos
	 */
	@Override
	public List<Departamento> findAll() {
		List<Departamento> lstDepartamentos = new ArrayList<>();
		try {
			Iterable<Departamento> itDepartamento = this.departamentoRepository.findAll();
			itDepartamento.forEach(lstDepartamentos::add);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstDepartamentos;
	}

	/**
	 * transforma una entidad en un objeto DTO
	 * 
	 * @param acDepartamento
	 * @return acDepartamentoDto
	 */
	public AcDepartamentoDTO transformToDto(Departamento acDepartamento) {
		AcDepartamentoDTO acDepartamentoDto = new AcDepartamentoDTO();
		try {
			acDepartamentoDto.setCodigo(acDepartamento.getCodigo());
			acDepartamentoDto.setNombre(acDepartamento.getNombre());
			List<AcCiudadDTO> lstCiudadDto = new ArrayList<>();
			for (Ciudad acCiudad : acDepartamento.getAcCiudadList()) {
				lstCiudadDto.add(this.ciudadService.transformToDto(acCiudad));
			}
			acDepartamentoDto.setAcCiudadList(lstCiudadDto);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acDepartamentoDto;
	}

	/**
	 * transforma un objeto DTO en una entidad
	 * 
	 * @param acDepartamentoDto
	 * @return acDepartamento
	 */
	public Departamento transformToEt(AcDepartamentoDTO acDepartamentoDto) {
		Departamento acDepartamento = new Departamento();
		try {
			acDepartamento.setCodigo(acDepartamentoDto.getCodigo());
			acDepartamento.setNombre(acDepartamentoDto.getNombre());
			List<Ciudad> lstCiudad = new ArrayList<>();
			for (AcCiudadDTO acCiudad : acDepartamentoDto.getAcCiudadList()) {
				lstCiudad.add(this.ciudadService.transformToET(acCiudad));
			}
			acDepartamento.setAcCiudadList(lstCiudad);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acDepartamento;
	}

	/**
	 * retorna una lista de objetos AcDepartamentoDTO
	 * 
	 * @return lstDepartamentosDto
	 */
	public List<AcDepartamentoDTO> findAllDto() {
		List<AcDepartamentoDTO> lstDepartamentosDto = new ArrayList<>();
		List<Departamento> lstDepartamentos = new ArrayList<>();
		try {
			lstDepartamentos = this.findAll();
			for (Departamento acDepartamento : lstDepartamentos) {
				lstDepartamentosDto.add(this.transformToDto(acDepartamento));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstDepartamentosDto;
	}

	/**
	 * Metodo para buscar una entidad tipo AcDepartamento por el codigo recibido por
	 * parametro
	 * 
	 * @param codigo
	 * @return acDepartamento
	 */
	@Override
	public Departamento findByCodigo(Long codigo) {
		Departamento acDepartamento = new Departamento();
		try {
			acDepartamento = this.departamentoRepository.findById(codigo).get();
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acDepartamento;
	}
}
