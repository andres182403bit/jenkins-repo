/**
 *	Classname: SectionDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

import java.util.List;

public class SectionDTO {
	private String name;
	private String description;
	private String module;
	private String icon;
	private List<SectionDTO> actions;
	
	public SectionDTO() {
	}
	public SectionDTO(String name) {
		this.name = name;
	}
	public SectionDTO(String name, String description, String module) {
		this.name = name;
		this.description = description;
		this.module = module;
	}
	
	public SectionDTO(String name, String module) {
		this.name = name;
		this.module = module;
	}
	
	public SectionDTO(String name, String description, String module, String icon) {
		this.name = name;
		this.description = description;
		this.module = module;
		this.icon = icon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public List<SectionDTO> getActions() {
		return actions;
	}
	public void setActions(List<SectionDTO> actions) {
		this.actions = actions;
	}
	
	
}
