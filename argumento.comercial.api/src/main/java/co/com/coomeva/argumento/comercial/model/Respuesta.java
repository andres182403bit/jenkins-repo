/**
* Classname: AcRespuesta
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_RESPUESTA", schema = DatabaseUtil.SCHEMA)
public class Respuesta implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_RESPUESTA_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_RESPUESTA_SEQ", sequenceName = "AC_RESPUESTA_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Size(max = 300)
	@Column(name = "RESPUESTA")
	private String respuesta;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 4)
	@Column(name = "CODIGO_PREGUNTA")
	private String codigoPregunta;
	@JoinColumn(name = "CODIGO_PROSPECTO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	private Prospecto codigoProspecto;

	public Respuesta() {
	}

	public Respuesta(Long codigo) {
		this.codigo = codigo;
	}

	public Respuesta(Long codigo, String codigoPregunta) {
		this.codigo = codigo;
		this.codigoPregunta = codigoPregunta;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getCodigoPregunta() {
		return codigoPregunta;
	}

	public void setCodigoPregunta(String codigoPregunta) {
		this.codigoPregunta = codigoPregunta;
	}

	public Prospecto getCodigoProspecto() {
		return codigoProspecto;
	}

	public void setCodigoProspecto(Prospecto codigoProspecto) {
		this.codigoProspecto = codigoProspecto;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Respuesta)) {
			return false;
		}
		Respuesta other = (Respuesta) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcRespuesta[ codigo=" + codigo + " ]";
	}

}
