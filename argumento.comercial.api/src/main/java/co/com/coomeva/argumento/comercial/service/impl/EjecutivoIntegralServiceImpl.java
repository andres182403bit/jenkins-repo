/**
* Classname: AcEjecutivoIntegralImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcEjecutivoIntegralDTO;
import co.com.coomeva.argumento.comercial.model.EjecutivoIntegral;
import co.com.coomeva.argumento.comercial.repository.EjecutivoIntegralRepository;
import co.com.coomeva.argumento.comercial.service.EjecutivoIntegralService;

@Transactional
@Service
public class EjecutivoIntegralServiceImpl implements EjecutivoIntegralService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EjecutivoIntegralServiceImpl.class);

	@Autowired
	private EjecutivoIntegralRepository ejecutivoIntegralRepository;

	/**
	 * Retorna una lista de AcEjecutivoIntegral
	 * 
	 * @return List<AcEjecutivoIntegral>
	 */
	@Override
	public List<EjecutivoIntegral> findAll() {
		return (List<EjecutivoIntegral>) ejecutivoIntegralRepository.findAll();
	}

	/**
	 * Guarda una entidada de tipo AcEjecutivoIntegral en la base de datos
	 * 
	 * @return AcEjecutivoIntegral
	 * @param prospecto
	 */
	@Override
	public EjecutivoIntegral save(EjecutivoIntegral prospecto) {
		return ejecutivoIntegralRepository.save(prospecto);
	}

	/**
	 * Metodo que busca un usuario por nombre de usuario
	 * 
	 * @param usuario
	 * @return AcEjecutivoIntegral
	 */
	@Override
	public EjecutivoIntegral findByUsuario(String usuario) {
		return ejecutivoIntegralRepository.findByUsuario(usuario);
	}

	/**
	 * Transforma un objeto Entidad en un DTO
	 * 
	 * @param ejecutivoIntegral
	 * @return acEjecutivoIntegralDTO
	 */
	public AcEjecutivoIntegralDTO transformToDTO(EjecutivoIntegral ejecutivoIntegral) {
		AcEjecutivoIntegralDTO acEjecutivoIntegralDTO = new AcEjecutivoIntegralDTO();
		EjecutivoIntegral ejecutivoIntegralTranform = new EjecutivoIntegral();
		try {
			ejecutivoIntegralTranform = ejecutivoIntegral;
			acEjecutivoIntegralDTO.setCelular(ejecutivoIntegralTranform.getCelular());
			acEjecutivoIntegralDTO.setCodigo(ejecutivoIntegralTranform.getCodigo().toString());
			acEjecutivoIntegralDTO.setEmail(ejecutivoIntegralTranform.getEmail());
			acEjecutivoIntegralDTO.setIdentificacion(ejecutivoIntegralTranform.getIdentificacion().toString());
			acEjecutivoIntegralDTO.setName(ejecutivoIntegralTranform.getNombre());
			acEjecutivoIntegralDTO.setUsuario(ejecutivoIntegralTranform.getUsuario());
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acEjecutivoIntegralDTO;
	}

	/**
	 * valida si un usuario existe si existe actualiza el email y el nombre
	 * 
	 * @param acEjecutivoIntegralDto
	 * @return acEjecutivoIntegralDto
	 */
	public AcEjecutivoIntegralDTO validarUsurio(AcEjecutivoIntegralDTO acEjecutivoIntegralDto) {
		EjecutivoIntegral acEjecutivoIntegral = new EjecutivoIntegral();
		try {
			acEjecutivoIntegral = this.findByUsuario(acEjecutivoIntegralDto.getUsuario());

			if (acEjecutivoIntegral == null) {
				acEjecutivoIntegral = new EjecutivoIntegral();
				acEjecutivoIntegral.setNombre(acEjecutivoIntegralDto.getName());
				acEjecutivoIntegral.setEmail(acEjecutivoIntegralDto.getEmail());
				acEjecutivoIntegral.setUsuario(acEjecutivoIntegralDto.getUsuario());
				acEjecutivoIntegral.setIdentificacion(Long.parseLong(acEjecutivoIntegralDto.getIdentificacion()));
			} else {
				acEjecutivoIntegral.setNombre(acEjecutivoIntegralDto.getName());
				acEjecutivoIntegral.setEmail(acEjecutivoIntegralDto.getEmail());
			}

			acEjecutivoIntegralDto = this.transformToDTO(this.save(acEjecutivoIntegral));
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}

		return acEjecutivoIntegralDto;
	}

}
