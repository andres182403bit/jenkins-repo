/**
* InterfaseName: IAcEjecutivoIntegralRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import co.com.coomeva.argumento.comercial.model.EjecutivoIntegral;

public interface EjecutivoIntegralRepository extends JpaRepository<EjecutivoIntegral, Integer> {

	public EjecutivoIntegral findByUsuario(@Param("usuario") String usuario);
}
