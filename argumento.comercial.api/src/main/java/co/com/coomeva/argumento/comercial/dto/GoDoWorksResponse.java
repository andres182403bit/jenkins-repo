package co.com.coomeva.argumento.comercial.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model mapping for entity SLF_PARAMETRO.
 * 
 * @author Bastian M. Cabello <bastian.cabello@ingenieros.com>
 * @since 08/08/2019
 * @version 1.0
 */
public class GoDoWorksResponse {

	@JsonProperty(value = "msg")
	private String mensaje;

	@JsonProperty(value = "error")
	private boolean error;

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return the error
	 */
	public boolean isError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(boolean error) {
		this.error = error;
	}

}