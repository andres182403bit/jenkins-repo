/**
* Classname: ResponseServiceUtil
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.dto.Status;

public class ResponseServiceUtil {
	
	/**
	 * Builds a success response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildSuccessResponse(ResponseService responseService, List<?> data) {
		responseService.setStatus(Status.OK);
		responseService.setData(data);
	}
	
	/**
	 * Builds a fail response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildFailResponse(ResponseService responseService, String message) {
		responseService.setStatus(Status.FAILURE);
		responseService.setMessage(message);
		responseService.setMessageError(message);
		responseService.setTitle("Error");
	}
	
	/**
	 * Builds a success response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildSuccessResponse(ResponseService responseService, Object data) {
		responseService.setStatus(Status.OK);
		responseService.setData(data);
	}
	
	/**
	 * Builds a success response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildSuccessResponse(ResponseService responseService, String data) {
		responseService.setStatus(Status.OK);
		responseService.setMessage(data);
	}
	
	/**
	 * Builds a success response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildSuccessResponse(ResponseService responseService, String message, Object data) {
		responseService.setStatus(Status.OK);
		responseService.setMessage(message);
		responseService.setData(data);
	}
	
	/**
	 * Builds a fail response message.
	 * 
	 * @param responseService the response message object.
	 * @param ex the exception object.
	 */
	public static void buildFailResponse(ResponseService responseService, Exception ex) {
		responseService.setStatus(Status.FAILURE);
		responseService.setMessageError("Error al consultar el servicio. Revise los logs para mas informacion.");
		responseService.setTitle("Error");
		responseService.setMessage("Error al consultar el servicio: " + ex.getMessage());
	}

}