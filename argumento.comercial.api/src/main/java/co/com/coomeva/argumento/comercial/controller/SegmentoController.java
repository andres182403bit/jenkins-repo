package co.com.coomeva.argumento.comercial.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcSegmentoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.SegmentoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo del CRUD de los segmentos de la
 * aplicación.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/segmento" })
public class SegmentoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SegmentoController.class);

	@Autowired
	private SegmentoService segmentoService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que retornar todos los segmentos que se encuentran almacenados en el
	 * sistema.
	 * 
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@GetMapping("/getAll")
	public ResponseService getAll() {
		ResponseService responseService = new ResponseService();
		List<AcSegmentoDTO> listSegmentoDTO = new ArrayList<>();
		try {
			listSegmentoDTO = segmentoService.findAllDto();
			ResponseServiceUtil.buildSuccessResponse(responseService, listSegmentoDTO);
		} catch (Exception e) {
			LOGGER.error("Error SegmentoController::getAll: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		return responseService;
	}

	/**
	 * Servicio que se encarga de crear o actualizar un segmento en el sistema.
	 * 
	 * @param segmentoDTO Contiene la información del segmento.
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/save")
	public ResponseService save(@RequestBody AcSegmentoDTO segmentoDTO) {
		ResponseService responseService = new ResponseService();
		try {
			AcSegmentoDTO segmentoResponseDTO = this.segmentoService.save(segmentoDTO);

			if (segmentoResponseDTO != null) {
				ResponseServiceUtil.buildSuccessResponse(responseService,
						managePropertiesLogic.getProperties("respuesta.properties", "saveRow"), segmentoResponseDTO);
			} else {
				ResponseServiceUtil.buildFailResponse(responseService,
						managePropertiesLogic.getProperties("respuesta.properties", "segmentoError"));
			}
		} catch (Exception e) {
			LOGGER.error("Error SegmentoController::save: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}

	/**
	 * Servicio que se encarga de eliminar un segmento en el sistema.
	 * 
	 * @param segmentoDTO Contiene la información del segmento.
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/delete")
	public ResponseService delete(@RequestBody AcSegmentoDTO segmentoDTO) {
		ResponseService responseService = new ResponseService();
		try {
			responseService = this.segmentoService.delete(segmentoDTO);
		} catch (Exception e) {
			LOGGER.error("Error SegmentoController::delete: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}
}