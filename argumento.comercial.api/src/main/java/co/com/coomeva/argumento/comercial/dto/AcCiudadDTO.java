/**
 *	Classname: AcCiudadDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 30/04/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

public class AcCiudadDTO {

    private Long codigo;
    private String nombre;
    private Long codigoDepartamento;
    private String nombreDepartamento;

 
	public String getNombreDepartamento() {
		return nombreDepartamento;
	}

	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}

	public Long getCodigoDepartamento() {
		return codigoDepartamento;
	}

	public void setCodigoDepartamento(Long codigoDepartamento) {
		this.codigoDepartamento = codigoDepartamento;
	}

	public AcCiudadDTO() {
    }

    public AcCiudadDTO(Long codigo) {
        this.codigo = codigo;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
