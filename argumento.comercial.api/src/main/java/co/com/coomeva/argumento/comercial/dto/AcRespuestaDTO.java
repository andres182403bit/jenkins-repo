/**
 *	Classname: AcRespuestaDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

public class AcRespuestaDTO {
	private Long codigo;
	private String respuesta;
	private String codigoPregunta;
	private AcProspectoDTO acProspectoDto;
	private Long codigoProspecto;
	  
	public Long getCodigoProspecto() {
		return codigoProspecto;
	}
	public void setCodigoProspecto(Long codigoProspecto) {
		this.codigoProspecto = codigoProspecto;
	}
	public AcProspectoDTO getAcProspectoDto() {
		return acProspectoDto;
	}
	public void setAcProspectoDto(AcProspectoDTO acProspectoDto) {
		this.acProspectoDto = acProspectoDto;
	}
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getCodigoPregunta() {
		return codigoPregunta;
	}
	public void setCodigoPregunta(String codigoPregunta) {
		this.codigoPregunta = codigoPregunta;
	}
}
