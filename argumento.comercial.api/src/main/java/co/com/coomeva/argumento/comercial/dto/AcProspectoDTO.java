/**
 *	Classname: AcProspectoDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

import java.util.List;

public class AcProspectoDTO {
	private Long codigo;
	private Short tipoIdentificacion;
	private String identificacion;
	private String nombre;
	private String apellido;
	private Short genero;
	private String zona;
	private String celular;
	private String email;
	private String direccion;
	private String estrato;
	private String nivelAcademico;
	private String profesion;
	private String personaACargo;
	private String edad;
	private String esAsociado;
	private AcEjecutivoIntegralDTO ejecutivoIntegral;
	private AcCiudadDTO ciudadDto;
	private List<AcObjetivoDTO> lstObjetivo;
	private AcSegmentoDTO segmentoDto;

	public AcProspectoDTO() {
		this.codigo = new Long("0");
		this.tipoIdentificacion = new Short("0");
		this.identificacion = "";
		this.nombre = "";
		this.apellido = "";
		this.genero = new Short("0");
		this.zona = "";
		this.celular = "";
		this.email = "";
		this.direccion = "";
		this.estrato = "";
		this.nivelAcademico = "";
		this.profesion = "";
		this.personaACargo = "";
		this.edad = "";
		this.esAsociado = "";
	}

	public List<AcObjetivoDTO> getLstObjetivo() {
		return lstObjetivo;
	}

	public void setLstObjetivo(List<AcObjetivoDTO> lstObjetivo) {
		this.lstObjetivo = lstObjetivo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Short getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(Short tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Short getGenero() {
		return genero;
	}

	public void setGenero(Short genero) {
		this.genero = genero;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstrato() {
		return estrato;
	}

	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}

	public String getNivelAcademico() {
		return nivelAcademico;
	}

	public void setNivelAcademico(String nivelAcademico) {
		this.nivelAcademico = nivelAcademico;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getPersonaACargo() {
		return personaACargo;
	}

	public void setPersonaACargo(String personaACargo) {
		this.personaACargo = personaACargo;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getEsAsociado() {
		return esAsociado;
	}

	public void setEsAsociado(String esAsociado) {
		this.esAsociado = esAsociado;
	}

	public AcEjecutivoIntegralDTO getEjecutivoIntegral() {
		return ejecutivoIntegral;
	}

	public void setEjecutivoIntegral(AcEjecutivoIntegralDTO ejecutivoIntegral) {
		this.ejecutivoIntegral = ejecutivoIntegral;
	}

	public AcSegmentoDTO getSegmentoDto() {
		return segmentoDto;
	}

	public void setSegmentoDto(AcSegmentoDTO segmentoDto) {
		this.segmentoDto = segmentoDto;
	}

	public AcCiudadDTO getCiudadDto() {
		return ciudadDto;
	}

	public void setCiudadDto(AcCiudadDTO acCiudadDto) {
		this.ciudadDto = acCiudadDto;
	}

	@Override
	public String toString() {
		return "AcProspectoDTO [codigo=" + codigo + ", tipoIdentificacion=" + tipoIdentificacion + ", identificacion="
				+ identificacion + ", nombre=" + nombre + ", apellido=" + apellido + ", genero=" + genero + ", zona="
				+ zona + ", celular=" + celular + ", email=" + email + ", direccion=" + direccion + ", estrato="
				+ estrato + ", nivelAcademico=" + nivelAcademico + ", profesion=" + profesion + ", personaACargo="
				+ personaACargo + ", edad=" + edad + ", esAsociado=" + esAsociado + ", ejecutivoIntegral="
				+ ejecutivoIntegral + ", ciudadDto=" + ciudadDto + ", lstObjetivo=" + lstObjetivo + ", segmentoDto="
				+ segmentoDto + "]";
	}

}
