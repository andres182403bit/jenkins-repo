/**
* Classname: AcObjetivoProspectoImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcObjetivosProspectoDTO;
import co.com.coomeva.argumento.comercial.model.Objetivo;
import co.com.coomeva.argumento.comercial.model.ObjetivosProspecto;
import co.com.coomeva.argumento.comercial.model.Prospecto;
import co.com.coomeva.argumento.comercial.repository.ObjetivoProspectoRepository;
import co.com.coomeva.argumento.comercial.service.ObjetivoProspectoService;
import co.com.coomeva.argumento.comercial.service.ObjetivoService;
import co.com.coomeva.argumento.comercial.service.ProspectoService;

@Transactional
@Service
public class ObjetivoProspectoServiceImpl implements ObjetivoProspectoService{ 
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ObjetivoProspectoServiceImpl.class);

	@Autowired
	private ObjetivoProspectoRepository objetivoProspectoRepository;
	@Autowired
	private ObjetivoService objetivoService;
	@Autowired
	private ProspectoService prospectoService;

	
    /**
     * Metodo que retorna todos los objetos de tipo AcObjetivosProspecto 
     * que existan en la base de datos
     * @return List<AcObjetivosProspecto>
     */
	@Override
	public List<ObjetivosProspecto> findAll() { 
		return objetivoProspectoRepository.findAll();
	}

	/**
	 * Metodo que almacena un objetivo prospecto en la base de datos
	 * @param acObjetivosProspecto
	 * @return acObjetivosProspectoDto
	 */
	@Override
	public AcObjetivosProspectoDTO save(AcObjetivosProspectoDTO acObjetivosProspecto) 
	{
		AcObjetivosProspectoDTO acObjetivosProspectoDto = new AcObjetivosProspectoDTO();
		ObjetivosProspecto acObjetivosProspectoET = new ObjetivosProspecto();
		try
		{
			acObjetivosProspectoDto = acObjetivosProspecto;
			this.deleteObjetivoProspecto(acObjetivosProspecto.getAcProspectoDto().getCodigo());
			acObjetivosProspectoET = this.objetivoProspectoRepository.save(this.transformToET(acObjetivosProspectoDto));
			acObjetivosProspectoDto = this.transformToDTO(acObjetivosProspectoET);	
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acObjetivosProspectoDto;
	}
	
	/**
	 * sirve para transformar un objeto DTO en una Entidad
	 * 
	 * @param acObjetivosProspecto
	 * @return
	 */
	public ObjetivosProspecto transformToET(AcObjetivosProspectoDTO acObjetivosProspecto)
	{
	  AcObjetivosProspectoDTO acObjetivosProspectoDto = new AcObjetivosProspectoDTO();
	  ObjetivosProspecto acObjetivosProspectoET = new ObjetivosProspecto();
	  Objetivo acObjetivo = new Objetivo();
	  Prospecto acProspecto = new Prospecto();
	  try
	  {
		acObjetivosProspectoDto = acObjetivosProspecto;
		acObjetivosProspectoET.setCodigo(acObjetivosProspectoDto.getCodigo());
		if(acObjetivosProspectoDto.getAcObjetivoDto()!=null)
		{
		  acObjetivo = this.objetivoService.findByCodigo(acObjetivosProspectoDto.getAcObjetivoDto().getCodigo());
		  acObjetivosProspectoET.setCodigoObjetivo(acObjetivo);
		}
		if(acObjetivosProspectoDto.getAcProspectoDto().getCodigo()!=null)
		{	
		  acProspecto = this.prospectoService.findByCodigo(acObjetivosProspectoDto.getAcProspectoDto().getCodigo());
		  acObjetivosProspectoET.setCodigoProspecto(acProspecto);
		}
	  }catch (Exception e)
	   {
		LOGGER.error("Error: {}", e.getMessage(), e);
		throw e;
	   }
	  return acObjetivosProspectoET;
	}
     
	/**
	 * Metodo que transforma un objeto entidad en un DTO
	 * @param acObjetivosProspectoET
	 * @return acObjetivosProspectoDto
	 */
	public AcObjetivosProspectoDTO transformToDTO(ObjetivosProspecto acObjetivosProspectoET)
	{
	  AcObjetivosProspectoDTO acObjetivosProspectoDto = new AcObjetivosProspectoDTO();
	  try
	  {
	   if(acObjetivosProspectoET!=null)
	   {
		 acObjetivosProspectoDto.setAcObjetivoDto(this.objetivoService.transformToDTO(acObjetivosProspectoET.getCodigoObjetivo()));
		 acObjetivosProspectoDto.setAcProspectoDto(this.prospectoService.transformToDTO(acObjetivosProspectoET.getCodigoProspecto()));
		 acObjetivosProspectoDto.setCodigo(acObjetivosProspectoET.getCodigo());
	   }
	  }catch (Exception e)
	   {
		LOGGER.error("Error: {}", e.getMessage(), e);
		throw e;
	   }
	  return acObjetivosProspectoDto;
	}

	/**
	 * Metodo que busca un metodo por codigo objetivo y codigo prospecto
	 * @param codObjetivo
	 * @param codProspecto
	 * @return acObjetivosProspectoET
	 */
	@Override
	public ObjetivosProspecto findObjetivoProspecto(Long codObjetivo, Long codProspecto) {
	  ObjetivosProspecto acObjetivosProspectoET = new ObjetivosProspecto();
	  try
	  {
		  acObjetivosProspectoET = this.objetivoProspectoRepository.findObjetivoProspecto(codObjetivo, codProspecto);
	  }catch (Exception e)
	   {
		LOGGER.error("Error: {}", e.getMessage(), e);
		throw e;
	   }
	  return acObjetivosProspectoET;
	}

	/**
	 * Metodo que elimina un objetivo prospecto 
	 * @param codProspecto
	 */
	@Override
	public void deleteObjetivoProspecto(Long codProspecto) 
	{
	  try
	  {
		this.objetivoProspectoRepository.deleteObjetivoProspecto(codProspecto);
	  }catch (Exception e)
	   {
		 LOGGER.error("Error: {}", e.getMessage(), e);
		 throw e;
	   }
	}
}