/**
* Classname: AcCiudad
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_CIUDAD", schema = DatabaseUtil.SCHEMA)
public class Ciudad implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_CIUDAD_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_CIUDAD_SEQ", sequenceName = "AC_CIUDAD_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Size(max = 100)
	@Column(name = "NOMBRE")
	private String nombre;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "CODIGO_DEPARTAMENTO", referencedColumnName = "CODIGO")
	private Departamento codigoDepartamento;

	public Ciudad() {
	}

	public Ciudad(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Departamento getCodigoDepartamento() {
		return codigoDepartamento;
	}

	public void setCodigoDepartamento(Departamento codigoDepartamento) {
		this.codigoDepartamento = codigoDepartamento;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Ciudad)) {
			return false;
		}
		Ciudad other = (Ciudad) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcCiudad[ codigo=" + codigo + " ]";
	}

}
