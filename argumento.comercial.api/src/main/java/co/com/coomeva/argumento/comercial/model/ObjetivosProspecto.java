/**
* Classname: AcObjetivoSegmento
* @version 1.0 15/03/2020
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_OBJETIVOS_PROSPECTO", schema = DatabaseUtil.SCHEMA)
public class ObjetivosProspecto implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_OBJETIVOS_PROSPECTO_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_OBJETIVOS_PROSPECTO_SEQ", sequenceName = "AC_OBJETIVOS_PROSPECTO_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@JoinColumn(name = "CODIGO_OBJETIVO", referencedColumnName = "CODIGO")
	@ManyToOne(optional = false)
	private Objetivo codigoObjetivo;
	@JoinColumn(name = "CODIGO_PROSPECTO", referencedColumnName = "CODIGO")
	@ManyToOne
	@JsonBackReference
	private Prospecto codigoProspecto;

	public ObjetivosProspecto() {
	}

	public ObjetivosProspecto(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Objetivo getCodigoObjetivo() {
		return codigoObjetivo;
	}

	public void setCodigoObjetivo(Objetivo codigoObjetivo) {
		this.codigoObjetivo = codigoObjetivo;
	}

	public Prospecto getCodigoProspecto() {
		return codigoProspecto;
	}

	public void setCodigoProspecto(Prospecto codigoProspecto) {
		this.codigoProspecto = codigoProspecto;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ObjetivosProspecto)) {
			return false;
		}
		ObjetivosProspecto other = (ObjetivosProspecto) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcObjetivosProspecto[ codigo=" + codigo + " ]";
	}

}
