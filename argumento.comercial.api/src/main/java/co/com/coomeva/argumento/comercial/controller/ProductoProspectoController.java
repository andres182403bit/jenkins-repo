package co.com.coomeva.argumento.comercial.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcProductoProspectoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.ProductoProspectoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo de los productos de la encuesta del
 * prospecto.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/productoprospecto" })
public class ProductoProspectoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductoProspectoController.class);

	@Autowired
	private ProductoProspectoService productoProspectoService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que se encarga de almacenar los productos de un prospecto en una
	 * encuesta y los envia a GoDoWorks.
	 * 
	 * @param listProductoProspectoDTO listado de productos seleccionados.
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/save")
	public ResponseService save(@RequestBody List<AcProductoProspectoDTO> listProductoProspectoDTO) {
		ResponseService responseService = new ResponseService();
		try {
			List<AcProductoProspectoDTO> listProductoProspectoResponseDTO = this.productoProspectoService
					.saveList(listProductoProspectoDTO);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"),
					listProductoProspectoResponseDTO);
		} catch (Exception e) {
			LOGGER.error("Error ProductoProspectoController::save: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}
}