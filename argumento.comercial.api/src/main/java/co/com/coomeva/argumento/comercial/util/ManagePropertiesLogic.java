/**
* Classname: ManagePropertiesLogic
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import java.util.List;

public class ManagePropertiesLogic {

	public ManagePropertiesLogic() {
		// Construct
	}
	/**
	 * Añade los mensajes de properties a la lista
	 * @param list lista donde se agregan los mensajes
	 * @param codMsg código del mensaje
	 * @param descMsg descripción del mensaje
	 * @param nameProperties nombre del properties
	 * @return list
	 */
	public List<ManageProperties> addMessagePropertiesToList(List<ManageProperties> list, String codMsg, String descMsg, String nameProperties){
		ManageProperties message = new ManageProperties();
		message.setLabel(LoadProperties.getInstance().getProperty(nameProperties, codMsg));
		message.setValue(LoadProperties.getInstance().getProperty(nameProperties, descMsg));
		list.add(message); 
		return list;
	}
	
	/**
	 * Obtiene las propiedades
	 * @param properties nombre del properties
	 * @param descProperties descripción o variable del properties
	 * @return cadena
	 */
	public String getProperties(String properties, String descProperties){
		return LoadProperties.getInstance().getProperty(properties, descProperties);
	}
}
