/**
* Classname: AcPregunta
*
* @version 1.0 04/05/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_PREGUNTA", schema = DatabaseUtil.SCHEMA)
public class Pregunta implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_PREGUNTA_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_PREGUNTA_SEQ", sequenceName = "AC_PREGUNTA_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 3)
	@Column(name = "NUMERO")
	private String numero;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 400)
	@Column(name = "PREGUNTA")
	private String pregunta;
	@Size(max = 4000)
	@Column(name = "AYUDA")
	private String ayuda;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoPregunta")
	private List<PreguntaSegmento> acPreguntaSegmentoList;

	public Pregunta() {
	}

	public Pregunta(Long codigo) {
		this.codigo = codigo;
	}

	public Pregunta(Long codigo, String numero, String pregunta) {
		this.codigo = codigo;
		this.numero = numero;
		this.pregunta = pregunta;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getAyuda() {
		return ayuda;
	}

	public void setAyuda(String ayuda) {
		this.ayuda = ayuda;
	}

	@XmlTransient
	@JsonIgnore
	public List<PreguntaSegmento> getAcPreguntaSegmentoList() {
		return acPreguntaSegmentoList;
	}

	public void setAcPreguntaSegmentoList(List<PreguntaSegmento> acPreguntaSegmentoList) {
		this.acPreguntaSegmentoList = acPreguntaSegmentoList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Pregunta)) {
			return false;
		}
		Pregunta other = (Pregunta) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcPregunta[ codigo=" + codigo + " ]";
	}

}
