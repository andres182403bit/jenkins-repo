/**
 *	Classname: AcObjetivosProspectoDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

import co.com.coomeva.argumento.comercial.dto.AcObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.AcProspectoDTO;

public class AcObjetivosProspectoDTO {
    private Long codigo;
	private AcObjetivoDTO acObjetivoDto;
    private AcProspectoDTO acProspectoDto;
    
    public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public AcObjetivoDTO getAcObjetivoDto() {
		return acObjetivoDto;
	}
	public void setAcObjetivoDto(AcObjetivoDTO acObjetivoDto) {
		this.acObjetivoDto = acObjetivoDto;
	}
	public AcProspectoDTO getAcProspectoDto() {
		return acProspectoDto;
	}
	public void setAcProspectoDto(AcProspectoDTO acProspectoDto) {
		this.acProspectoDto = acProspectoDto;
	}
}
