/**
* Classname: AcEjecutivoIntegral
* @version 1.0 15/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.com.coomeva.argumento.comercial.util.DatabaseUtil;

@Entity
@Table(name = "AC_EJECUTIVO_INTEGRAL", schema = DatabaseUtil.SCHEMA)
public class EjecutivoIntegral implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "AC_EJECUTIVO_INTEGRAL_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AC_EJECUTIVO_INTEGRAL_SEQ", sequenceName = "AC_EJECUTIVO_INTEGRAL_SEQ", allocationSize = 1)
	@Basic(optional = false)
	@NotNull
	@Column(name = "CODIGO")
	private Long codigo;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 300)
	@Column(name = "NOMBRE")
	private String nombre;
	@Size(max = 10)
	@Column(name = "CELULAR")
	private String celular;
	// @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
	// message="Invalid email")//if the field contains email address consider using
	// this annotation to enforce field validation
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 300)
	@Column(name = "EMAIL")
	private String email;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 20)
	@Column(name = "USUARIO")
	private String usuario;
	@Basic(optional = false)
	@NotNull
	@Column(name = "IDENTIFICACION")
	private Long identificacion;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoEjecutivo")
	@JsonIgnore
	private List<Prospecto> acProspectoList;

	public EjecutivoIntegral() {
	}

	public EjecutivoIntegral(Long codigo) {
		this.codigo = codigo;
	}

	public EjecutivoIntegral(Long codigo, String nombre, String email, String usuario, Long identificacion) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.email = email;
		this.usuario = usuario;
		this.identificacion = identificacion;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(Long identificacion) {
		this.identificacion = identificacion;
	}

	@XmlTransient
	public List<Prospecto> getAcProspectoList() {
		return acProspectoList;
	}

	public void setAcProspectoList(List<Prospecto> acProspectoList) {
		this.acProspectoList = acProspectoList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof EjecutivoIntegral)) {
			return false;
		}
		EjecutivoIntegral other = (EjecutivoIntegral) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.coomeva.argumento.comercial.model.AcEjecutivoIntegral[ codigo=" + codigo + " ]";
	}

}