/**
 *	Classname: AcDepartamentoDTO
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 30/04/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.dto;

import java.util.List;


public class AcDepartamentoDTO {

    private Long codigo;
    private String nombre;
    private List<AcCiudadDTO> lstAcCiudadDto;

    public AcDepartamentoDTO() {
    }

    public AcDepartamentoDTO(Long codigo) {
        this.codigo = codigo;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<AcCiudadDTO> getAcCiudadList() {
        return lstAcCiudadDto;
    }

    public void setAcCiudadList(List<AcCiudadDTO> lstAcCiudadDto) {
        this.lstAcCiudadDto = lstAcCiudadDto;
    }
}
