package co.com.coomeva.argumento.comercial.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.ObjetivoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo del CRUD de los objetivos de la
 * aplicación.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/objetivo" })
public class ObjetivoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ObjetivoController.class);

	@Autowired
	private ObjetivoService objetivoService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que retornar todos los objetivos que se encuentran almacenados en el
	 * sistema.
	 * 
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@GetMapping("/getAll")
	public ResponseService getAll() {
		ResponseService responseService = new ResponseService();
		try {
			List<AcObjetivoDTO> listObjetivoDTO = this.objetivoService.findAllDto();
			ResponseServiceUtil.buildSuccessResponse(responseService, listObjetivoDTO);
		} catch (Exception e) {
			LOGGER.error("Error ObjetivoController::getAll: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		return responseService;
	}

	/**
	 * Servicio que se encarga de crear o actualizar un objetivo en el sistema.
	 * 
	 * @param objetivoDTO Contiene la información del objetivo.
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@PostMapping("/save")
	public ResponseService save(@RequestBody AcObjetivoDTO objetivoDTO) {
		ResponseService responseService = new ResponseService();
		try {
			AcObjetivoDTO objetivoResponseDTO = this.objetivoService.save(objetivoDTO);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"), objetivoResponseDTO);
		} catch (Exception e) {
			LOGGER.error("Error ObjetivoController::save: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}
	
    /**
     * Servicio que se encarga de eliminar un objetivo de la base de datos siempre y
     * cuando no tenga relaciones con ninguna otra entidad de la aplicación
     * 
     * @param objetivoDTO Contiene la información del objetivo.
     * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
    @PostMapping("/delete")
    public ResponseService delete(@RequestBody AcObjetivoDTO objetivoDTO) {
        ResponseService responseService = new ResponseService();
        try {
            responseService = this.objetivoService.delete(objetivoDTO);
        } catch (Exception e) {
        	LOGGER.error("Error ObjetivoController::delete: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}