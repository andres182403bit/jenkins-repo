/**
 *	Interfasename: IAcProducto
 *	@author: Ricardo Alejandro Morales Penilla
 *	@version 1.0 15/03/2020
 *	Copyright (c) GTC Corporation
 */
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.AcProductoDTO;
import co.com.coomeva.argumento.comercial.dto.AcProductoObjetivoDTO;
import co.com.coomeva.argumento.comercial.model.Producto;

public interface ProductoService {
	public List<Producto> findAll();

	public Producto save(Producto prospecto);

	public List<AcProductoObjetivoDTO> getObjetivosProductos(List<AcObjetivoDTO> lstObjetivo);

	public AcProductoDTO transformToDTO(Producto acProductoEt);

	public Producto transformToET(AcProductoDTO acProducto);
}