package co.com.coomeva.argumento.comercial.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcPreguntaDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.PreguntaService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo de las preguntas de una encuesta para
 * un prospecto teniendo encuenta el segmento asociado.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/pregunta" })
public class PreguntaController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreguntaController.class);

	@Autowired
	private PreguntaService preguntaService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Servicio que se encarga de consultar las preguntas para una encuesta.
	 * 
	 * @param codigoSegmento codigo del segmento asociado al prospecto.
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@GetMapping("/findBySegmento")
	public ResponseService findBySegmento(@RequestParam("codigoSegmento") Long codigoSegmento) {
		ResponseService responseService = new ResponseService();
		List<AcPreguntaDTO> lstPregunta = new ArrayList<>();
		try {
			lstPregunta = this.preguntaService.getQuestions(codigoSegmento);
			ResponseServiceUtil.buildSuccessResponse(responseService,
					managePropertiesLogic.getProperties("respuesta.properties", "saveRow"), lstPregunta);
		} catch (Exception e) {
			LOGGER.error("Error PreguntaController::findBySegmento: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}
}
