/**
* InterfaseName: IAcEjecutivoIntegral
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service;

import java.util.List;

import co.com.coomeva.argumento.comercial.dto.AcEjecutivoIntegralDTO;
import co.com.coomeva.argumento.comercial.model.EjecutivoIntegral;


public interface EjecutivoIntegralService {
	public List<EjecutivoIntegral> findAll(); 
	public EjecutivoIntegral save(EjecutivoIntegral prospecto);
	public EjecutivoIntegral findByUsuario(String usuario);
	public AcEjecutivoIntegralDTO transformToDTO(EjecutivoIntegral ejecutivoIntegral);
	public AcEjecutivoIntegralDTO validarUsurio(AcEjecutivoIntegralDTO acEjecutivoIntegralDto);
}
