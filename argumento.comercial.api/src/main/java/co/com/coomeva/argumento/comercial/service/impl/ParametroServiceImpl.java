/**
*	Classname: AcParametroImpl
*	@author: Ricardo Alejandro Morales Penilla
*	@version 1.0 04/05/2020
*	Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.model.Parametro;
import co.com.coomeva.argumento.comercial.repository.ParametroRepository;
import co.com.coomeva.argumento.comercial.service.ParametroService;

@Transactional
@Service
public class ParametroServiceImpl implements ParametroService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParametroServiceImpl.class);

	@Autowired
	private ParametroRepository parametroRepository;

	/**
	 * Metodo para buscar un parametro por el nombre recibido
	 * 
	 * @param nombre
	 * @return acParametro
	 */
	@Override
	public Parametro findByName(String nombre) {
		Parametro acParametro = new Parametro();
		try {
			acParametro = this.parametroRepository.findByName(nombre);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return acParametro;
	}
}
