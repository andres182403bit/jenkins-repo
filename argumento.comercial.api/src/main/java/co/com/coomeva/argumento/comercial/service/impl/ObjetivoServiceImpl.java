/**
* Classname: AcObjetivoImpl
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.argumento.comercial.dto.AcNecesidadDerivadaDTO;
import co.com.coomeva.argumento.comercial.dto.AcObjetivoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.model.NecesidadObjetivo;
import co.com.coomeva.argumento.comercial.model.Objetivo;
import co.com.coomeva.argumento.comercial.repository.ObjetivoRepository;
import co.com.coomeva.argumento.comercial.service.NecesidadDerivadaService;
import co.com.coomeva.argumento.comercial.service.ObjetivoService;
import co.com.coomeva.argumento.comercial.util.ManagePropertiesLogic;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

@Transactional
@Service
public class ObjetivoServiceImpl implements ObjetivoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ObjetivoServiceImpl.class);

	@Autowired
	private ObjetivoRepository objetivoRepository;
	@Autowired
	private NecesidadDerivadaService necesidadDerivadaService;

	private ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();

	/**
	 * Metodo que devuelve todos los objetos tipo AcObjetivo que existan en la base
	 * de datos
	 * 
	 * @return List<AcObjetivo>
	 */
	@Override
	public List<Objetivo> findAll() {
		return objetivoRepository.findAll();
	}

	/**
	 * Metodo que almacena un objetivo que recibe como parametro
	 * 
	 * @return acObjetivoDTO
	 * @param objetivo
	 */
	@Override
	public AcObjetivoDTO save(AcObjetivoDTO objetivo) {
		AcObjetivoDTO acObjetivoDTO = new AcObjetivoDTO();
		Objetivo acObjetivoEt = new Objetivo();
		try {
			acObjetivoDTO = objetivo;
			acObjetivoEt = this.transformToET(objetivo);

			if (acObjetivoEt.getCodigo() != null) {
				acObjetivoEt = this.objetivoRepository.findByCodigo(acObjetivoEt.getCodigo());
				acObjetivoEt.setDescripcion(acObjetivoDTO.getDescripcion());
			}

			acObjetivoEt = objetivoRepository.save(acObjetivoEt);
			acObjetivoDTO = this.transformToDTO(acObjetivoEt);
			return acObjetivoDTO;
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Transforma un objeto entidad en un DTO
	 * 
	 * @param objetivo
	 * @return acObjetivoDTO
	 */
	public AcObjetivoDTO transformToDTO(Objetivo objetivo) {
		AcObjetivoDTO acObjetivoDTO = new AcObjetivoDTO();
		Objetivo acObjetivoEt = new Objetivo();
		try {
			acObjetivoEt = objetivo;
			acObjetivoDTO.setCodigo(acObjetivoEt.getCodigo());
			acObjetivoDTO.setDescripcion(acObjetivoEt.getDescripcion());

			if (acObjetivoEt.getAcNecesidadObjetivoList() != null
					&& !acObjetivoEt.getAcNecesidadObjetivoList().isEmpty()) {
				List<AcNecesidadDerivadaDTO> lstNecesidadDerivadaDto = new ArrayList<>();
				List<NecesidadObjetivo> lstNecesidadObjativoEt = acObjetivoEt.getAcNecesidadObjetivoList();

				for (NecesidadObjetivo acNecesidadObjetivo : lstNecesidadObjativoEt) {
					lstNecesidadDerivadaDto
							.add(this.necesidadDerivadaService.transformToDTO(acNecesidadObjetivo.getCodigoNecesidad()));
				}
				acObjetivoDTO.setLstNecesodadDerivadaDto(lstNecesidadDerivadaDto);
			}
			return acObjetivoDTO;
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Busca un objeto AcObjetivo en la base de datos dependiendo de el codigo
	 * utilizado
	 * 
	 * @param codigo
	 * @return AcObjetivo
	 */
	@Override
	public Objetivo findByCodigo(Long codigo) {
		try {
			return this.objetivoRepository.findByCodigo(codigo);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Tranforma un objeto DTO en un entidad
	 * 
	 * @param acObjetivo
	 * @return acObjetivoEt
	 */
	public Objetivo transformToET(AcObjetivoDTO acObjetivo) {
		AcObjetivoDTO acObjetivoDTO = new AcObjetivoDTO();
		Objetivo acObjetivoEt = new Objetivo();
		try {
			acObjetivoDTO = acObjetivo;
			acObjetivoEt.setCodigo(acObjetivoDTO.getCodigo());
			acObjetivoEt.setDescripcion(acObjetivoDTO.getDescripcion());
			return acObjetivoEt;
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Metodo que devuelve una lista de Objetivos DTO que existen en la base de
	 * datos
	 * 
	 * @return lstObjetivoDto
	 */
	@Override
	public List<AcObjetivoDTO> findAllDto() {
		List<AcObjetivoDTO> lstObjetivoDto = new ArrayList<>();
		List<Objetivo> lstObjetivoEt = new ArrayList<>();
		try {
			lstObjetivoEt = this.findAll();
			for (Objetivo acObjetivo : lstObjetivoEt) {
				lstObjetivoDto.add(this.transformToDTO(acObjetivo));
			}
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			throw e;
		}
		return lstObjetivoDto;
	}

	/**
	 * Metodo que recibe un objetivo DTO y lo elimina de la base de datos
	 * 
	 * @param acObjetivo
	 * @return responseService
	 */
	@Override
	public ResponseService delete(AcObjetivoDTO acObjetivoDto) {
		ResponseService responseService = new ResponseService();
		Objetivo acObjetivoEt = new Objetivo();
		try {
			acObjetivoEt = this.objetivoRepository.findByCodigo(acObjetivoDto.getCodigo());
			if (acObjetivoEt.getAcNecesidadObjetivoList().isEmpty()
					&& acObjetivoEt.getAcObjetivoSegmentoList().isEmpty()
					&& acObjetivoEt.getAcObjetivosProspectoList().isEmpty()
					&& acObjetivoEt.getAcProductoObjetivoList().isEmpty()) {
				this.objetivoRepository.delete(acObjetivoEt);
				ResponseServiceUtil.buildSuccessResponse(responseService, acObjetivoDto);
				responseService.setMessage(managePropertiesLogic.getProperties("respuesta.properties", "DeleteDatos"));
			} else {
				ResponseServiceUtil.buildFailResponse(responseService,
						managePropertiesLogic.getProperties("respuesta.properties", "DeleteDatosAsociados"));
			}

		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		return responseService;
	}
}
