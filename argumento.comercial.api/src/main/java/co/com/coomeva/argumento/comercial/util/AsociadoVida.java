/**
* Classname: AsociadoVida
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AsociadoVida", propOrder = { "codAsociado", "corte", "cuotaVencida", "estado", "fecEstado",
		"fecIngreso", "fecNacimiento", "ocupacion", "profesion" })
public class AsociadoVida extends Asociado {

	protected long codAsociado;
	@XmlElement(required = true, nillable = true)
	protected String corte;
	protected long cuotaVencida;
	protected long estado;
	protected long fecEstado;
	protected long fecIngreso;
	protected long fecNacimiento;
	@XmlElement(required = true, nillable = true)
	protected String ocupacion;
	@XmlElement(required = true, nillable = true)
	protected String profesion;

	protected long codOficinaVin;
	protected long codTipoVinculacion;
	protected String desOficinaVin;
	protected String desTipovinculacion;
	
	public AsociadoVida()
	{
		this.codAsociado = new Long(0);
		this.corte = "";
		this.cuotaVencida= new Long(0);
		this.estado= new Long(0);
		this.fecEstado= new Long(0);
		this.fecIngreso= new Long(0);
		this.fecNacimiento= new Long(0);
		this.ocupacion = "";
		this.profesion = "";
		this.codOficinaVin= new Long(0);
		this.codTipoVinculacion= new Long(0);
		this.desOficinaVin = "";
		this.desTipovinculacion = "";
	}

	public long getCodAsociado() {
		return codAsociado;
	}

	public void setCodAsociado(long value) {
		this.codAsociado = value;
	}

	public String getCorte() {
		return corte;
	}

	public void setCorte(String value) {
		this.corte = value;
	}

	public long getCuotaVencida() {
		return cuotaVencida;
	}

	public void setCuotaVencida(long value) {
		this.cuotaVencida = value;
	}

	public long getEstado() {
		return estado;
	}

	public void setEstado(long value) {
		this.estado = value;
	}

	public long getFecEstado() {
		return fecEstado;
	}

	public void setFecEstado(long value) {
		this.fecEstado = value;
	}

	public long getFecIngreso() {
		return fecIngreso;
	}

	public void setFecIngreso(long value) {
		this.fecIngreso = value;
	}

	public long getFecNacimiento() {
		return fecNacimiento;
	}

	public void setFecNacimiento(long value) {
		this.fecNacimiento = value;
	}

	public String getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(String value) {
		this.ocupacion = value;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String value) {
		this.profesion = value;
	}

	public long getCodOficinaVin() {
		return codOficinaVin;
	}

	public void setCodOficinaVin(long codOficinaVin) {
		this.codOficinaVin = codOficinaVin;
	}

	public long getCodTipoVinculacion() {
		return codTipoVinculacion;
	}

	public void setCodTipoVinculacion(long codTipoVinculacion) {
		this.codTipoVinculacion = codTipoVinculacion;
	}

	public String getDesOficinaVin() {
		return desOficinaVin;
	}

	public void setDesOficinaVin(String desOficinaVin) {
		this.desOficinaVin = desOficinaVin;
	}

	public String getDesTipovinculacion() {
		return desTipovinculacion;
	}

	public void setDesTipovinculacion(String desTipovinculacion) {
		this.desTipovinculacion = desTipovinculacion;
	}

	@Override
	public String toString() {
		return "AsociadoVida [codAsociado=" + codAsociado + ", corte=" + corte + ", cuotaVencida=" + cuotaVencida
				+ ", estado=" + estado + ", fecEstado=" + fecEstado + ", fecIngreso=" + fecIngreso + ", fecNacimiento="
				+ fecNacimiento + ", ocupacion=" + ocupacion + ", profesion=" + profesion + ", codOficinaVin="
				+ codOficinaVin + ", codTipoVinculacion=" + codTipoVinculacion + ", desOficinaVin=" + desOficinaVin
				+ ", desTipovinculacion=" + desTipovinculacion + "]";
	}

}
