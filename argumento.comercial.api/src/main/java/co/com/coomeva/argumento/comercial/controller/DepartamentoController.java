package co.com.coomeva.argumento.comercial.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.argumento.comercial.dto.AcDepartamentoDTO;
import co.com.coomeva.argumento.comercial.dto.ResponseService;
import co.com.coomeva.argumento.comercial.service.DepartamentoService;
import co.com.coomeva.argumento.comercial.util.ResponseServiceUtil;

/**
 * Servicio Rest Controller para el manejo del CRUD de un departamento en la
 * aplicación.
 * 
 * @author: Diego Bocanegra
 * @version 1.0
 */
@RestController
@RequestMapping({ "/departamento" })
public class DepartamentoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartamentoController.class);

	@Autowired
	private DepartamentoService departamentoService;

	/**
	 * Servicio que retorna todos los departamentos que se encuentran almacenados
	 * en el sistema.
	 * 
	 * @return Retorna una respuesta de tipo {@link ResponseService}
	 */
	@GetMapping("/getAll")
	public ResponseService getAll() {
		ResponseService responseService = new ResponseService();
		try {
			List<AcDepartamentoDTO> listDepartamentoDTO = this.departamentoService.findAllDto();
			ResponseServiceUtil.buildSuccessResponse(responseService, listDepartamentoDTO);
		} catch (Exception e) {
			LOGGER.error("Error DepartamentoController::getAll: {}", e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}

		return responseService;
	}

}
