/**
* InterfaseName: IAcProductoProspectoRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.coomeva.argumento.comercial.model.ProductoProspecto;

public interface ProductoProspectoRepository extends JpaRepository<ProductoProspecto, Integer> {

	public ProductoProspecto findByCodigo(@Param("codigo") Long codigo);

	@Modifying
	@Query(value = "DELETE FROM ProductoProspecto a WHERE a.codigoProspecto.codigo = :codProspecto")
	public void deleteProductoProspecto(@Param("codProspecto") Long codProspecto);
}
