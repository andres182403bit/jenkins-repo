/**
* Classname: LoadProperties
* @version 1.0 28/04/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.util;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase Generica para cargar las propuedades de los archivos .properties
 */
public class LoadProperties {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoadProperties.class);

	private static LoadProperties instance;

	private LoadProperties() {
	}

	/**
	 * Patron singular
	 * 
	 * @return LoadProperties Instancia de la clase.
	 */
	public static LoadProperties getInstance() {
		if (instance == null) {
			instance = new LoadProperties();
		}
		return instance;
	}

	/**
	 * Metodo para obtener una propiedad especifica
	 * 
	 * @param fileName
	 * @param property
	 * @return String
	 */
	public String getProperty(String fileName, String property) {
		Properties pro = null;
		try {
			pro = new Properties();
			pro.load(this.getClass().getResource(fileName).openStream());
			return pro.getProperty(property);
		} catch (Exception e) {
			LOGGER.error("Error: {}", e.getMessage(), e);
		}
		return null;
	}
}
