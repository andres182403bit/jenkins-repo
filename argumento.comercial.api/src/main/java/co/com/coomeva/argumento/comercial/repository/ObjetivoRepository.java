/**
* InterfaseName: IAcObjetivoRepository
*
* @version 1.0 04/03/2020 
* @author Ricardo Alejandro Morales Penilla
* Copyright (c) GTC Corporation
*/
package co.com.coomeva.argumento.comercial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.coomeva.argumento.comercial.model.Objetivo;

public interface ObjetivoRepository extends JpaRepository<Objetivo, Integer> {

	@Query("SELECT a FROM Objetivo a ORDER BY a.codigo desc")
	public List<Objetivo> findAll();

	public Objetivo findByCodigo(@Param("codigo") Long codigo);

	public void delete(@Param("acObjetivo") Objetivo acObjetivo);
}
